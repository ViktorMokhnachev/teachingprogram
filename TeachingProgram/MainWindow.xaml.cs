﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TeachingProgram
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (Window1.WindowCount == 0)
            {
                Window1 window = new Window1();
                window.Owner = this;
                window.Show();
                Window1.WindowCount++;
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Window2 window = new Window2();
            window.Owner = this;
            window.Show();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            Window3 window = new Window3();
            window.Owner = this;
            window.Show();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            Window4 window = new Window4();
            window.Owner = this;
            window.Show();
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            Window5 window = new Window5();
            window.Owner = this;
            window.Show();
        }
    }
}
