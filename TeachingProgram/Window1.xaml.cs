﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TeachingProgram
{
    /// <summary>
    /// Логика взаимодействия для Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        public static int WindowCount = 0;
        bool isSqrt = false;
        bool isFrac = false;

        public Window1()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, TextChangedEventArgs e)
        {
            int start = textBox1.SelectionStart;
            if (isSqrt && !isFrac)
            {
                isSqrt = false;
                string newstring = textBox1.Text.Insert(start, "}");
                textBox1.Text = newstring;
                textBox1.Focus();
                textBox1.SelectionStart = start;
            }
            formula.Formula = textBox1.Text;
        }

        private void Sqrt_Click(object sender, RoutedEventArgs e)
        {
            int start = textBox1.SelectionStart;
            int length = textBox1.SelectionLength;
            int finish = start + length;
            if (length == 0)
            {
                string sqrt = "\\sqrt{";
                string newstring = textBox1.Text.Insert(start, sqrt);
                textBox1.Text = newstring;
                textBox1.Focus();
                textBox1.SelectionStart = start + sqrt.Length;
                isSqrt = true;
            }
            else
            {
                string newstring = textBox1.Text.Insert(start, "\\sqrt{").Insert(finish + 6, "}");
                textBox1.Text = newstring;
                textBox1.Focus();
                textBox1.SelectionStart = finish + 7;
            }
        }

        private void Frac_Click(object sender, RoutedEventArgs e)
        {
            isFrac = true;
            int start = textBox1.SelectionStart;
            int length = textBox1.SelectionLength;
            int finish = start + length;
            if (length == 0)
            {
                string frac = "\\frac{}{}";
                string newstring = textBox1.Text.Insert(start, frac);
                textBox1.Text = newstring;
                textBox1.Focus();
                isFrac = false;
                if (isSqrt)
                {
                    isSqrt = false;
                    newstring = textBox1.Text.Insert(start + frac.Length, "}");
                    textBox1.Text = newstring;
                    textBox1.Focus();
                }
                textBox1.SelectionStart = start + frac.Length - 3;
            }
            else
            {
                string newstring = textBox1.Text.Insert(start, "\\frac{").Insert(finish + 6, "}{}");
                textBox1.Text = newstring;
                textBox1.Focus();
                textBox1.SelectionStart = finish + 8;
            }
            
        }

        private void Square_Click(object sender, RoutedEventArgs e)
        {
            int start = textBox1.SelectionStart;
            int length = textBox1.SelectionLength;
            int finish = start + length;
            if (length == 0)
            {
                string newstring = textBox1.Text.Insert(start, "^2");
                textBox1.Text = newstring;
                textBox1.Focus();
                textBox1.SelectionStart = start + 2;
            }
            else
            {
                string newstring = textBox1.Text.Insert(start, "(").Insert(finish + 1, ")^2");
                textBox1.Text = newstring;
                textBox1.Focus();
                textBox1.SelectionStart = finish + 4;
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            WindowCount--;
        }

        private void Brackets_Click(object sender, RoutedEventArgs e)
        {
            int start = textBox1.SelectionStart;
            int length = textBox1.SelectionLength;
            int finish = start + length;
            if (length != 0)
            {
                string newstring = textBox1.Text.Insert(start, "(").Insert(finish + 1, ")");
                textBox1.Text = newstring;
                textBox1.Focus();
                textBox1.SelectionStart = finish + 2;
            }
        }

        private void Approx_Click(object sender, RoutedEventArgs e)
        {
            int start = textBox1.SelectionStart;
            int length = textBox1.SelectionLength;
            if (length == 0)
            {
                string newstring = textBox1.Text.Insert(start, "\\approx ");
                textBox1.Text = newstring;
                textBox1.Focus();
                textBox1.SelectionStart = start + 8;
            }
        }

        private void Index_Click(object sender, RoutedEventArgs e)
        {
            int start = textBox1.SelectionStart;
            int length = textBox1.SelectionLength;
            if (length == 0)
            {
                string newstring = textBox1.Text.Insert(start, "_{}");
                textBox1.Text = newstring;
                textBox1.Focus();
                textBox1.SelectionStart = start + 2;
            }
        }

        private void Pm_Click(object sender, RoutedEventArgs e)
        {
            int start = textBox1.SelectionStart;
            int length = textBox1.SelectionLength;
            if (length == 0)
            {
                string newstring = textBox1.Text.Insert(start, "\\pm ");
                textBox1.Text = newstring;
                textBox1.Focus();
                textBox1.SelectionStart = start + 4;
            }
        }

        private void Cdot_Click(object sender, RoutedEventArgs e)
        {
            int start = textBox1.SelectionStart;
            int length = textBox1.SelectionLength;
            if (length == 0)
            {
                string newstring = textBox1.Text.Insert(start, "\\cdot ");
                textBox1.Text = newstring;
                textBox1.Focus();
                textBox1.SelectionStart = start + 6;
            }
        }
    }
}
