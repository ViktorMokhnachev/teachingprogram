﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Excel = Microsoft.Office.Interop.Excel;

namespace TeachingProgram
{
    /// <summary>
    /// Логика взаимодействия для Window2.xaml
    /// </summary>
    public partial class Window2 : Window
    {
        public Window2()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Excel.Application excel = new Excel.Application(); //создаем COM-объект Excel

            Object formula, result; //переменные для хранения формулы и результата

            formula = tbFormula.Text; //берем формулу

            result = excel._Evaluate(formula); //вычисляем формулу

            MessageBox.Show(result.ToString()); //выводим результат

            excel.Quit(); //закрываем Excel
        }
    }
}
