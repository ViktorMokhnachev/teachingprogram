﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using NCalc;
using MathExpressionsNET;

namespace TeachingProgram
{
    /// <summary>
    /// Логика взаимодействия для Window4.xaml
    /// </summary>
    public partial class Window4 : Window
    {
        public Window4()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            NCalc.Expression exp = new NCalc.Expression(tb1.Text.Replace('\'', '´'));
            if (exp.HasErrors())
            {
                MessageBox.Show("Неверное выражение");
            }
            else
            {
                try
                {
                    int n = 10;
                    for (int i = 0; i < n; i++)
                    {
                        int s0 = i;
                        int ss = n;
                        int s = s0 + ss;
                        exp.Parameters["a_"] = s0;
                        exp.Parameters["b´"] = ss;
                        exp.Parameters["c"] = s;
                        rtb.AppendText(exp.Evaluate().ToString() + "\n");
                    }
                }
                catch (Exception exe)
                {
                    MessageBox.Show(exe.Message);
                }
            }

            
        }
    }
}
