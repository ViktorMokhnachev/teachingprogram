﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TeachingProgram
{
    /// <summary>
    /// Логика взаимодействия для Window3.xaml
    /// </summary>
    public partial class Window3 : Window
    {
        public Window3()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Type scriptType = Type.GetTypeFromCLSID(Guid.Parse("0E59F1D5-1FBE-11D0-8FF2-00A0D10038BC"));
            dynamic obj = Activator.CreateInstance(scriptType, false);
            obj.Language = "javascript";
            
            string str = null;

            try
            {
                if (screen.Text.Contains('='))
                {
                    string[] strings = screen.Text.Split('=');
                    string res1 = obj.Eval(
                        "a=4;" +
                        "b´=1;" +
                        "v=0;" +
                        strings[0].Replace('\'', '´')).ToString();
                    string res2 = obj.Eval(
                        "a=4;" +
                        "b´=1;" +
                        "v=0;" +
                        strings[1].Replace('\'', '´')).ToString();
                    screen.Text = (res1.Equals(res2)).ToString();
                }
                else
                {
                    var res = obj.Eval(
                        "sqrt = Math.sqrt;" +
                        "pow = Math.pow;" +
                        "a=4;" +
                        "b´=100;" +
                        "α = 2;" +
                        "v=0;" +
                        "sin = Math.sin;" +
                        "cos = Math.cos;" +
                         screen.Text.Replace('\'', '´'));
                    str = Convert.ToString(res);
                    screen.Text = screen.Text + "=" + str;
                }
            }
            catch (SystemException)
            {
                screen.Text = "Syntax Error";
            }
        }

        private void screen_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (screen.Text.Contains("alpha"))
            {
                screen.Text = screen.Text.Replace("alpha", "α");
            }
            if (screen.Text.Contains("eta"))
            {
                screen.Text = screen.Text.Replace("eta", "");
            }
        }
        
    }
}
