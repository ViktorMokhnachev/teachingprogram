﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Patulas
{
    /// <summary>
    /// Логика взаимодействия для Slide3в_1.xaml
    /// </summary>
    public partial class Slide3c_1 : Page
    {
        private Slide1_3 slideElements;
        private ListViewItem selected;
        private AnswerChecker checker;
        private XmlDataAdapter taskAnswer;
        private int Errors = 0;
        public int n;
        public List<string> actions;
        public string situation;
        public int situationID;

        public Slide3c_1(int n, int id, string text, Slide1_3 slideElements, ListViewItem selected)
        {
            InitializeComponent();
            situation = text;
            this.slideElements = slideElements;
            this.selected = selected;
            this.n = n;
            situationID = id;
            Situation.Text = "Ситуация " + (id + 1) + ". " + situation;
            taskAnswer = new XmlDataAdapter("task/answers/answers" + n + ".xml");
            AnswerPattern.Text = taskAnswer.GetAnswerPattern();
            checker = new AnswerChecker(taskAnswer, id);
        }

        private void BtnCheck_Click(object sender, RoutedEventArgs e)
        {
            bool correct = checker.CheckTextAnswer(AnswerField.Text);
            if (correct)
            {
                selected.IsEnabled = false;
                Slide3c_2 next = new Slide3c_2(slideElements, taskAnswer);
                MainWindow.mainWindow.OpenNewPage(next);
            }
            else
            {
                Errors++;
                if (Errors == 1)
                {
                    Slide2 next = new Slide2(n, this);
                    MainWindow.mainWindow.OpenNewPage(next);
                }
                if (Errors == 2)
                {
                    Slide3a_1 next = new Slide3a_1(this);
                    MainWindow.mainWindow.OpenNewPage(next);
                }
            }
        }
    }
}
