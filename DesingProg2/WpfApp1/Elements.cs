﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Patulas
{
    /*
     * Вспомогательный класс, представляющий перечень элементов знаний
     */
    class Elements
    {
        public string Element { get; set; }
        public int UnitID { get; set; }
        public int TopicID { get; set; }
        public int ElementID { get; set; }
        public XmlDataAdapter elements;
        private int unit, topic;

        public Elements()
        {
            //пустой конструктор по умолчанию используется при формировании таблицы со списком элементов знаний
        }

        public Elements(int UnitID, int TopicID)
        {
            elements = new XmlDataAdapter("elements/elements.xml");
            unit = UnitID;
            topic = TopicID;
        }

        public Elements(int unitID)
        {
            elements = new XmlDataAdapter("elements/elements.xml");
            unit = unitID;
        }

        public List<string> getElements()
        {
            List<string> elementItems = new List<string>();
            elementItems = elements.getElements(unit, topic);
            return elementItems;
        }
    }
}
