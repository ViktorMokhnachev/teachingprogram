﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Patulas
{
    /// <summary>
    /// Логика взаимодействия для knowledge.xaml
    /// </summary>
    public partial class knowledge : UserControl
    {

        public knowledge(string uid)
        {
            InitializeComponent();
            //очищаем ListView для вывода элемнетов знаний
            Table.Items.Clear();
            //создаём список под элементы знаний
            List<string> values = new List<string>();
            //получаем входящие id
            //если у раздела есть темы, их будет 2, разделённых пробелом, иначе - 1
            string[] ids = uid.Split(' ');
            int UnitID, TopicID;
            //вспомогательный объект для получения перечня элементов знаний
            Elements elements;
            //id раздела
            UnitID = Convert.ToInt32(ids[0]);
            //id темы, если у раздел нет тем, останется -1
            TopicID = -1;
            //название выбранной темы (раздела), отображаемое вверху окна
            string topic;
            //если темы есть
            if (ids.Length == 2)
            {
                TopicID = Convert.ToInt32(ids[1]);
                //получаем список элементов знаний, относящихся к этой теме
                elements = new Elements(UnitID, TopicID);
                //получаем выбранную тему
                topic = elements.elements.getTopics(UnitID)[TopicID];
            }
            //если в разделе тем нет
            else
            {   //получаем список элементов знаний, относящихся к этому разделу
                elements = new Elements(UnitID);
                //считаем раздел темой
                topic = elements.elements.getUnits()[UnitID];
            }
            //выводим название выбранной темы (раздела)
            TopicName.Text = topic;
            //получаем список элементов знаний
            values = elements.getElements();
            //создаём список вспомогательных объектов, представляющих перечень элементов знаний в таблице
            List<Elements> elementsList = new List<Elements>();
            for (int i = 0; i < values.Count; i++)
            {
                Elements elementItem = new Elements();
                elementItem.ElementID = i;
                elementItem.UnitID = UnitID;
                elementItem.Element = values[i];
                if (ids.Length == 2)
                {
                    elementItem.TopicID = TopicID;
                }
                elementsList.Add(elementItem);
            }
            //источник записей в таблице
            List<ListViewItem> elementItems = new List<ListViewItem>();
            for (int i = 0; i < values.Count; i++)
            {
                ListViewItem item = new ListViewItem();
                item.Content = values[i];
                item.FontSize = 20;
                item.DataContext = elementsList[i];
                item.PreviewMouseUp += BtnOpenElement_Click;
                elementItems.Add(item);
            }
            Table.ItemsSource = elementItems;
        }

        //открытие элемента знаний
        private void BtnOpenElement_Click(object sender, RoutedEventArgs e)
        {
            //получаем элемент, на который нажали
            ListViewItem item = (ListViewItem)sender;
            //получаем вспомогательный объект из этого элемента
            Elements elementItem = (Elements)item.DataContext;
            string element = elementItem.Element;
            int UnitID = elementItem.UnitID;
            int TopicID = elementItem.TopicID;
            int ElementID = elementItem.ElementID;
            XmlDataAdapter ElementSettings = new XmlDataAdapter("elements/elements.xml");
            //открываем слайд с выбранным элементом знаний
            openElementApplyingPage(UnitID, TopicID, ElementID, ElementSettings, element);
        }

        private void openElementApplyingPage(int UnitID, int TopicID, int ElementID, XmlDataAdapter elements, string element)
        {
            //номер задания, которое нужно открыть
            int n = elements.getTaskNumber(UnitID, TopicID, ElementID);
            string genitive = elements.GetElementGenitiveByTaskID(n);
            Slide1_2 slide = new Slide1_2(n, element, genitive);
            MainWindow.mainWindow.OpenNewPage(slide);
        }
    }
}
