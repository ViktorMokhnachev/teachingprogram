﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Patulas
{
    /// <summary>
    /// Логика взаимодействия для SlideNotice4.xaml
    /// </summary>
    public partial class Slide2_1 : Page
    {
        private Slide3c_1 next;

        public Slide2_1(Slide3c_1 slide, string genitive, List<string> actions)
        {
            InitializeComponent();
            ElementGenitive.Text = genitive;
            next = slide;
            next.actions = actions;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.mainWindow.OpenNewPage(next);
        }
    }
}
