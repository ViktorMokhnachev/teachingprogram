﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Patulas
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private XmlDataAdapter menusettings;
        private List<ListViewItem> unitItems;
        public static MainWindow mainWindow;

        public MainWindow()
        {
            InitializeComponent();
            InputLanguageManager.SetInputLanguage(this, System.Globalization.CultureInfo.CurrentCulture);
            mainWindow = this;
            menusettings = new XmlDataAdapter("menusettings.xml");
            ListViewMenu.Items.Clear();
            MenuSelectTopic.Items.Clear();
            getUnits();
            getTopics();
        }


        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void ButtonOpenMenu_Click(object sender, RoutedEventArgs e)
        {
            ButtonCloseMenu.Visibility = Visibility.Visible;
            ButtonOpenMenu.Visibility = Visibility.Collapsed;
        }

        private void ButtonCloseMenu_Click(object sender, RoutedEventArgs e)
        {
            ButtonCloseMenu.Visibility = Visibility.Collapsed;
            ButtonOpenMenu.Visibility = Visibility.Visible;
        }

        private void getUnits()
        {
            List<string> units = menusettings.getUnits();
            unitItems = new List<ListViewItem>(units.Count);
            List<string> icons = menusettings.getIcons();
            foreach (string unit in units)
            {
                TextBlock text = new TextBlock();
                text.Text = unit;
                text.VerticalAlignment = VerticalAlignment.Center;
                text.Margin = new Thickness(20, 10, 0, 0);
                StackPanel stackPanel = new StackPanel();
                stackPanel.Orientation = Orientation.Horizontal;
                try
                {
                    MaterialDesignThemes.Wpf.PackIcon icon = new MaterialDesignThemes.Wpf.PackIcon();
                    icon.Kind = (MaterialDesignThemes.Wpf.PackIconKind)Enum.Parse(typeof(MaterialDesignThemes.Wpf.PackIconKind), icons[units.IndexOf(unit)]);
                    icon.Height = 25;
                    icon.Width = 25;
                    icon.Margin = new Thickness(10);
                    
                    stackPanel.Children.Add(icon);
                    stackPanel.Children.Add(text);
                }
                catch (ArgumentException)
                {
                    MessageBox.Show("Иконка" + icons[units.IndexOf(unit)] + " не найдена.", "Иконка не найдена", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                ListViewItem listViewItem = new ListViewItem();
                listViewItem.Height = 60;
                listViewItem.Content = stackPanel;
                ContextMenu menu = new ContextMenu();

                ListViewMenu.Items.Add(listViewItem);
                unitItems.Add(listViewItem);
            }
        }

        private void getTopics()
        {
            foreach (ListViewItem unit in unitItems)
            {
                int i = unitItems.IndexOf(unit);
                List<string> topics = menusettings.getTopics(i);
                if (topics[0] != "")
                {
                    unit.PreviewMouseUp += ListViewItem_PreviewMouseUp;
                    ContextMenu menu = new ContextMenu();
                    menu.Background = Brushes.Black;
                    BrushConverter brushConverter = new BrushConverter();
                    menu.Foreground = new SolidColorBrush(Color.FromArgb(255, 92, 153, 214));
                    foreach (string topic in topics)
                    {
                        int j = topics.IndexOf(topic);
                        MenuItem item = new MenuItem();
                        item.Header = topic;
                        item.Uid = i + " " + j;
                        item.Click += OpenTopic;
                        menu.Items.Add(item);
                    }
                    unit.ContextMenu = menu;
                }
                else
                {
                    unit.Uid = i + "";
                    unit.PreviewMouseUp += OpenUnit;
                }
            }
        }

        private void ListViewItem_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            ListViewItem clicked = (ListViewItem)sender;
            clicked.ContextMenu.IsOpen = true;
        }

        private void OpenUnit(object sender, RoutedEventArgs e)
        {
            ListViewItem selected = (ListViewItem)sender;
            String uid = selected.Uid;
            knowledge taskView = new knowledge(uid);
            taskView.Height = TaskView.Height;
            taskView.Width = TaskView.Width;
            TaskView.Content = taskView;

        }

        private void OpenTopic(object sender, RoutedEventArgs e)
        {
            MenuItem selected = (MenuItem)sender;
            String uid = selected.Uid;
            knowledge taskView = new knowledge(uid);
            taskView.Height = TaskView.Height;
            taskView.Width = TaskView.Width;
            TaskView.Content = taskView;

        }

        public void OpenNewPage(Page page)
        {
            page.Height = TaskView.Height;
            page.Width = TaskView.Width;
            TaskView.Content = page;
        }
    }

    internal class UserControlHome : UserControl
    {
    }

    internal class UserControlCreate : UserControl
    {
    }
}
