﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Patulas
{
    /// <summary>
    /// Логика взаимодействия для Slide1.xaml
    /// </summary>
    public partial class Slide1_2 : Page
    {
        private int n;
        private List<string> applyList;
        private int correctID, formulationID, researchID;
        private List<RadioButton> radioButtons;
        private string genitive;

        //n - номер выбранного элемента знаний (совпадает с номером задания)
        public Slide1_2(int n, string element, string genitive)
        {
            InitializeComponent();
            this.n = n;
            this.genitive = genitive;
            XmlDataAdapter applying = new XmlDataAdapter("elements/applying.xml");
            applyList = applying.GetApply(n);
            string correctApply = applyList[0];
            string formulation = applyList[1];
            string research = applyList[2];
            applyList = Utils.shuffleList(applyList);
            correctID = applyList.IndexOf(correctApply);
            formulationID = applyList.IndexOf(formulation);
            researchID = applyList.IndexOf(research);
            radioButtons = new List<RadioButton>();
            foreach (UIElement control in GridMain.Children)
            {
                if (control.GetType().ToString().IndexOf("RadioButton") > -1)
                {
                    radioButtons.Add((RadioButton)control);
                }
            }
            radioButtons[correctID].Tag = "correct";
            radioButtons[formulationID].Tag = "formulation";
            radioButtons[researchID].Tag = "research";
            ElementNameHeader.Text = element;
            ElementName0.Text = element;
            ElementName1.Text = element;
            ApplyVariant1.Text = applyList[0];
            ApplyVariant2.Text = applyList[1];
            ApplyVariant3.Text = applyList[2];
        }

        private void RadioButton_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            BtnCheck.IsEnabled = true;
        }

        private void BtnCheck_Click(object sender, RoutedEventArgs e)
        {
            int id = -1;
            foreach (RadioButton radioButton in radioButtons)
            {
                if (radioButton.IsChecked == true)
                {
                    id = radioButtons.IndexOf(radioButton);
                    break;
                }
            }

            switch (radioButtons[id].Tag)
            {
                case "correct":
                    Slide1_3 next = new Slide1_3(n);
                    MainWindow.mainWindow.OpenNewPage(next);
                    break;
                case "formulation":
                    Slide1_2_1 formulation = new Slide1_2_1(this);
                    MainWindow.mainWindow.OpenNewPage(formulation);
                    break;
                case "research":
                    Slide1_2_2 research = new Slide1_2_2(this, genitive);
                    MainWindow.mainWindow.OpenNewPage(research);
                    break;
            }
        }
    }
}
