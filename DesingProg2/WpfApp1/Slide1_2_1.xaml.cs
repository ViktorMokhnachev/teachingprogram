﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Patulas
{
    /// <summary>
    /// Логика взаимодействия для SlideNotice.xaml
    /// </summary>
    public partial class Slide1_2_1 : Page
    {
        private Slide1_2 previous;

        public Slide1_2_1(Slide1_2 slide)
        {
            InitializeComponent();
            previous = slide;
        }

        private void BtnTryAgain_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.mainWindow.OpenNewPage(previous);
        }
    }
}
