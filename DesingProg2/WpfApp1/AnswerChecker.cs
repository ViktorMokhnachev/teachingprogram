﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Patulas
{
    public class AnswerChecker
    {
        //список обязательных ключевых слов
        //в каждом элементе списка хранится одно ключевое слово
        //ответ должен содержать все обязательные ключевые слова, иначе ответ будет считаться неверным
        private List<string> keywordsRequired;
        //массив списков необязательных ключевых слов
        //в каждом элементе массива хранится список ключевых слов с одинаковым id
        //хотя бы одно из этих слов должно содержаться в ответе, иначе ответ будет считаться неверным
        private List<string>[] keywordsOptional;
        //список слов, которых не должно быть в ответе
        //наличие хотя бы одного из этих слов в ответе означает, что ответ неверен
        private List<string> keywordsNotRequired;

        //конструктор для проверки простого текстового ответа
        public AnswerChecker(XmlDataAdapter taskAnswer, int situationId)
        {
            keywordsRequired = taskAnswer.GetKeywordsRequired(situationId);
            keywordsOptional = taskAnswer.GetKeywordsOptional(situationId);
            keywordsNotRequired = taskAnswer.GetKeywordsNotRequired(situationId);
        }

        //конструктор для проверки текстового ответа при разборе по действиям
        public AnswerChecker(XmlDataAdapter taskAnswer, int situationId, int actionID)
        {
            keywordsRequired = taskAnswer.GetActionKeywordsRequired(situationId, actionID);
            keywordsOptional = taskAnswer.GetActionKeywordsOptional(situationId, actionID);
            keywordsNotRequired = taskAnswer.GetActionKeywordsNotRequired(situationId, actionID);
        }

        public bool CheckTextAnswer(string answer)
        {
            answer = answer.ToLower().Replace('ё', 'е');
            answer = Regex.Replace(answer, "\\W", "");
            bool correct = true;
            foreach (string keyword in keywordsRequired)
            {
                if (!answer.Contains(keyword))
                {
                    correct = false;
                    break;
                }
            }
            if (!correct)
            {
                //ответ неверен, дальше не проверяем
                MessageBox.Show("Ответ неверен, дальше не проверяем");
                return false;
            }
            else
            {
                for (int i = 0; i < keywordsOptional.Length; i++)
                {
                    bool contains = false;
                    foreach (string keyword in keywordsOptional[i])
                    {
                        if (answer.Contains(keyword))
                        {
                            contains = true;
                            break;
                        }
                    }
                    correct = contains;
                }
            }
            if (!correct)
            {
                //ответ не верен
                MessageBox.Show("Ответ неверен");
                return false;
            }
            else
            {
                foreach (string keyword in keywordsNotRequired)
                {
                    if (answer.Contains(keyword))
                    {
                        correct = false;
                        break;
                    }
                }
            }
            if (!correct)
            {
                //ответ не верен
                MessageBox.Show("Ответ неверен, потому что содержит стоп-слова");
                return false;
            }
            else
            {
                MessageBox.Show("Ответ верен");
                return true;
            }
        }
    }
}
