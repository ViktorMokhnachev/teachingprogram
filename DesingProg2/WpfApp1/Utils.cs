﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patulas
{
    public static class Utils
    {
        public static List<string> shuffleList(List<string> list)
        {
            List<string> shuffled = new List<string>(list.Capacity);
            for (int i = 0; i < list.Count; i++)
            {
                shuffled.Add(list[i]);
            }
            Random rnd = new Random();
            for (int i = 0; i < shuffled.Count; i++)
            {
                string tmp = shuffled[i];
                shuffled.RemoveAt(i);
                shuffled.Insert(rnd.Next(shuffled.Count), tmp);
            }
            return shuffled;
        }
    }
}
