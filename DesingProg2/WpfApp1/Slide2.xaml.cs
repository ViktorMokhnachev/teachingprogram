﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Patulas
{
    /// <summary>
    /// Логика взаимодействия для Slide2.xaml
    /// </summary>
    public partial class Slide2 : Page
    {
        private int id, n;
        private List<TextBox> answers = new List<TextBox>();
        private List<TextBlock> actionItems = new List<TextBlock>();
        private List<Label> actionLabels = new List<Label>();
        private List<Label> answersLabels = new List<Label>();
        private List<string> actions;
        private Dictionary<int, string> letters = new Dictionary<int, string>
        {
            [0] = "А",
            [1] = "Б",
            [2] = "В",
            [3] = "Г",
            [4] = "Д",
            [5] = "Е"
        };
        private List<int> correctAnswerIds;
        private string correctAnswer;
        private Slide3c_1 source;

        public Slide2(int id, Slide3c_1 source)
        {
            InitializeComponent();
            this.id = id;
            this.source = source;
            foreach (UIElement control in GridAnswers.Children)
            {
                if (control is TextBox)
                {
                    answers.Add((TextBox)control);
                }
                if (control is Label)
                {
                    answersLabels.Add((Label)control);
                }
            }
            foreach (UIElement control in GridScroll.Children)
            {
                if (control is TextBlock)
                {
                    actionItems.Add((TextBlock)control);
                }
                if (control is Label)
                {
                    actionLabels.Add((Label)control);
                }
            }
            XmlDataAdapter actions = new XmlDataAdapter("elements/actions.xml");
            ActionName.Text = actions.GetActionElement(id);
            this.actions = actions.GetActions(id);
            n = this.actions.Count;
            int m = actionItems.Count;
            if (n < m)
            {
                for (int i = m - 1; i >= n; i--)
                {
                    GridScroll.Children.Remove(actionLabels[i]);
                    GridScroll.Children.Remove(actionItems[i]);
                }
                for (int i = m - 2; i >= n - 1; i--)
                {
                    GridAnswers.Children.Remove(answersLabels[i]);
                    GridAnswers.Children.Remove(answers[i]);
                    answers.Remove(answers[i]);
                }
            }
            correctAnswerIds = new List<int>(this.actions.Count - 2);
            List<string> shuffledActions = Utils.shuffleList(this.actions);
            string extra = this.actions[this.actions.Count - 1];
            foreach (string action in this.actions)
            {
                foreach (string shuffledAction in shuffledActions)
                {
                    if (action == shuffledAction && action != extra)
                    {
                        int i = shuffledActions.IndexOf(shuffledAction);
                        {
                            correctAnswerIds.Add(i);
                        }
                        break;
                    }
                }
            }
            for (int i = 0; i < this.actions.Count; i++)
            {
                actionItems[i].Text = shuffledActions[i];
            }
            StringBuilder builder = new StringBuilder();
            foreach (int i in correctAnswerIds)
            {
                builder.Append(letters[i]);
            }
            correctAnswer = builder.ToString();
        }

        private void BtnCheck_Click(object sender, RoutedEventArgs e)
        {
            bool correct;
            string answer;
            StringBuilder builder = new StringBuilder();
            foreach (TextBox textbox in answers)
            {
                builder.Append(textbox.Text);
            }
            answer = builder.ToString().ToUpper();
            correct = answer == correctAnswer;
            if (!correct)
            {
                MessageBox.Show("Последовательность неверная");
            }
            else
            {
                MessageBox.Show("Последовательность верная");
                XmlDataAdapter elements = new XmlDataAdapter("elements/elements.xml");
                actions.Remove(actions.Last());
                string genitive = elements.GetElementGenitiveByTaskID(id);
                Slide2_1 next = new Slide2_1(source, genitive, actions);
                MainWindow.mainWindow.OpenNewPage(next);
            }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            bool allAnswerFieldsAreFilled = true;
            foreach (TextBox answer in answers)
            {
                if (answer.Text == "")
                {
                    allAnswerFieldsAreFilled = false;
                    break;
                }
            }
            BtnCheck.IsEnabled = allAnswerFieldsAreFilled;
        }
    }
}
