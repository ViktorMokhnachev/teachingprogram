﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Patulas
{
    /// <summary>
    /// Логика взаимодействия для Slide3б_1_1.xaml
    /// </summary>
    public partial class Slide3c_2 : Page
    {
        private Slide1_3 slideElements;
        public List<Button> buttons = new List<Button>();
        public List<TextBlock> situations = new List<TextBlock>();
        public List<TextBox> answers = new List<TextBox>();
        private XmlDataAdapter taskAnswer;

        public Slide3c_2(Slide1_3 slideElements, XmlDataAdapter taskAnswer)
        {
            InitializeComponent();
            this.slideElements = slideElements;
            this.taskAnswer = taskAnswer;
            foreach (UIElement control in GridMain.Children)
            {
                if (control is Button)
                {
                    buttons.Add((Button)control);
                    if (control.Uid != "2")
                    {
                        control.Visibility = Visibility.Hidden;
                    }
                }
                if (control is TextBlock)
                {
                    situations.Add((TextBlock)control);
                    control.Visibility = Visibility.Hidden;
                }
                if (control is TextBox)
                {
                    answers.Add((TextBox)control);
                    control.Visibility = Visibility.Hidden;
                }
            }
        }

        private void BtnSelect_Click(object sender, RoutedEventArgs e)
        {
            Button clicked = (Button)sender;
            slideElements.situationID = Convert.ToInt32(clicked.Uid);
            slideElements.nextSlide = this;
            slideElements.Header.Text = "Выберите ситуацию";
            slideElements.SelectFirstSituation.Visibility = Visibility.Hidden;
            MainWindow.mainWindow.OpenNewPage(slideElements);
        }

        private void Answer_TextChanged(object sender, TextChangedEventArgs e)
        {
            bool allAnswerFieldsAreFilled = true;
            foreach (TextBox answer in answers)
            {
                if (answer.Text == "")
                {
                    allAnswerFieldsAreFilled = false;
                    break;
                }
            }
            BtnCheck.IsEnabled = allAnswerFieldsAreFilled;
        }

        private void BtnCheck_Click(object sender, RoutedEventArgs e)
        {
            AnswerChecker[] checker = new AnswerChecker[4];
            bool correct = true;
            for (int i = 0; i < checker.Length; i++)
            {
                checker[i] = new AnswerChecker(taskAnswer, Convert.ToInt32(answers[i].Uid));
                if (!checker[i].CheckTextAnswer(answers[i].Text))
                {
                    correct = false;
                }
            }
            if (correct)
            {
                MessageBox.Show("Вы ответили правильно!");
                //задание на оценку
            }
            else
            {
                MessageBox.Show("Вы ответили неправильно!");
            }
        }
    }
}
