﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Patulas
{
    /// <summary>
    /// Логика взаимодействия для Slide1.xaml
    /// </summary>
    public partial class Slide1_3 : Page
    {
        private List<string> situations;
        private int n;
        public int situationID = 1;
        public Page nextSlide;

        public Slide1_3(int n)
        {
            InitializeComponent();
            this.n = n;
            XmlDataAdapter taskFile = new XmlDataAdapter("task/task" + n + ".xml");
            string text = taskFile.GetTaskText();
            TaskText.Text = text;
            situations = taskFile.GetTaskSituations();
            foreach (string situation in situations)
            {
                ListViewItem item = new ListViewItem();
                item.Content = situation;
                item.Uid = situations.IndexOf(situation) + "";
                item.PreviewMouseUp += ListViewItem_PreviewMouseUp;
                ContentSituations.Items.Add(item);
            }
        }

        private void ListViewItem_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            ListViewItem clicked = (ListViewItem)sender;
            int id = Convert.ToInt32(clicked.Uid);
            string situation = situations[id];
            if (situationID == 1)
            {
                Slide3c_1 next = new Slide3c_1(n, id, situation, this, clicked);
                MainWindow.mainWindow.OpenNewPage(next);
            }
            else
            {
                Int32[] ids = { 2, 3, 4, 5 };
                if (ids.Contains(situationID))
                {
                    Slide3c_2 next = (Slide3c_2)nextSlide;
                    next.buttons[situationID - 2].Visibility = Visibility.Hidden;
                    next.situations[situationID - 2].Visibility = Visibility.Visible;
                    next.situations[situationID - 2].Text = situations[id];
                    next.answers[situationID - 2].Visibility = Visibility.Visible;
                    next.answers[situationID - 2].Uid = id + "";
                    if (situationID != 5)
                    {
                        next.buttons[situationID - 1].Visibility = Visibility.Visible;
                    }
                    clicked.IsEnabled = false;
                    MainWindow.mainWindow.OpenNewPage(next);
                }
            }
        }
    }
}
