﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Patulas
{
    /// <summary>
    /// Логика взаимодействия для Slide3a_1.xaml
    /// </summary>
    public partial class Slide3a_1 : Page
    {
        private List<TextBox> answerFields = new List<TextBox>();
        private List<string> answers;
        private Slide3c_1 source;

        public Slide3a_1(Slide3c_1 source)
        {
            InitializeComponent();
            List<TextBlock> actionsText = new List<TextBlock>(source.actions.Count);
            this.source = source;
            foreach (UIElement control in GridMain.Children)
            {
                if (control is TextBlock)
                {
                    actionsText.Add((TextBlock)control);
                }
                if (control is TextBox)
                {
                    answerFields.Add((TextBox)control);
                }
            }
            for (int i = 0; i < source.actions.Count; i++)
            {
                actionsText[i].Text = source.actions[i];
            }
            SituationText.Text = source.situation;
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            bool allAnswerFieldsAreFilled = true;
            foreach (TextBox answer in answerFields)
            {
                if (answer.Text == "")
                {
                    allAnswerFieldsAreFilled = false;
                    break;
                }
            }
            BtnCheck.IsEnabled = allAnswerFieldsAreFilled;
        }

        private void BtnCheck_Click(object sender, RoutedEventArgs e)
        {
            answers = new List<string>();
            foreach (TextBox textbox in answerFields)
            {
                answers.Add(textbox.Text);
            }
            XmlDataAdapter actionAnswers = new XmlDataAdapter("task/answers/answers" + source.n + ".xml");
            List<AnswerChecker> checkers = new List<AnswerChecker>(answerFields.Count);
            for (int i = 0; i < checkers.Capacity - 1; i++)
            {
                checkers.Add(new AnswerChecker(actionAnswers, source.situationID, i));
            }
            checkers.Add(new AnswerChecker(actionAnswers, source.situationID));
            bool allCorrect = true ;
            List<bool> correctAnswers = new List<bool>();
            foreach (AnswerChecker checker in checkers)
            {
                correctAnswers.Add(checker.CheckTextAnswer(answers[checkers.IndexOf(checker)]));
            }
            foreach (bool correct in correctAnswers)
            {
                if (!correct)
                {
                    allCorrect = false;
                    break;
                }
            }
            if (allCorrect)
            {
                MessageBox.Show("Всё правильно");
            }
            else
            {
                MessageBox.Show("Не всё правильно");
            }
        }
    }
}
