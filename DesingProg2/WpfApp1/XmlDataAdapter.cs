﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Resources;
using System.Xml.Linq;

namespace Patulas
{
    public class XmlDataAdapter
    {
        XDocument XmlFile;

        public XmlDataAdapter(string path)
        {
            var xd = new XDocument();
            var uri = new Uri(path, UriKind.Relative);
            var rs = Application.GetResourceStream(uri);
            if (rs == null)
                throw new FileNotFoundException("Cannot find resource " + path);
            using (var stream = rs.Stream)
            using (var tr = new StreamReader(stream))
                XmlFile = XDocument.Load(tr);
        }

        public List<string> getUnits()
        {
            List<string> units = new List<string>();
            foreach (XElement unit in XmlFile.Element("units").Elements())
            {
                units.Add(unit.Attribute("name").Value);
            }
            return units;
        }

        public List<string> getIcons()
        {
            List<string> icons = new List<string>();
            foreach (XElement unit in XmlFile.Element("units").Elements())
            {
                icons.Add(unit.Attribute("icon").Value);
            }
            return icons;
        }

        public List<string> getTopics(int UnitID)
        {
            List<string> topics = new List<string>();
            foreach (XElement unit in XmlFile.Element("units").Elements())
            {
                if (Convert.ToInt32(unit.Attribute("id").Value) == UnitID)
                {
                    foreach (XElement topic in unit.Elements())
                    {
                        topics.Add(topic.Attribute("name").Value);
                    }
                }
            }
            return topics;
        }

        public List<string> getElements(int UnitID, int TopicID)
        {
            List<string> elements = new List<string>();
            foreach (XElement unit in XmlFile.Element("units").Elements())
            {
                if (Convert.ToInt32(unit.Attribute("id").Value) == UnitID)
                {
                    foreach (XElement topic in unit.Elements())
                    {
                        if (Convert.ToInt32(topic.Attribute("id").Value) == TopicID)
                        {
                            foreach (XElement element in topic.Elements())
                            {
                                string item = element.Attribute("name").Value;
                                elements.Add(item);
                            }
                        }
                    }
                }
            }
            return elements;
        }

        public int getTaskNumber(int UnitID, int TopicID, int ElementID)
        {
            int n = -1;
            foreach (XElement unit in XmlFile.Element("units").Elements())
            {
                if (Convert.ToInt32(unit.Attribute("id").Value) == UnitID)
                {
                    foreach (XElement topic in unit.Elements())
                    {
                        if (Convert.ToInt32(topic.Attribute("id").Value) == TopicID)
                        {
                            foreach (XElement element in topic.Elements())
                            {
                                if (Convert.ToInt32(element.Attribute("id").Value) == ElementID)
                                {
                                    n = Convert.ToInt32(element.Attribute("task").Value);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            return n;
        }

        public List<string> GetApply(int n)
        {
            List<string> applyList = new List<string>(3);
            foreach (XElement element in XmlFile.Element("elements").Elements())
            {
                if (Convert.ToInt32(element.Attribute("id").Value) == n)
                {
                    foreach (XElement apply in element.Elements())
                    {
                        applyList.Add(apply.Attribute("text").Value);
                    }
                    break;
                }
            }
            return applyList;
        }

        public string GetTaskText()
        {
            return XmlFile.Element("task").Value;
        }

        public List<string> GetTaskSituations()
        {
            List<string> situations = new List<string>();
            foreach (XElement element in XmlFile.Element("task").Elements())
            {
                if (element.Name == "situations")
                {
                    foreach (XElement situation in element.Elements())
                    {
                        situations.Add(situation.Attribute("text").Value);
                    }
                    break;
                }
            }
            return situations;
        }

        public string GetAnswerPattern()
        {
            string pattern = "";
            foreach (XElement element in XmlFile.Element("answers").Elements())
            {
                if (element.Name == "pattern")
                {
                    pattern = element.Attribute("text").Value;
                    break;
                }
            }
            return pattern;
        }

        public List<string> GetKeywordsRequired(int situation)
        {
            List<string> keywordsRequired = new List<string>();
            foreach (XElement element in XmlFile.Element("answers").Elements())
            {
                if (element.Name == "general")
                {
                    foreach (XElement keyword in element.Elements())
                    {
                        if (keyword.Attribute("situation").Value == situation.ToString())
                        {
                            if (keyword.Attribute("required").Value == "true")
                            {
                                keywordsRequired.Add(Regex.Replace(keyword.Attribute("text").Value, "\\s", ""));
                            }
                        }
                        else
                        {
                            if (Convert.ToInt32(keyword.Attribute("situation").Value) > situation)
                            {
                                break;
                            }
                        }
                    }
                }
            }
            return keywordsRequired;
        }

        public List<string>[] GetKeywordsOptional(int situation)
        {
            int n = 0;
            int id = -1;
            foreach (XElement element in XmlFile.Element("answers").Elements())
            {
                if (element.Name == "general")
                {
                    foreach (XElement keyword in element.Elements())
                    {
                        if (keyword.Attribute("situation").Value == situation.ToString())
                        {
                            if (keyword.Attribute("required").Value == "optional")
                            {
                                int currentID = Convert.ToInt32(keyword.Attribute("id").Value);
                                if (currentID != id)
                                {
                                    id = currentID;
                                    n++;
                                }
                            }
                        }
                        else
                        {
                            if (Convert.ToInt32(keyword.Attribute("situation").Value) > situation)
                            {
                                break;
                            }
                        }
                    }
                }
            }
            List<string>[] keywordsOptional = new List<string>[n];
            List<string> keywords = null;
            id = -1;
            n = -1;
            foreach (XElement element in XmlFile.Element("answers").Elements())
            {
                if (element.Name == "general")
                {
                    foreach (XElement keyword in element.Elements())
                    {
                        if (keyword.Attribute("situation").Value == situation.ToString())
                        {
                            if (keyword.Attribute("required").Value == "optional")
                            {
                                int currentID = Convert.ToInt32(keyword.Attribute("id").Value);
                                if (currentID != id)
                                {
                                    if (n != -1)
                                    {
                                        keywordsOptional[n] = keywords;
                                    }
                                    id = currentID;
                                    n++;
                                    keywords = new List<string>();
                                }
                                keywords.Add(Regex.Replace(keyword.Attribute("text").Value, "\\s", ""));
                            }
                        }
                        else
                        {
                            if (Convert.ToInt32(keyword.Attribute("situation").Value) > situation)
                            {
                                if (n >= 0)
                                {
                                    keywordsOptional[n] = keywords;
                                }
                                break;
                            }
                        }
                    }
                }
            }
            return keywordsOptional;
        }

        public List<string> GetKeywordsNotRequired(int situation)
        {
            List<string> keywordsRequired = new List<string>();
            foreach (XElement element in XmlFile.Element("answers").Elements())
            {
                if (element.Name == "general")
                {
                    foreach (XElement keyword in element.Elements())
                    {
                        if (keyword.Attribute("situation").Value == situation.ToString())
                        {
                            if (keyword.Attribute("required").Value == "false")
                            {
                                keywordsRequired.Add(Regex.Replace(keyword.Attribute("text").Value, "\\s", ""));
                            }
                        }
                        else
                        {
                            if (Convert.ToInt32(keyword.Attribute("situation").Value) > situation)
                            {
                                break;
                            }
                        }
                    }
                }
            }
            return keywordsRequired;
        }

        public string GetActionElement(int id)
        {
            string element = "";
            foreach (XElement xelement in XmlFile.Element("actions").Elements())
            {
                if (xelement.Name == "element")
                {
                    if (xelement.Attribute("id").Value == id.ToString())
                    {
                        element = xelement.Attribute("text").Value;
                        break;
                    }
                }
            }
            return element;
        }

        public List<string> GetActions(int id)
        {
            List<string> actions = new List<string>();
            foreach (XElement xelement in XmlFile.Element("actions").Elements())
            {
                if (xelement.Name == "element")
                {
                    if (xelement.Attribute("id").Value == id.ToString())
                    {
                        foreach (XElement action in xelement.Elements())
                        {
                            actions.Add(action.Attribute("text").Value);
                        }
                        break;
                    }
                }
            }
            return actions;
        }

        public string GetElementGenitiveByTaskID(int n)
        {
            foreach (XElement unit in XmlFile.Element("units").Elements())
            {
                foreach (XElement topic in unit.Elements())
                {
                    foreach (XElement element in topic.Elements())
                    {
                        if (element.Attribute("task").Value == n.ToString())
                        {
                            return element.Attribute("genitive").Value;
                        }
                    }
                }
            }
            return "";
        }

        public List<string> GetActionKeywordsRequired(int situationID, int actionID)
        {
            List<string> keywordsRequired = new List<string>();
            foreach (XElement element in XmlFile.Element("answers").Elements())
            {
                if (element.Name == "actions")
                {
                    foreach (XElement situation in element.Elements())
                    {
                        if (situation.Attribute("id").Value == situationID.ToString())
                        {
                            foreach (XElement action in situation.Elements())
                            {
                                if (action.Attribute("id").Value == actionID.ToString())
                                {
                                    foreach (XElement keyword in action.Elements())
                                    {
                                        if (keyword.Attribute("required").Value == "true")
                                        {
                                            keywordsRequired.Add(Regex.Replace(keyword.Attribute("text").Value, "\\s", ""));
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (Convert.ToInt32(situation.Attribute("id").Value) > situationID)
                            {
                                break;
                            }
                        }
                    }
                }
            }
            return keywordsRequired;
        }

        public List<string>[] GetActionKeywordsOptional(int situationID, int actionID)
        {
            int n = 0;
            int id = -1;
            foreach (XElement element in XmlFile.Element("answers").Elements())
            {
                if (element.Name == "actions")
                {
                    foreach (XElement situation in element.Elements())
                    {
                        if (situation.Attribute("id").Value == situationID.ToString())
                        {
                            foreach (XElement action in situation.Elements())
                            {
                                if (action.Attribute("id").Value == actionID.ToString())
                                {
                                    foreach (XElement keyword in action.Elements())
                                    {
                                        if (keyword.Attribute("required").Value == "optional")
                                        {
                                            int currentID = Convert.ToInt32(keyword.Attribute("id").Value);
                                            if (currentID != id)
                                            {
                                                id = currentID;
                                                n++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (Convert.ToInt32(situation.Attribute("id").Value) > situationID)
                            {
                                break;
                            }
                        }
                }
                }
            }
            List<string>[] keywordsOptional = new List<string>[n];
            List<string> keywords = null;
            id = -1;
            n = -1;
            foreach (XElement element in XmlFile.Element("answers").Elements())
            {
                if (element.Name == "actions")
                {
                    foreach (XElement situation in element.Elements())
                    {
                        if (situation.Attribute("id").Value == situationID.ToString())
                        {
                            foreach (XElement action in situation.Elements())
                            {
                                if (action.Attribute("id").Value == actionID.ToString())
                                {
                                    foreach (XElement keyword in action.Elements())
                                    {
                                        if (keyword.Attribute("required").Value == "optional")
                                        {
                                            int currentID = Convert.ToInt32(keyword.Attribute("id").Value);
                                            if (currentID != id)
                                            {
                                                if (n != -1)
                                                {
                                                    keywordsOptional[n] = keywords;
                                                }
                                                id = currentID;
                                                n++;
                                                keywords = new List<string>();
                                            }
                                            keywords.Add(Regex.Replace(keyword.Attribute("text").Value, "\\s", ""));
                                        }
                                        if (action.Elements().Last().Equals(keyword))
                                        {
                                            keywordsOptional[n] = keywords;
                                        }
                                    }
                                }
                                else
                                {
                                    if (Convert.ToInt32(action.Attribute("id").Value) > actionID)
                                    {
                                        break;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (Convert.ToInt32(situation.Attribute("id").Value) > situationID)
                            {
                                break;
                            }
                        }
                    }
                }
            }
            return keywordsOptional;
        }

        public List<string> GetActionKeywordsNotRequired(int situationID, int actionID)
        {
            List<string> keywordsRequired = new List<string>();
            foreach (XElement element in XmlFile.Element("answers").Elements())
            {
                if (element.Name == "actions")
                {
                    foreach (XElement situation in element.Elements())
                    {
                        if (situation.Attribute("id").Value == situationID.ToString())
                        {
                            foreach (XElement action in situation.Elements())
                            {
                                if (action.Attribute("id").Value == actionID.ToString())
                                {
                                    foreach (XElement keyword in action.Elements())
                                    {
                                        if (keyword.Attribute("required").Value == "false")
                                        {
                                            keywordsRequired.Add(Regex.Replace(keyword.Attribute("text").Value, "\\s", ""));
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (Convert.ToInt32(situation.Attribute("id").Value) > situationID)
                            {
                                break;
                            }
                        }
                    }
                }
            }
            return keywordsRequired;
        }
    }
}
