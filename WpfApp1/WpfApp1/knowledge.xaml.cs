﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Логика взаимодействия для knowledge.xaml
    /// </summary>
    public partial class knowledge : Page
    {
        public knowledge(string uid)
        {
            InitializeComponent();
            List<string> values = new List<string>();
            string[] ids = uid.Split(' ');
            int UnitID, TopicID;
            Tasks task;
            UnitID = Convert.ToInt32(ids[0]);
            TopicID = -1;
            if (ids.Length == 2)
            {
                TopicID = Convert.ToInt32(ids[1]);
                task = new Tasks(UnitID, TopicID);
            }
            else
            {
                task = new Tasks(UnitID);
            }
            values = task.getElements();
            List<Tasks> source = new List<Tasks>();
            for (int i = 0; i < values.Count; i++)
            {
                Tasks taskItem = new Tasks();
                taskItem.Element = values[i];
                taskItem.UnitID = UnitID;
                if (ids.Length == 2)
                {
                    taskItem.TopicID = TopicID;
                }
                source.Add(taskItem);
            }
            Table.ItemsSource = source;
            
        }

        private void BtnOpenQuestion_Click(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            Tasks task = (Tasks)button.DataContext;
            string element = task.Element;
            int UnitID = task.UnitID;
            int TopicID = task.TopicID;
            int ElementID = -1;
            Settings ElementSettings = new Settings("elements.xml");
            List<string> elements = ElementSettings.getElements(UnitID, TopicID);
            for (int i = 0; i < elements.Count; i++)
            {
                if (elements[i] == element)
                {
                    ElementID = i;
                    break;
                }
            }
            openNewTask(UnitID, TopicID, ElementID, ElementSettings);
        }

        private void openNewTask(int UnitID, int TopicID, int ElementID, Settings elements)
        {
            //номер задания, которое нужно открыть
            int n = elements.getTaskNumber(UnitID, TopicID, ElementID);
        }
    }
}
