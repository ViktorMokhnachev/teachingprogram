﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Settings menusettings;
        List<MenuItem> unitItems;
        List<MenuItem>[] topicItems;


        public MainWindow()
        {
            InitializeComponent();
            menusettings = new Settings("menusettings.xml");
            MenuSelectTask.Items.Clear();
            getUnits();
            getTopics();
        }


        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void getUnits()
        {
            List<string> units = menusettings.getUnits();
            unitItems = new List<MenuItem>(units.Count);
            foreach (string unit in units)
            {
                MenuItem item = new MenuItem();
                item.Header = unit;
                item.Uid += units.IndexOf(unit);
                MenuSelectTask.Items.Add(item);
                unitItems.Add(item);
            }
        }

        private void getTopics()
        {
            List<string> units = menusettings.getUnits();
            topicItems = new List<MenuItem>[units.Count];
            for (int i = 0; i < units.Count; i++)
            {
                List<string> topics = menusettings.getTopics(i);
                topicItems[i] = new List<MenuItem>();
                foreach (string topic in topics)
                {
                    MenuItem unitItem = unitItems[i];
                    if (topic != "")
                    {
                        MenuItem item = new MenuItem();
                        item.Header = topic;
                        item.Click += OpenTopic;
                        item.Uid += unitItems.IndexOf(unitItem) + " " + topics.IndexOf(topic);
                        unitItem.Items.Add(item);
                        topicItems[i].Add(item);
                    }
                    else
                    {
                        unitItem.Click += OpenTopic;
                        topicItems[i].Add(unitItem);
                    }
                }
            }
        }
        
        private void SelectItemInMainMenu(object sender, MouseButtonEventArgs e)
        {
            if (sender.GetType().ToString().Contains("ListViewItem")){
                ListViewItem listViewItem = (ListViewItem)sender;
                MessageBox.Show("Click " + listViewItem.Uid);
            }
        }

        private void ListViewItem_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            ListViewItem clicked = (ListViewItem)sender;
            ListView clickedView = (ListView)clicked.Parent;
            clickedView.ContextMenu.IsOpen = true;
        }

        private void OpenTopic(object sender, RoutedEventArgs e)
        {
            MenuItem selected = (MenuItem)sender;
            String uid = selected.Uid;
            knowledge taskView = new knowledge(uid);
            taskView.Height = TaskView.Height;
            taskView.Width = TaskView.Width;
            TaskView.Content = taskView;
            
        }
    }
}
