﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Resources;
using System.Xml.Linq;

namespace WpfApp1
{
    public class Settings
    {
        XDocument settingsFile;

        public Settings(string settings)
        {
            var xd = new XDocument();
            var uri = new Uri(settings, UriKind.Relative);
            var rs = Application.GetResourceStream(uri);
            if (rs == null)
                throw new FileNotFoundException("Cannot find resource " + settings);
            using (var stream = rs.Stream)
            using (var tr = new StreamReader(stream))
            settingsFile = XDocument.Load(tr);
        }

        public List<string> getUnits()
        {
            List<string> units = new List<string>();
            foreach (XElement unit in settingsFile.Element("units").Elements())
            {
                units.Add(unit.Attribute("name").Value);
            }
            return units;
        }

        public List<string> getTopics(int UnitID)
        {
            List<string> topics = new List<string>();
            foreach (XElement unit in settingsFile.Element("units").Elements())
            {
                if (Convert.ToInt32(unit.Attribute("id").Value) == UnitID)
                {
                    foreach (XElement topic in unit.Elements())
                    {
                        topics.Add(topic.Attribute("name").Value);
                    }
                }
            }
            return topics;
        }

        public List<string> getElements(int UnitID, int TopicID)
        {
            List<string> elements = new List<string>();
            foreach (XElement unit in settingsFile.Element("units").Elements())
            {
                if (Convert.ToInt32(unit.Attribute("id").Value) == UnitID)
                {
                    foreach (XElement topic in unit.Elements())
                    {
                        if (Convert.ToInt32(topic.Attribute("id").Value) == TopicID)
                        {
                            foreach (XElement element in topic.Elements())
                            {
                                string item = element.Attribute("name").Value;
                                elements.Add(item);
                            }
                        }
                    }
                }
            }
            return elements;
        }

        public int getTaskNumber(int UnitID, int TopicID, int ElementID)
        {
            int n = -1;
            foreach (XElement unit in settingsFile.Element("units").Elements())
            {
                if (Convert.ToInt32(unit.Attribute("id").Value) == UnitID)
                {
                    foreach (XElement topic in unit.Elements())
                    {
                        if (Convert.ToInt32(topic.Attribute("id").Value) == TopicID)
                        {
                            foreach (XElement element in topic.Elements())
                            {
                                if (Convert.ToInt32(element.Attribute("id").Value) == ElementID)
                                {
                                    n = Convert.ToInt32(element.Attribute("task").Value);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            return n;
        }
    }
}
