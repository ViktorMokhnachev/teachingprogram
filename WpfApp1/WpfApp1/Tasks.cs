﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace WpfApp1
{
    class Tasks
    {
        public string Element { get; set; }
        public int UnitID { get; set; }
        public int TopicID { get; set; }
        private Settings elements;
        private int unit, topic;

        public Tasks()
        {

        }

        public Tasks(int UnitID, int TopicID)
        {
            elements = new Settings("elements.xml");
            unit = UnitID;
            topic = TopicID;
        }

        public Tasks(int unitID)
        {
            elements = new Settings("elements.xml");
            unit = unitID;
        }

        public List<string> getElements()
        {
            List<string> elementItems = new List<string>();
            elementItems = elements.getElements(unit, topic);
            return elementItems;
        }
    }
}
