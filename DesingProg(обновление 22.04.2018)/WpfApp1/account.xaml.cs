﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Data;

namespace Patulas
{
    /// <summary>
    /// Логика взаимодействия для account.xaml
    /// </summary>
    public partial class Account : Window
    {
        private int accessLevel;

        public Account()
        {
            InitializeComponent();
            Name.Text = MainWindow.get().GetCurrentUserName();
            List<Result> results = new List<Result>();
            accessLevel = MainWindow.get().GetCurrentUser().GetAccessLevel();
            if (accessLevel == 1)
            {
                WelcomeText.Text = Constants.WelcomeTextTeacher;
                List<int> users = DatabaseAdapter.GetDistinctUsersFromResults();
                for (int i = 0; i < users.Count; i++)
                {
                    int count = DatabaseAdapter.GetCountResults(users[i]);
                    for (int j = 0; j < count; j++)
                    {
                        results.Add(new Result(users[i], j));
                    }
                }
            }
            else
            {
                WelcomeText.Text = Constants.WelcomeTextStudent;
                int userID = MainWindow.get().GetCurrentUserID();
                int count = DatabaseAdapter.GetCountResults(userID);
                for (int i = 0; i < count; i++)
                {
                    results.Add(new Result(userID, i));
                }
            }
            ResultsGrid.DataContext = results;
            ResultsGrid.ItemsSource = results;
        }

        private void Logout_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.get().Logout();
            Close();
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void ResultsGrid_Loaded(object sender, RoutedEventArgs e)
        {
            if (accessLevel == 1)
            {
                ICollectionView cvs = CollectionViewSource.GetDefaultView(ResultsGrid.ItemsSource);
                if (cvs != null && cvs.CanGroup == true)
                {
                    cvs.GroupDescriptions.Clear();
                    // Указываем поле DataTable, по которому осуществляется новая группировка
                    cvs.GroupDescriptions.Add(new PropertyGroupDescription("group"));
                    cvs.GroupDescriptions.Add(new PropertyGroupDescription("name"));
                }
            }
        }
    }
}
