﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Patulas
{
    /// <summary>
    /// Логика взаимодействия для Slide1.xaml
    /// </summary>
    public partial class Slide1_2 : UserControl
    {
        private int n;
        private List<string> applyList;
        private int correctID, formulationID, researchID;
        private List<RadioButton> radioButtons;
        private string genitive;

        //n - номер выбранного элемента знаний (совпадает с номером задания)
        public Slide1_2(int n, string element, string genitive)
        {
            InitializeComponent();
            MainWindow.get().BlockButtons();
            this.n = n;
            this.genitive = genitive;
            XmlDataAdapter applying = new XmlDataAdapter(Constants.PathToApplying);
            applyList = applying.GetApply(n);
            string correctApply = applyList[0];
            string formulation = applyList[1];
            string research = applyList[2];
            applyList = Utils.shuffleList(applyList);
            correctID = applyList.IndexOf(correctApply);
            formulationID = applyList.IndexOf(formulation);
            researchID = applyList.IndexOf(research);
            radioButtons = new List<RadioButton>();
            foreach (UIElement control in GridMain.Children)
            {
                if (control.GetType().ToString().IndexOf(Constants.RadioButton) > -1)
                {
                    radioButtons.Add((RadioButton)control);
                }
            }
            radioButtons[correctID].Tag = Constants.Correct;
            radioButtons[formulationID].Tag = Constants.Formulation;
            radioButtons[researchID].Tag = Constants.Research;
            ElementNameHeader.Text = element;
            RadioButton0.Content = applyList[0];
            RadioButton1.Content = applyList[1];
            RadioButton2.Content = applyList[2];
        }

       
        private void RadioButton_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            BtnCheck.IsEnabled = true;
        }

        private void BtnCheck_Click(object sender, RoutedEventArgs e)
        {
            int id = -1;
            foreach (RadioButton radioButton in radioButtons)
            {
                if (radioButton.IsChecked == true)
                {
                    id = radioButtons.IndexOf(radioButton);
                    break;
                }
            }

            switch (radioButtons[id].Tag)
            {
                case Constants.Correct:
                    XmlDataAdapter taskFile = new XmlDataAdapter("task/task" + n + ".xml");
                    string taskType = taskFile.GetTaskType();
                    if (taskType != "table")
                    {
                        ISituations next = taskType.Contains(Constants.Text) ? new Slide1_3(n, taskFile) : (ISituations)new Slide1_3_Images(n, taskFile);
                        MainWindow.get().OpenNewSlide((UserControl)next);
                    }
                    else
                    {
                        MessageBox.Show("Неподдерживаемый тип задачи", Constants.Error, MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    break;
                case Constants.Formulation:
                    Slide1_2_1 formulation = new Slide1_2_1(this);
                    MainWindow.get().OpenNewSlide(formulation);
                    break;
                case Constants.Research:
                    Slide1_2_2 research = new Slide1_2_2(this, genitive);
                    MainWindow.get().OpenNewSlide(research);
                    break;
            }
        }
    }
}
