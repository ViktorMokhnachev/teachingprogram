﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace Patulas
{
    /// <summary>
    /// Логика взаимодействия для SlideNotice4.xaml
    /// </summary>
    public partial class Slide2_1 : UserControl
    {
        private Slide3c_1 next;
        private IActions next2;

        public Slide2_1(Slide3c_1 slide, string genitive, List<string> actions)
        {
            InitializeComponent();
            ElementGenitive.Text += genitive + ".";
            next = slide;
            next.actions = actions;
        }

        public Slide2_1(IActions slide, string genitive)
        {
            InitializeComponent();
            ElementGenitive.Text += genitive + ".";
            next2 = slide;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (next != null)
                MainWindow.get().OpenNewSlide(next);
            else
            {
                if (next2 is Slide3c_2)
                    ((Slide3c_2)next2).CorrectAnswer();
                MainWindow.get().OpenNewSlide((UserControl)next2);
            }
        }
    }
}
