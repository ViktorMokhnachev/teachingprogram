﻿using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace Patulas
{
    /// <summary>
    /// Логика взаимодействия для Slide3б_1_1.xaml
    /// </summary>
    public partial class Slide3b_1_1 : UserControl
    {
        UserControl previousSlide;

        //конструктор для задач с текстовыми ответами
        public Slide3b_1_1(int id, int situationID, string situation, XmlDataAdapter taskAnswer, UserControl previousSlide)
        {
            InitializeComponent();
            this.previousSlide = previousSlide;
            SituationNumber.Text = id.ToString();
            SituationText.Text = situation;
            Answer.Text = taskAnswer.GetFullAnswer(situationID);
        }

        //конструктор для задач с формулами
        public Slide3b_1_1(int id, int situationID, string situation, int formulasCount, XmlDataAdapter taskAnswer, UserControl previousSlide)
        {
            InitializeComponent();
            this.previousSlide = previousSlide;
            SituationNumber.Text = id.ToString();
            SituationText.Text = situation;
            if (formulasCount > 1)
            {
                StringBuilder answer = new StringBuilder();
                for (int i = 0; i < formulasCount - 1; i++)
                {
                    answer.Append(taskAnswer.GetFormulaResult(situationID, i)).Append("\n");
                }
                answer.Append(taskAnswer.GetFormulaResult(situationID, formulasCount - 1));
                Answer.Text = answer.ToString();
            }
            else
            {
                Answer.Text = taskAnswer.GetFormulaResult(situationID, 0);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (previousSlide is IActions)
                MainWindow.get().OpenNewSlide(((IActions)previousSlide).GetNextSlide());
            else
            {
                Slide3c_1 source = (Slide3c_1)previousSlide;
                MainWindow.get().OpenNewSlide(new Slide3b_1(source.n, source.slideElements, source.GetTaskType(), source.taskAnswer, source.answerType, false));
            }
        }
    }
}
