﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using Patulas.help;
using System.Speech.Synthesis;

namespace Patulas
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private XmlDataAdapter menusettings;
        private List<ListViewItem> unitItems;
        private static MainWindow mainWindow;
        public UserControl currentSlide;
        private static User currentUser;
        private static SituationImageWindow imageWindow;
        private static TaskWindow taskWindow;
        private static PatternWindow patternWindow;

        public MainWindow()
        {
            InitializeComponent();
            InputLanguageManager.SetInputLanguage(this, System.Globalization.CultureInfo.CurrentCulture);
            mainWindow = this;
            menusettings = new XmlDataAdapter(Constants.MenuSettings);
            ListViewMenu.Items.Clear();
            MenuSelectTopic.Items.Clear();
            getUnits();
            getTopics();
            BlockSideMenu();
          /* SpeechSynthesizer ss = new SpeechSynthesizer();
            ss.Volume = 100;// от 0 до 100 
            ss.Rate = 0;//от -10 до 10 
            ss.SpeakAsync("Welcome to program Patulas"); */
        }


        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
        private void ButtonOpenMenu_Click(object sender, RoutedEventArgs e)
        {
            ButtonCloseMenu.Visibility = Visibility.Visible;
            ButtonOpenMenu.Visibility = Visibility.Collapsed;
        }

        private void ButtonCloseMenu_Click(object sender, RoutedEventArgs e)
        {
            ButtonCloseMenu.Visibility = Visibility.Collapsed;
            ButtonOpenMenu.Visibility = Visibility.Visible;
        }

        private void getUnits()
        {
            List<string> units = menusettings.getUnits();
            unitItems = new List<ListViewItem>(units.Count);
            List<string> icons = menusettings.getIcons();
            foreach (string unit in units)
            {
                TextBlock text = new TextBlock();
                text.Text = unit;
                text.VerticalAlignment = VerticalAlignment.Center;
                text.Margin = new Thickness(20, 10, 0, 0);
                StackPanel stackPanel = new StackPanel();
                stackPanel.Orientation = Orientation.Horizontal;
                try
                {
                    MaterialDesignThemes.Wpf.PackIcon icon = new MaterialDesignThemes.Wpf.PackIcon();
                    icon.Kind = (MaterialDesignThemes.Wpf.PackIconKind)Enum.Parse(typeof(MaterialDesignThemes.Wpf.PackIconKind), icons[units.IndexOf(unit)]);
                    icon.Height = 25;
                    icon.Width = 25;
                    icon.Margin = new Thickness(10);

                    stackPanel.Children.Add(icon);
                    stackPanel.Children.Add(text);
                }
                catch (ArgumentException)
                {
                    MessageBox.Show(Constants.IconText + icons[units.IndexOf(unit)] + Constants.NotFound, Constants.IconNotFound, MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                ListViewItem listViewItem = new ListViewItem();
                listViewItem.Height = 60;
                listViewItem.Content = stackPanel;
                ContextMenu menu = new ContextMenu();

                ListViewMenu.Items.Add(listViewItem);
                unitItems.Add(listViewItem);
            }
        }

        private void getTopics()
        {
            foreach (ListViewItem unit in unitItems)
            {
                int i = unitItems.IndexOf(unit);
                List<string> topics = menusettings.getTopics(i);
                if (topics[0] != "")
                {
                    unit.PreviewMouseUp += ListViewItem_PreviewMouseUp;
                    ContextMenu menu = new ContextMenu();
                    menu.Background = Brushes.Black;
                    BrushConverter brushConverter = new BrushConverter();
                    menu.Foreground = new SolidColorBrush(Color.FromArgb(255, 92, 153, 214));
                    foreach (string topic in topics)
                    {
                        int j = topics.IndexOf(topic);
                        MenuItem item = new MenuItem();
                        item.Header = topic;
                        item.Uid = i + " " + j;
                        item.Click += OpenTopic;
                        menu.Items.Add(item);
                    }
                    unit.ContextMenu = menu;
                }
                else
                {
                    unit.Uid = i + "";
                    unit.PreviewMouseUp += OpenUnit;
                }
            }
        }

        internal void BlockSideMenu()
        {
            foreach (ListViewItem unit in unitItems)
            {
                unit.IsEnabled = false;
            }
        }

        internal void UnlockSideMenu()
        {
            foreach (ListViewItem unit in unitItems)
            {
                unit.IsEnabled = true;
            }
        }

        internal void BlockButtons()
        {
            BlockSideMenu();
            Exit.IsEnabled = false;
            Login.IsEnabled = false;
            Register.IsEnabled = false;
        }

        internal void UnlockButtons()
        {
            UnlockSideMenu();
            Exit.IsEnabled = true;
            Login.IsEnabled = true;
            Register.IsEnabled = true;
        }

        private void ListViewItem_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            ListViewItem clicked = (ListViewItem)sender;
            clicked.ContextMenu.IsOpen = true;
        }

        private void OpenUnit(object sender, RoutedEventArgs e)
        {
            ListViewItem selected = (ListViewItem)sender;
            String uid = selected.Uid;
            Slide1_1 taskView = new Slide1_1(uid);
            taskView.Height = TaskView.Height;
            taskView.Width = TaskView.Width;
            TaskView.Children.Add(taskView);
            currentSlide = taskView;
        }

        private void OpenTopic(object sender, RoutedEventArgs e)
        {
            MenuItem selected = (MenuItem)sender;
            String uid = selected.Uid;
            Slide1_1 taskView = new Slide1_1(uid);
            taskView.Height = TaskView.Height;
            taskView.Width = TaskView.Width;
            TaskView.Children.Add(taskView);
            currentSlide = taskView;
        }

        public void OpenNewSlide(UserControl page)
        {
            page.Height = TaskView.Height;
            page.Width = TaskView.Width;
            TaskView.Children.Clear();
            TaskView.Children.Add(page);
            currentSlide = page;
            if (imageWindow != null)
                imageWindow.Close();
            if (taskWindow != null)
                taskWindow.Close();
            if (patternWindow != null)
                patternWindow.Close();
        }

        private void Login_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Login login = new Login();
                login.Owner = this;
                login.ShowDialog();
            }
            catch (Npgsql.NpgsqlException)
            {
                return;
            }
        }

        private void Journal_Click(object sender, RoutedEventArgs e)
        {
            Account account = new Account();
            account.Owner = this;
            account.ShowDialog();
        }

        public void SetJournalHandler()
        {
            Login.Click -= Login_Click;
            Login.Click += Journal_Click;
        }

        public void SetLoginHandler()
        {
            Login.Click -= Journal_Click;
            Login.Click += Login_Click;
        }

        private void ExitAccount_Click(object sender, RoutedEventArgs e)
        {
            Logout();
        }

        public void Logout()
        {
            currentUser = null;
            UserName.Content = "";
            Login.Content = Constants.Login;
            SetLoginHandler();
            Register.Content = Constants.Register;
            SetCreateAccountHandler();
            BlockSideMenu();
            TaskView.Children.Clear();
            InfoRectangle.Visibility = Visibility.Visible;
            InfoText.Visibility = Visibility.Visible;
        }

        public void SetCreateAccountHandler()
        {
            Register.Click -= ExitAccount_Click;
            Register.Click += Register_Click;
        }

        public void SetExitAccountHandler()
        {
            Register.Click -= Register_Click;
            Register.Click += ExitAccount_Click;
        }

        private void Help_Click(object sender, RoutedEventArgs e)
        {
            Helper helper = new Helper(currentSlide);
            helper.Height = TaskView.Height;
            helper.Width = TaskView.Width;
            TaskView.Children.Clear();
            TaskView.Children.Add(helper);
        }

        private void Register_Click(object sender, RoutedEventArgs e)
        {
            Registration registration = new Registration();
            registration.Owner = this;
            try
            {
                registration.ShowDialog();
            }
            catch (Exception)
            {
                registration.Close();
            }
        }

        public static MainWindow get()
        {
            return mainWindow;
        }

        public User GetCurrentUser()
        {
            return currentUser;
        }

        public void SetUser(User user)
        {
            currentUser = user;
        }

        public string GetCurrentUserName()
        {
            if (currentUser.GetName() != "" && currentUser.GetName() != null)
                return currentUser.GetName();
            else return currentUser.GetLogin();
        }

        public int GetCurrentUserID()
        {
            return currentUser.GetID();
        }

        public static void OpenSituationImageWindow(Image image)
        {
            if (SituationImageWindow.count == 0)
            {
                imageWindow = new SituationImageWindow(image);
                imageWindow.Show();
            }
            else
            {
                imageWindow.SetNewImage(image);
            }
        }

        public static void OpenTaskWindow(string text)
        {
            if (TaskWindow.count == 0)
            {
                taskWindow = new TaskWindow(text);
                taskWindow.Show();
            }
        }

        public static void OpenPatternWindow(string text)
        {
            if (PatternWindow.count == 0)
            {
                patternWindow = new PatternWindow(text);
                patternWindow.Show();
            }
        }

        private void About_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Программу разработали студенты группы 164-381 Мохначёв В.С. и Дикусар В.Г.", "О программе");
        }
    }

    internal class UserControlHome : UserControl
    {
    }

    internal class UserControlCreate : UserControl
    {
    }
}
