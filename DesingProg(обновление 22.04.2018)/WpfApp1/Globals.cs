﻿namespace Patulas
{
    /**
     * Класс, в котором будут храниться промежуточные данные
     * об освоении элемента знаний
     * Для каждого элемента знаний (задания) создаётся
     * собственный экземлпяр
     * После выполнения задания на оценку данные отсюда
     * записываются в журнал ученика
     * */
    public class Globals
    {
        //текущий объект класса
        private static Globals globals;
        //номер элемента знаний (задания)
        private int n;
        //пройден ли этап составления алгоритма
        private bool algorithm = false;
        //достигнутый уровень сложности
        private int difficulty = 0;
        //количество правильных решённых ситуаций
        private int countRight = 0;
        //количество неправильно решённых ситуаций
        private int countWrong = 0;
        //количество выбранных ситуаций
        private int selected = 0;
        //всего ситуаций в задании
        private int total;
        //выбранный раздел
        private string unit;
        //выбранная тема
        private string topic;
        //выбранный элемент знаний
        private string element;
        //формулировка задания
        private string taskText;

        public Globals(int n)
        {
            this.n = n;
            globals = this;
        }

        public int GetTaskNumber()
        {
            return n;
        }

        public void PassAlgorithm()
        {
            algorithm = true;
        }

        public bool IsAlgorithmPassed()
        {
            return algorithm;
        }

        public void IncreaseDifficulty()
        {
            if (difficulty < 2)
                difficulty++;
        }

        public int GetDifficultyLevel()
        {
            return difficulty;
        }

        public void AddRight()
        {
            countRight++;
        }

        public void AddWrong()
        {
            countWrong++;
        }

        public void AddSelected()
        {
            selected++;
        }

        public int GetCountRight()
        {
            return countRight;
        }

        public int GetCountWrong()
        {
            return countWrong;
        }

        public int GetSelected()
        {
            return selected;
        }

        public void SetTotal(int total)
        {
            this.total = total;
        }

        public int GetTotal()
        {
            return total;
        }

        public void SetUnit(string unit)
        {
            this.unit = unit;
        }

        public string GetUnit()
        {
            return unit;
        }

        public void SetTopic(string topic)
        {
            this.topic = topic;
        }

        public string GetTopic()
        {
            return topic;
        }

        public void SetElement(string element)
        {
            this.element = element;
        }

        public string GetElement()
        {
            return element;
        }

        public void SetTaskText(string text)
        {
            taskText = text;
        }

        public string GetTaskText()
        {
            return taskText;
        }

        public static Globals get()
        {
            return globals;
        }

    }
}
