﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace Patulas
{
    /// <summary>
    /// Логика взаимодействия для SlideTaskGraph.xaml
    /// </summary>
    public partial class SlideTaskImages : UserControl
    {
        private int n;
        public UserControl next;
        private UserControl source;
        private List<Image> images = new List<Image>(4);
        private List<Button> buttonsSelect = new List<Button>(4);
        private List<PackIcon> iconsRight = new List<PackIcon>(4);
        private List<PackIcon> iconsWrong = new List<PackIcon>(4);
        private List<Border> borders = new List<Border>(4);
        private int selectedID;
        private bool isCorrect;
        private string taskType;
        private Image situationImage;
        private int situationID;

        public SlideTaskImages(UserControl source, int n, int situationID, List<Image> images, string taskType, string situation)
        {
            InitializeComponent();
            createSlide(source, n, situationID, images, taskType);
            SituationText.Text = situation;
        }

        public SlideTaskImages(UserControl source, int n, int situationID, List<Image> images, string taskType, string situation, Image image)
        {
            InitializeComponent();
            createSlide(source, n, situationID, images, taskType);
            SituationText.Text = situation;
            situationImage = image;
            if (situationImage.Source == null)
                BtnOpenImage.Visibility = Visibility.Hidden;
        }

        public SlideTaskImages(UserControl source, int n, int situationID, List<Image> images, string taskType, Image image)
        {
            InitializeComponent();
            createSlide(source, n, situationID, images, taskType);
            situationImage = image;
        }

        private void createSlide(UserControl source, int n, int situationID, List<Image> images, string taskType)
        {
            this.source = source;
            this.taskType = taskType;
            this.n = n;
            this.situationID = situationID;
            HideIcons();
            this.images.Add(Image1);
            this.images.Add(Image2);
            this.images.Add(Image3);
            this.images.Add(Image4);
            buttonsSelect.Add(Select1);
            buttonsSelect.Add(Select2);
            buttonsSelect.Add(Select3);
            buttonsSelect.Add(Select4);
            iconsRight.Add(IconRight1);
            iconsRight.Add(IconRight2);
            iconsRight.Add(IconRight3);
            iconsRight.Add(IconRight4);
            iconsWrong.Add(IconWrong1);
            iconsWrong.Add(IconWrong2);
            iconsWrong.Add(IconWrong3);
            iconsWrong.Add(IconWrong4);
            borders.Add(Border1);
            borders.Add(Border2);
            borders.Add(Border3);
            borders.Add(Border4);
            List<Image> shuffled = shuffleImages(images);
            for (int i = 0; i < 4; i++)
            {
                this.images[i].Source = shuffled[i].Source;
                if (shuffled[i].Source == images[0].Source)
                {
                    this.images[i].Uid = "0";
                }
            }
            Title.Content += n + "";
            if (!taskType.Contains(Constants.Images))
                BtnOpenImage.Visibility = Visibility.Hidden;
        }

        private void HideIcons()
        {
            IconRight1.Visibility = Visibility.Hidden;
            IconRight2.Visibility = Visibility.Hidden;
            IconRight3.Visibility = Visibility.Hidden;
            IconRight4.Visibility = Visibility.Hidden;
            IconWrong1.Visibility = Visibility.Hidden;
            IconWrong2.Visibility = Visibility.Hidden;
            IconWrong3.Visibility = Visibility.Hidden;
            IconWrong4.Visibility = Visibility.Hidden;
            Border1.Visibility = Visibility.Hidden;
            Border2.Visibility = Visibility.Hidden;
            Border3.Visibility = Visibility.Hidden;
            Border4.Visibility = Visibility.Hidden;
        }

        private List<Image> shuffleImages(List<Image> list)
        {
            List<Image> shuffled = new List<Image>(list.Capacity);
            for (int i = 0; i < list.Count; i++)
            {
                shuffled.Add(list[i]);
            }
            Random rnd = new Random();
            for (int i = 0; i < shuffled.Count; i++)
            {
                Image tmp = shuffled[i];
                shuffled.RemoveAt(i);
                shuffled.Insert(rnd.Next(shuffled.Count), tmp);
            }
            return shuffled;
        }

        private void Select_Click(object sender, RoutedEventArgs e)
        {
            Button clicked = (Button)sender;
            selectedID = buttonsSelect.IndexOf(clicked);
            foreach (Border border in borders)
            {
                if (borders.IndexOf(border) == selectedID)
                {
                    border.Visibility = Visibility.Visible;
                }
                else
                {
                    border.Visibility = Visibility.Hidden;
                }
            }
            borders[selectedID].Visibility = Visibility.Visible;
            BtnCheck.IsEnabled = true;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (images[selectedID].Uid == "0")
            {
                iconsRight[selectedID].Visibility = Visibility.Visible;
                isCorrect = true;
            }
            else
            {
                iconsWrong[selectedID].Visibility = Visibility.Visible;
                isCorrect = false;
            }
            foreach (Button select in buttonsSelect)
            {
                select.Click -= Select_Click;
            }
            BtnCheck.Click -= Button_Click;
            BtnCheck.Content = Constants.Continue;
            BtnCheck.Click += Continue;
        }

        private void Continue(object sender, RoutedEventArgs e)
        {
            if (!isCorrect)
            {
                if (source is Slide3c_1)
                {
                    Slide3c_1 slide = (Slide3c_1)source;
                    if (slide.GetErrors() == 0)
                    {
                        slide.IncreaseErrors();
                        next = new Slide2(n, slide);
                    }
                    else if (slide.GetErrors() == 1)
                    {
                        slide.IncreaseErrors();
                        next = new Slide3a_1(slide);
                    }
                    else
                    {
                        Globals.get().AddWrong();
                        next = new Slide3b_1(n, slide.slideElements, taskType, slide.taskAnswer, slide.answerType, false);
                    }
                }
                if (source is IActions)
                {
                    ((IActions)source).HighlightWrongAnswer(situationID);
                    ((IActions)source).HideSelectImageButton(situationID);
                    if (source is Slide3c_2)
                    {
                        if (((Slide3c_2)source).GetCurrentSituation() != 2)
                        {
                            if (Globals.get().GetTotal() - ((IActions)source).GetMaxSituationNumber() > 0)
                                ((IActions)source).ShowNextSelectButton(situationID);
                            else
                            {
                                int abs = Math.Abs(Globals.get().GetTotal() - ((IActions)source).GetMaxSituationNumber());
                                if (situationID == ((IActions)source).GetMaxSituationNumber() - ((IActions)source).GetMinSituationNumber() - abs - 1)
                                {
                                    if (((IActions)source).GetLeftToParse() <= 0)
                                        ((IActions)source).AllowContinue();
                                }
                                else if (((IActions)source).GetLeftToParse() <= 0)
                                    ((IActions)source).ShowNextSelectButton(situationID);
                            }
                        }
                        
                    }
                    if (source is Slide3b_1)
                    {
                        if (Globals.get().GetTotal() - ((IActions)source).GetMaxSituationNumber() > 0)
                            ((IActions)source).ShowNextSelectButton(situationID);
                        else
                        {
                            int abs = Math.Abs(Globals.get().GetTotal() - ((IActions)source).GetMaxSituationNumber());
                            if (situationID == ((IActions)source).GetMaxSituationNumber() - ((IActions)source).GetMinSituationNumber() - abs - 1)
                            {
                                if (((IActions)source).GetLeftToParse() <= 0)
                                    ((IActions)source).AllowContinue();
                            }
                            else if (((IActions)source).GetLeftToParse() <= 0)
                                ((IActions)source).ShowNextSelectButton(situationID);
                        }
                    }
                    next = source;
                }
                if (source is Slide3a_1)
                {
                    ((Slide3a_1)source).HighlightWrongAction(situationID);
                    ((Slide3a_1)source).correctAnswers[situationID] = isCorrect;
                    next = source;
                }
                if (source is Slide4)
                {
                    ((Slide4)source).IncreaseCountTry();
                    if (((Slide4)source).IsAnotherTryPossible())
                    {
                        ((Slide4)source).GiveAnotherTry();
                        next = source;
                    }
                    else {
                        int mark = ((Slide4)source).CalculateMark(isCorrect);
                        next = new Slide4_1(mark);
                    }
                }
            }
            else
            {
                if (!(source is Slide4))
                    Globals.get().AddRight();
                if (source is Slide3c_1)
                {
                    Slide3c_1 slide = (Slide3c_1)source;
                    if (slide.GetErrors() == 2)
                    {
                        next = new Slide3b_1(slide.n, slide.slideElements, slide.GetTaskType(), slide.taskAnswer, slide.answerType, true);
                    }
                    else
                    {
                        next = new Slide3c_2(slide.n, slide.slideElements, taskType, slide.taskAnswer, Constants.Images);
                    }
                }
                if (source is IActions)
                {
                    ((IActions)source).HighlightCorrectAnswer(situationID);
                    ((IActions)source).HideSelectImageButton(situationID);
                    if (Globals.get().GetTotal() - ((IActions)source).GetMaxSituationNumber() > 0)
                        ((IActions)source).ShowNextSelectButton(situationID);
                    else
                    {
                        int abs = Math.Abs(Globals.get().GetTotal() - ((IActions)source).GetMaxSituationNumber());
                        if (situationID == ((IActions)source).GetMaxSituationNumber() - ((IActions)source).GetMinSituationNumber() - abs - 1)
                        {
                            ((IActions)source).AllowContinue();
                        }
                        else ((IActions)source).ShowNextSelectButton(situationID);
                    }
                    next = source;
                }
                if (source is Slide3a_1)
                {
                    ((Slide3a_1)source).HighlightCorrectAction(situationID);
                    ((Slide3a_1)source).correctAnswers[situationID] = isCorrect;
                    next = source;
                }
                if (source is Slide4)
                {
                    ((Slide4)source).IncreaseCountTry();
                    int mark = ((Slide4)source).CalculateMark(isCorrect);
                    next = new Slide4_1(mark);
                }
            }
            MainWindow.get().OpenNewSlide(next);
        }

        private void BtnOpenImage_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.OpenSituationImageWindow(situationImage);
        }
    }
}
