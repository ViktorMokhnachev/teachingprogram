﻿using System;
using System.Windows;

namespace Patulas
{
    /// <summary>
    /// Логика взаимодействия для TaskWindow.xaml
    /// </summary>
    public partial class TaskWindow : Window
    {
        public static int count = 0;

        public TaskWindow(string text)
        {
            InitializeComponent();
            TaskText.Text = text;
            count++;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            count--;
        }
    }
}
