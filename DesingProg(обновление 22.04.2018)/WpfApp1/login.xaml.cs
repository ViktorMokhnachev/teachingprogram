﻿using System;
using System.Windows;
using System.Windows.Input;
using static Patulas.DatabaseAdapter;

namespace Patulas
{
    /// <summary>
    /// Логика взаимодействия для login.xaml
    /// </summary>
    public partial class Login : Window
    {
        public Login()
        {
            InitializeComponent();
            InputLanguageManager.SetInputLanguage(this, System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
            //тестовое обращение к БД, чтобы проверить, доступна ли она
            try
            {
                GetMaxLength(Constants.TableUsers, Constants.ColumnLogin);
            }
            catch (Npgsql.NpgsqlException)
            {
                MessageBox.Show(Constants.SocketExceptionMessage, Constants.Error, MessageBoxButton.OK, MessageBoxImage.Error);
                Close();
                throw;
            }
        }

        private void Window_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                Close();
            }
        }

        private void Close_MouseUp(object sender, MouseButtonEventArgs e)
        {
            Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string login = LoginBox.Text.ToLower();
            string password = Password.Password;
            string md5;
            string tail;
            if (login == "")
            {
                MessageBox.Show(Constants.ErrorEnterUserName, Constants.Error, MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (password == "")
            {
                MessageBox.Show(Constants.ErrorEnterPassword, Constants.Error, MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            try
            {
                md5 = GetUserPassword(login);
                tail = GetUserPasswordTail(login);
            }
            catch (NullReferenceException)
            {
                MessageBox.Show(Constants.ErrorUserENotFound, Constants.Error, MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (ComparePassword(password, md5, tail))
            {
                User user = new User(GetUserID(login), login, GetUserName(login), GetGroup(login));
                MainWindow.get().SetUser(user);
                if (user.GetName() == null || user.GetName() == "")
                {
                    MainWindow.get().UserName.Content = Constants.Welcome + user.GetLogin();
                }
                else
                {
                    MainWindow.get().UserName.Content = Constants.Welcome + user.GetName();
                }
                MainWindow.get().Login.Content = Constants.Journal;
                MainWindow.get().SetJournalHandler();
                MainWindow.get().Register.Content = Constants.ExitAccount;
                MainWindow.get().SetExitAccountHandler();
                MainWindow.get().UnlockButtons();
                MainWindow.get().InfoRectangle.Visibility = Visibility.Hidden;
                MainWindow.get().InfoText.Visibility = Visibility.Hidden;
                Close();
            }
            else
            {
                MessageBox.Show(Constants.ErrorWrongPassword, Constants.Error, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LoginBox.Focus();
        }
    }

}
