﻿using System.Collections.Generic;

namespace Patulas
{
    /*
     * Вспомогательный класс, представляющий перечень элементов знаний
     */
    class Elements
    {
        public string Element { get; set; }
        public int UnitID { get; set; }
        public int TopicID { get; set; }
        public int ElementID { get; set; }
        public XmlDataAdapter elements;
        private int unit, topic;

        public Elements()
        {
            //пустой конструктор по умолчанию используется при формировании таблицы со списком элементов знаний
        }

        public Elements(int UnitID, int TopicID)
        {
            elements = new XmlDataAdapter(Constants.PathToElements);
            unit = UnitID;
            topic = TopicID;
        }

        public Elements(int unitID)
        {
            elements = new XmlDataAdapter(Constants.PathToElements);
            unit = unitID;
        }

        public List<string> getElements()
        {
            List<string> elementItems = new List<string>();
            elementItems = elements.getElements(unit, topic);
            return elementItems;
        }
    }
}
