﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Npgsql;
using static Patulas.Constants;

namespace Patulas
{
    /**
     * Статический класс для взаимодействия с базой данных
     * */
    public static class DatabaseAdapter
    {
        //объект соединения с БД
        private static NpgsqlConnection conn;
        //объект команды к БД
        private static NpgsqlCommand comm;
        //параметры соединения с БД
        private static string conn_param = "Server=127.0.0.1;Port=5432;User Id=postgres;Password=gaga;Database=TeachingProgram;CONNECTIONLIFETIME=0;";

        //получить MD5-хэш для шифрования пароля
        private static string GetMD5(string value)
        {
            System.Security.Cryptography.MD5CryptoServiceProvider my_pass = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] data = Encoding.ASCII.GetBytes(value);
            data = my_pass.ComputeHash(data);
            return Encoding.ASCII.GetString(data);
        }

        //получить максимальную длину поля в БД
        public static int GetMaxLength(string table, string column)
        {
            conn = new NpgsqlConnection(conn_param);
            conn.Open();
            comm = new NpgsqlCommand(SqlQueries.GetVarcharMaxLength(table, column), conn);
            int length = Convert.ToInt32(comm.ExecuteScalar().ToString());
            conn.Close();
            return length;
        }

        //сгенерировать "хвостик" для сохранения пароля в БД
        //"хвостик" - случайный набор символов, который дописывается к паролю,
        //необходим для повышения уровня безопасности,
        //т.к пароль становится труднее расшифровать
        private static string GenerateTail()
        {
            string symbols = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789.+-*/,<>/?[]{}`~!@#$%^&()_=";
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            int n = random.Next(5) + 5;
            for (int i = 0; i < n; i++)
            {
                builder.Append(symbols.ToCharArray()[random.Next(symbols.Length)]);
            }
            return builder.ToString();
        }

        //добавить нового пользователя в БД
        public static User AddNewUser(string login, string password, string name, string group)
        {
            conn = new NpgsqlConnection(conn_param);
            conn.Open();
            comm = new NpgsqlCommand(SqlQueries.CheckUniqueLogin(login), conn);
            int unique = Convert.ToInt32(comm.ExecuteScalar().ToString());
            if (unique != 0)
            {
                conn.Close();
                throw new ArgumentException();
            }
            else
            {
                comm = new NpgsqlCommand(SqlQueries.GetNewUserID(), conn);
                int id = Convert.ToInt32(comm.ExecuteScalar().ToString());
                string tail = GenerateTail();
                string md5 = GetMD5(GetMD5(password) + tail);
                comm = new NpgsqlCommand(SqlQueries.AddNewUser(id, login, md5, tail, name, group), conn);
                comm.ExecuteScalar();
                conn.Close();
                return new User(id, login, name, group);
            }
        }

        //получить номер пользователя в БД по его логину
        public static int GetUserID(string login)
        {
            conn = new NpgsqlConnection(conn_param);
            conn.Open();
            comm = new NpgsqlCommand(SqlQueries.GetUserID(login), conn);
            int id = Convert.ToInt32(comm.ExecuteScalar().ToString());
            return id;
        }

        //получить пароль пользователя из БД
        public static string GetUserPassword(string login)
        {
            conn = new NpgsqlConnection(conn_param);
            conn.Open();
            comm = new NpgsqlCommand(SqlQueries.GetUserPassword(login), conn);
            string md5;
            try
            {
                md5 = comm.ExecuteScalar().ToString();
            }
            catch (NullReferenceException)
            {
                throw;
            }
            finally
            {
                conn.Close();
            }
            return md5;
        }

        //получить "хвостик" пароля пользователя
        public static string GetUserPasswordTail(string login)
        {
            conn = new NpgsqlConnection(conn_param);
            conn.Open();
            comm = new NpgsqlCommand(SqlQueries.GetUserPasswordTail(login), conn);
            string tail = comm.ExecuteScalar().ToString();
            conn.Close();
            return tail;
        }

        //получить реальное имя пользователя по его логину
        public static string GetUserName(string login)
        {
            conn = new NpgsqlConnection(conn_param);
            conn.Open();
            comm = new NpgsqlCommand(SqlQueries.GetUserName(login), conn);
            string tail = comm.ExecuteScalar().ToString();
            conn.Close();
            return tail;
        }

        //получить реальное имя пользователя по его номеру
        public static string GetUserName(int id)
        {
            conn = new NpgsqlConnection(conn_param);
            conn.Open();
            comm = new NpgsqlCommand(SqlQueries.GetUserName(id), conn);
            string tail = comm.ExecuteScalar().ToString();
            conn.Close();
            return tail;
        }

        //получить учебную группу пользователя по логину
        public static string GetGroup(string login)
        {
            conn = new NpgsqlConnection(conn_param);
            conn.Open();
            comm = new NpgsqlCommand(SqlQueries.GetGroup(login), conn);
            string group = comm.ExecuteScalar().ToString();
            conn.Close();
            return group;
        }

        //получить учебную группу пользователя по номеру
        public static string GetGroup(int id)
        {
            conn = new NpgsqlConnection(conn_param);
            conn.Open();
            comm = new NpgsqlCommand(SqlQueries.GetGroup(id), conn);
            string group = comm.ExecuteScalar().ToString();
            conn.Close();
            return group;
        }

        //сравнить введённый пароль с паролем из БД
        public static bool ComparePassword(string password, string md5, string tail)
        {
            return (GetMD5(GetMD5(password) + tail) == md5);
        }
        
        //запись результата в БД
        public static void AddNewResult(int userID, string unit, string topic, string element, int situationsTotal,
            int situationsSolved, int situationsRight, int situationsWrong, int difficulty, int mark, string date)
        {
            conn = new NpgsqlConnection(conn_param);
            conn.Open();
            comm = new NpgsqlCommand(SqlQueries.GetNewResultID(), conn);
            int id = Convert.ToInt32(comm.ExecuteScalar().ToString());
            comm = new NpgsqlCommand(SqlQueries.AddNewResult(id, userID, unit, topic, element, situationsTotal,
                situationsSolved, situationsRight, situationsWrong, difficulty, mark, date), conn);
            comm.ExecuteScalar();
            conn.Close();
        }

        //получить значение из указанной ячейки в таблице результатов
        public static string GetResultColumn(string column, int userID, int id)
        {
            conn = new NpgsqlConnection(conn_param);
            conn.Open();
            comm = new NpgsqlCommand(SqlQueries.GetResultColumn(column, userID, id), conn);
            string result = comm.ExecuteScalar().ToString();
            conn.Close();
            return result;
        }

        //получить общее число записей в таблице резльутатов по номеру пользователя
        public static int GetCountResults(int userID)
        {
            conn = new NpgsqlConnection(conn_param);
            conn.Open();
            comm = new NpgsqlCommand(SqlQueries.GetResultsCount(userID), conn);
            int count = Convert.ToInt32(comm.ExecuteScalar().ToString());
            conn.Close();
            return count;
        }

        //получить общее число записей в таблице резльутатов
        public static int GetCountResults()
        {
            conn = new NpgsqlConnection(conn_param);
            conn.Open();
            comm = new NpgsqlCommand(SqlQueries.GetResultsCount(), conn);
            int count = Convert.ToInt32(comm.ExecuteScalar().ToString());
            conn.Close();
            return count;
        }

        //получить уровень доступа пользователя
        public static int GetAccessLevel(int userID)
        {
            conn = new NpgsqlConnection(conn_param);
            conn.Open();
            comm = new NpgsqlCommand(SqlQueries.GetAccessLevel(userID), conn);
            int accessLevel = Convert.ToInt32(comm.ExecuteScalar().ToString());
            conn.Close();
            return accessLevel;
        }

        //получить номера пользователей, у которых есть записи в таблице результатов
        public static List<int> GetDistinctUsersFromResults()
        {
            conn = new NpgsqlConnection(conn_param);
            conn.Open();
            comm = new NpgsqlCommand(SqlQueries.GetCountDistinctUsersFromResults(), conn);
            int count = Convert.ToInt32(comm.ExecuteScalar().ToString());
            List<int> ids = new List<int>(count);
            for (int i = 0; i < count; i++)
            {
                comm = new NpgsqlCommand(SqlQueries.GetDistinctUserFromResults(i), conn);
                ids.Add(Convert.ToInt32(comm.ExecuteScalar().ToString()));
            }
            conn.Close();
            return ids;
        }
    }

    /**
     * Статический класс для генерации SQL-запросов
     * */
    static class SqlQueries
    {
        //запрос на получение максимальную длину указанного поля типа varchar из указанной таблицы
        //varchar - строковый тип переменной длины
        public static string GetVarcharMaxLength(string table, string column)
        {
            return "SELECT CHARACTER_MAXIMUM_LENGTH FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = "
                + table.Replace('\"', '\'') + " AND COLUMN_NAME = " + column.Replace('\"', '\'') + "; ";
        }

        //запрос на добавления нового пользователя
        public static string AddNewUser(int id, string login, string md5, string tail, string name, string group)
        {
            return "INSERT INTO " + TableUsers + " (" + ColumnID + ", " + ColumnName + ", " + ColumnLogin + ", " + ColumnGroup +  ", " + ColumnPassword + ", "
                + ColumnTail + ") VALUES ('" + id + "', '" + name + "', '" + login + "', '" + group + "', '" + md5 + "', '" + tail + "');";
        }

        //запрос на получение номера нового пользователя
        public static string GetNewUserID()
        {
            return "SELECT COUNT (*) FROM " + TableUsers + "; ";
        }

        //запрос на получение номера новой записи в таблице результатов
        public static string GetNewResultID()
        {
            return "SELECT COUNT (*) FROM " + TableResults + "; ";
        }

        //запрос на проверку уникальности логина
        public static string CheckUniqueLogin(string login)
        {
            return "SELECT COUNT (*) FROM " + TableUsers + " WHERE " + ColumnLogin + " LIKE '" + login + "';";
        }

        //запрос на получение пароля пользователя
        public static string GetUserPassword(string login)
        {
            return "SELECT " + ColumnPassword + " FROM " + TableUsers + " WHERE " + ColumnLogin + " LIKE '" + login + "';";
        }

        //запрос на получение "хвостика" пароля пользователя
        public static string GetUserPasswordTail(string login)
        {
            return "SELECT " + ColumnTail + " FROM " + TableUsers + " WHERE " + ColumnLogin + " LIKE '" + login + "';";
        }

        //запрос на получение номера пользователя по логину
        public static string GetUserID(string login)
        {
            return "SELECT " + ColumnID+ " FROM " + TableUsers + " WHERE " + ColumnLogin + " LIKE '" + login + "';";
        }

        //запрос на получение настоящего имени пользователя по его логину
        public static string GetUserName(string login)
        {
            return "SELECT " + ColumnName + " FROM " + TableUsers + " WHERE " + ColumnLogin + " LIKE '" + login + "';";
        }

        //запрос на получение учебной группы пользователя по его логину
        public static string GetGroup(string login)
        {
            return "SELECT " + ColumnGroup + " FROM " + TableUsers + " WHERE " + ColumnLogin + " LIKE '" + login + "';";
        }

        //запрос на получение учебной группы пользователя по его номеру
        public static string GetGroup(int id)
        {
            return "SELECT " + ColumnGroup + " FROM " + TableUsers + " WHERE " + ColumnID + " = " + id + ";";
        }

        //запрос на получение настоящего имени пользователя по его номеру
        public static string GetUserName(int id)
        {
            return "SELECT " + ColumnName + " FROM " + TableUsers + " WHERE " + ColumnID + " = " + id + ";";
        }

        //запрос на добавление нового результата
        public static string AddNewResult(int id, int userID, string unit, string topic, string element, int situationsTotal,
            int situationsSolved, int situationsRight, int situationsWrong, int difficulty, int mark, string date)
        {
            return "INSERT INTO " + TableResults + " (" + ColumnID + ", " + ColumnUserID + ", " + ColumnUnit + ", " + ColumnTopic + ", "
                + ColumnElement + ", " + ColumnSituationsCount + ", " + ColumnSituationsSolved + ", " + ColumnSituationsRight + ", " 
                + ColumnSituationsWrong + ", " + ColumnDifficulty + ", " + ColumnMark + ", " + ColumnDate + ") VALUES ('" + id + "', '" + userID + 
                "', '" + unit + "', '" + topic + "', '" + element + "', '" + situationsTotal + "', '" + situationsSolved + "', '"
                + situationsRight + "', '" + situationsWrong + "', '" + difficulty + "', '" + mark + "', '" + date + "');";
        }
       
        //запрос на получение значения из указанной ячейки из таблицы результатов
        public static string GetResultColumn(string column, int userID, int id)
        {
            return "SELECT " + column + " FROM " + TableResults + " WHERE " + ColumnUserID + " = " + userID + " limit 1 offset " + id + ";";
        }

        //запрос на получение количества записей в таблице результатов
        public static string GetResultsCount(int userID)
        {
            return "SELECT COUNT (*) FROM " + TableResults + " WHERE " + ColumnUserID + " = " + userID + "; ";
        }

        //запрос на получение количества записей в таблице результатов
        public static string GetResultsCount()
        {
            return "SELECT COUNT (*) FROM " + TableResults + "; ";
        }

        //запрос на получение уровня доступа пользователя
        public static string GetAccessLevel(int id)
        {
            return "SELECT " + ColumnAccessLevel + " FROM " + TableUsers + " WHERE " + ColumnID + " = " + id + ";";
        }

        //запрос на получение номеров пользователей, у которых есть записи в таблице результатов
        public static string GetCountDistinctUsersFromResults()
        {
            return "SELECT COUNT (DISTINCT " + ColumnUserID + ") FROM " + TableResults + ";";
        }

        public static string GetDistinctUserFromResults(int id)
        {
            return "SELECT DISTINCT " + ColumnUserID + " FROM " + TableResults + " ORDER BY " + ColumnUserID + "limit 1 offset " + id + ";";
        }
    }
}
