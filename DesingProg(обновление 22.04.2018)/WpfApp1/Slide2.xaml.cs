﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace Patulas
{
    /// <summary>
    /// Логика взаимодействия для Slide2.xaml
    /// </summary>
    public partial class Slide2 : UserControl
    {
        private int id, n;
        private List<TextBox> answers = new List<TextBox>();
        private List<string> actions;
        private List<int> correctAnswerIds;
        private string correctAnswer;
        private Slide3c_1 source1;
        private IActions source2;
        private string actionElement;

        public Slide2(int id, Slide3c_1 source)
        {
            InitializeComponent();
            this.id = id;
            source1 = source;
            XmlDataAdapter actions = new XmlDataAdapter(Constants.PathToActions);
            actionElement = actions.GetActionElement(id);
            ActionName.Text = Constants.ToDo + actionElement + Constants.YouNeed;
            this.actions = actions.GetActions(id);
            n = this.actions.Count;
            correctAnswerIds = new List<int>(this.actions.Count - 2);
            List<string> shuffledActions = Utils.shuffleList(this.actions);
            string extra = this.actions[this.actions.Count - 1];
            foreach (string action in this.actions)
            {
                foreach (string shuffledAction in shuffledActions)
                {
                    if (action == shuffledAction && action != extra)
                    {
                        int i = shuffledActions.IndexOf(shuffledAction);
                        {
                            correctAnswerIds.Add(i);
                        }
                        break;
                    }
                }
            }
            Utils.GenerateActionsList(shuffledActions, ListActions);
            GenerateAnswerFields();
            StringBuilder builder = new StringBuilder();
            foreach (int i in correctAnswerIds)
            {
                builder.Append(Utils.letters[i]);
            }
            correctAnswer = builder.ToString();
        }

        public Slide2(int id, IActions source)
        {
            InitializeComponent();
            this.id = id;
            source2 = source;
            XmlDataAdapter actions = new XmlDataAdapter(Constants.PathToActions);
            actionElement = actions.GetActionElement(id);
            ActionName.Text = Constants.ToDo + actionElement + Constants.YouNeed;
            this.actions = actions.GetActions(id);
            n = this.actions.Count;
            correctAnswerIds = new List<int>(this.actions.Count - 2);
            List<string> shuffledActions = Utils.shuffleList(this.actions);
            string extra = this.actions[this.actions.Count - 1];
            foreach (string action in this.actions)
            {
                foreach (string shuffledAction in shuffledActions)
                {
                    if (action == shuffledAction && action != extra)
                    {
                        int i = shuffledActions.IndexOf(shuffledAction);
                        {
                            correctAnswerIds.Add(i);
                        }
                        break;
                    }
                }
            }
            Utils.GenerateActionsList(shuffledActions, ListActions);
            GenerateAnswerFields();
            StringBuilder builder = new StringBuilder();
            foreach (int i in correctAnswerIds)
            {
                builder.Append(Utils.letters[i]);
            }
            correctAnswer = builder.ToString();
        }

        private void BtnCheck_Click(object sender, RoutedEventArgs e)
        {
            Globals.get().PassAlgorithm();
            bool correct;
            string answer;
            StringBuilder builder = new StringBuilder();
            foreach (TextBox textbox in answers)
            {
                builder.Append(textbox.Text);
            }
            answer = builder.ToString().ToUpper();
            correct = answer == correctAnswer;
            XmlDataAdapter elements = new XmlDataAdapter(Constants.PathToElements);
            actions.Remove(actions.Last());
            string genitive = elements.GetElementGenitiveByTaskID(id);
            if (!correct)
            {
                if (source1 != null)
                    MainWindow.get().OpenNewSlide(new Slide2_2(source1, genitive, actions, actionElement));
                else MainWindow.get().OpenNewSlide(new Slide2_2(source2, genitive, actions, actionElement));
            }
            else
            {
                if (source1 != null)
                    MainWindow.get().OpenNewSlide(new Slide2_1(source1, genitive, actions));
                else MainWindow.get().OpenNewSlide(new Slide2_1(source2, genitive));
            }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            bool allAnswerFieldsAreFilled = true;
            foreach (TextBox answer in answers)
            {
                if (answer.Text == "")
                {
                    allAnswerFieldsAreFilled = false;
                    break;
                }
            }
            BtnCheck.IsEnabled = allAnswerFieldsAreFilled;
        }

        private void GenerateAnswerFields()
        {
            int width = 100;
            for (int i = 0; i < n - 1; i++)
            {
                Label letter = new Label();
                letter.Content = (i + 1) + ".";
                letter.FontSize = 18;
                letter.HorizontalAlignment = HorizontalAlignment.Left;
                letter.VerticalAlignment = VerticalAlignment.Top;
                letter.VerticalContentAlignment = VerticalAlignment.Center;
                letter.Margin = new Thickness(width, 10, 0, 0);
                letter.Width = 30;
                GridAnswers.Children.Add(letter);
                width += 30;
                TextBox textBox = new TextBox();
                textBox.HorizontalAlignment = HorizontalAlignment.Left;
                textBox.VerticalAlignment = VerticalAlignment.Top;
                textBox.Height = 35;
                textBox.Width = 65;
                textBox.Margin = new Thickness(width, 10, 0, 0);
                textBox.FontSize = 18;
                textBox.TextChanged += TextBox_TextChanged;
                answers.Add(textBox);
                GridAnswers.Children.Add(textBox);
                width += 65;
            }
        }
    }
}
