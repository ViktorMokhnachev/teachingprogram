﻿using System;
using static Patulas.Constants;

namespace Patulas
{
    /**
     * Класс, представляющий одну строку из таблицы Results
     * */
    public class Result
    {
        public string group { get; set; }
        public string name { get; set; }
        public string date { get; set; }
        public string unit { get; set; }
        public string topic { get; set; }
        public string element{ get; set; }
        public int situationsCount{ get; set; }
        public int situationsSolved{ get; set; }
        public int situationsRight{ get; set; }
        public int situationsWrong{ get; set; }
        public string difficulty{ get; set; }
        public int mark{ get; set; }

        public Result(int userID, int id)
        {
            group = DatabaseAdapter.GetGroup(userID);
            name = DatabaseAdapter.GetUserName(userID);
            date = DatabaseAdapter.GetResultColumn(ColumnDate, userID, id);
            unit = DatabaseAdapter.GetResultColumn(ColumnUnit, userID, id);
            topic = DatabaseAdapter.GetResultColumn(ColumnTopic, userID, id);
            element = DatabaseAdapter.GetResultColumn(ColumnElement, userID, id);
            situationsCount = Convert.ToInt32(DatabaseAdapter.GetResultColumn(ColumnSituationsCount, userID, id));
            situationsSolved = Convert.ToInt32(DatabaseAdapter.GetResultColumn(ColumnSituationsSolved, userID, id));
            situationsRight = Convert.ToInt32(DatabaseAdapter.GetResultColumn(ColumnSituationsRight, userID, id));
            situationsWrong = Convert.ToInt32(DatabaseAdapter.GetResultColumn(ColumnSituationsWrong, userID, id));
            int d = Convert.ToInt32(DatabaseAdapter.GetResultColumn(ColumnDifficulty, userID, id));
            switch (d)
            {
                case 0:
                    difficulty = "простое";
                    break;
                case 1:
                    difficulty = "непростое";
                    break;
                case 2:
                    difficulty = "трудное";
                    break;
            }
            mark = Convert.ToInt32(DatabaseAdapter.GetResultColumn(ColumnMark, userID, id));
        }
    }
}
