﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace Patulas
{
    /// <summary>
    /// Логика взаимодействия для Slide1_3_Images.xaml
    /// </summary>
    public partial class Slide1_3_Images : UserControl, ISituations
    {
        private List<Image> situations;
        private int n;
        public int situationID = 1;
        public UserControl nextSlide;
        private string answerType;
        public List<ListViewItem> situationsList;

        public Slide1_3_Images(int n, XmlDataAdapter taskFile)
        {
            InitializeComponent();
            this.n = n;
            string text = taskFile.GetTaskText();
            string source = taskFile.GetSource();
            int sourceimages = n;
            answerType = taskFile.GetAnswerType();
            TaskText.Text = text;
            if (source != null)
            {
                sourceimages = Convert.ToInt32(source);
            }
            string pathToTaskImages = Constants.PathToTaskImages + sourceimages + "/";
            int countImages = taskFile.GetCountImages();
            situations = GetImages(pathToTaskImages, countImages);
            Globals.get().SetTotal(situations.Count);
            Globals.get().SetTaskText(text);
            situationsList = new List<ListViewItem>(situations.Count);
            foreach (Image situation in situations)
            {
                ListViewItem item = new ListViewItem();
                item.Content = situation;
                item.Uid = situations.IndexOf(situation) + "";
                situation.Height = 100;
                situation.Width = 100;
                item.HorizontalContentAlignment = HorizontalAlignment.Center;
                item.PreviewMouseUp += ListViewItem_PreviewMouseUp;
                ContentSituations.Items.Add(item);
                situationsList.Add(item);
            }
        }

        private List<Image> GetImages(string pathToTaskImages, int countImages)
        {
            List<Image> list = new List<Image>();
            for (int i = 0; i < countImages; i++)
            {
                Image image = new Image();
                image.Source = new BitmapImage(new Uri(pathToTaskImages + i + Constants.ExtensionPng, UriKind.Relative));
                list.Add(image);
            }
            return list;
        }

        private void ListViewItem_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            ListViewItem clicked = (ListViewItem)sender;
            clicked.IsEnabled = false;
            situationsList.Remove(clicked);
            int id = Convert.ToInt32(clicked.Uid);
            Image situation = situations[id];
            Globals.get().AddSelected();
            if (situationID == 1)
            {
                Slide3c_1 next = new Slide3c_1(n, id, situation, answerType, this, clicked);
                MainWindow.get().OpenNewSlide(next);
            }
            else
            {
                if (!(nextSlide is Slide3c_3))
                {
                    Int32[] ids = { 2, 3, 4, 5 };
                    if (ids.Contains(situationID))
                    {
                        IActions next = (IActions)nextSlide;
                        next.HideButtonSelect(situationID - 2);
                        next.ShowSituationField(situationID - 2);
                        next.ShowAnswerField(situationID - 2);
                        next.SetAnswerFieldUid(situationID - 2, id + "");
                        next.SetSelectedSituation(situationID - 2, id);
                        next.ShowCurrentAnswerField();
                        Image image = new Image();
                        image.Source = Utils.GetTaskImage(n, id);
                        next.AddSituationImage(image);
                        if (image.Source != null)
                            next.ShowOpenImageButton(situationID - 2);
                        if (answerType == Constants.Images)
                        {
                            next.ShowSelectImageButton(situationID - 2);
                        }
                        MainWindow.get().OpenNewSlide((UserControl)next);
                    }
                }
                else
                {
                    Int32[] ids = { 5, 6 };
                    if (ids.Contains(situationID))
                    {
                        IActions next = (IActions)nextSlide;
                        next.HideButtonSelect(situationID - 5);
                        next.ShowSituationField(situationID - 5);
                        next.ShowAnswerField(situationID - 5);
                        next.SetAnswerFieldUid(situationID - 5, id + "");
                        next.SetSelectedSituation(situationID - 5, id);
                        next.ShowCurrentAnswerField();
                        Image image = new Image();
                        image.Source = Utils.GetTaskImage(n, id);
                        next.AddSituationImage(image);
                        if (image.Source != null)
                            next.ShowOpenImageButton(situationID - 5);
                        if (answerType == Constants.Images)
                        {
                            next.ShowSelectImageButton(situationID - 5);
                        }
                        MainWindow.get().OpenNewSlide((UserControl)next);
                    }
                }
            }
        }

        public Image GetSituation(int id)
        {
            return situations[id];
        }

        public int GetAllSituaitonsCount()
        {
            return situations.Count;
        }
        
        /**
         * Ниже идёт реализация интерфейса IActions
         */

        public void setSituationID(int situationID)
        {
            this.situationID = situationID;
        }

        public void setNextSlide(UserControl slide)
        {
            nextSlide = slide;
        }

        public void setHeaderText(string text)
        {
            Header.Text = text;
        }

        public void HideSelectFirstSituation()
        {
            SelectFirstSituation.Visibility = Visibility.Hidden;
        }

        public List<ListViewItem> getSituationsList()
        {
            return situationsList;
        }

        public string GetTaskType()
        {
            return Constants.Images;
        }
    }
}
