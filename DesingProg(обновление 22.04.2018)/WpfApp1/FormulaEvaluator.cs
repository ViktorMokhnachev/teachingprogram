﻿using System;
using System.Text;

namespace Patulas
{
    //оценивает правильность введённой формулы
    //один объект этого класса проверяет одну формулу
    //для проверки каждой новой формулы требуется создавать новый объект
    class FormulaEvaluator
    {
        private string left;//имя переменной в левой части выражения
        private string[] right;//переменные в правой части выражения
        private double[,] values;//начальные и конечные значения и шаг для скалярных формул
        private int[] vx0, vx1, vxi, vy0, vy1, vyi;//начальные и конечные координаты и шаг для векторных формул
        private string resultFormula;//правильная формула
        private int[,] resultVector;//ожидаемые результаты на каждом шаге при проверке формулы в случае сложения векторов
        private bool isVector;//формула векторная или скалярная
        private int vectorsCount;
        private int stepsCount;//количество шагов для оценки формулы

        //конструктор для формулы итогового ответа
        public FormulaEvaluator(int situationID, int formulaID, XmlDataAdapter answerFile)
        {
            isVector = answerFile.isVector(situationID, formulaID);
            if (isVector)
            {
                vectorsCount = answerFile.GetVectorsCount(situationID, formulaID);
                vx0 = answerFile.GetVectorsValues(situationID, formulaID, vectorsCount, Constants.x0);
                vx1 = answerFile.GetVectorsValues(situationID, formulaID, vectorsCount, Constants.x1);
                vxi = answerFile.GetVectorsValues(situationID, formulaID, vectorsCount, Constants.xi);
                vy0 = answerFile.GetVectorsValues(situationID, formulaID, vectorsCount, Constants.y0);
                vy1 = answerFile.GetVectorsValues(situationID, formulaID, vectorsCount, Constants.y1);
                vyi = answerFile.GetVectorsValues(situationID, formulaID, vectorsCount, Constants.yi);
                resultVector = answerFile.GetResultVector(situationID, formulaID);
            }
            else
            {
                values = answerFile.GetFormulaValues(situationID, formulaID);
                resultFormula = answerFile.GetFormulaResult(situationID, formulaID);
            }
            stepsCount = answerFile.GetStepsCount(situationID, formulaID, isVector);
            left = answerFile.GetEquationPart(situationID, formulaID, Constants.Left).Replace('\'', '´');
            right = answerFile.GetEquationPart(situationID, formulaID, Constants.Right).Replace('\'', '´').Split(',');
        }

        public bool evaluate(string expression)
        {
            Type scriptType = Type.GetTypeFromCLSID(Guid.Parse("0E59F1D5-1FBE-11D0-8FF2-00A0D10038BC"));
            dynamic obj = Activator.CreateInstance(scriptType, false);
            obj.Language = "javascript";

            if (isVector)
            {
                for (int i = 0; i < stepsCount; i++)
                {
                    try
                    {
                        double result = obj.Eval(
                            operators() + 
                            left + "=" + resultVector[i, 0] + ";" +
                            GetRightPartOfEquation(i, true) +
                            expression.Replace('\'', '´'));
                        if (result != resultVector[i, 0]) return false;
                        result = obj.Eval(
                            operators() +
                            left + "=" + resultVector[i, 1] + ";" +
                            GetRightPartOfEquation(i, false) +
                            expression.Replace('\'', '´'));
                        if (result != resultVector[i, 1]) return false;
                    }
                    catch (System.Runtime.InteropServices.COMException e)
                    {
                        throw e;
                    }
                }
                return true;
            }
            else
            {
                for (int i = 0; i < stepsCount; i++)
                {
                    try
                    {
                        double expected = obj.Eval(
                            operators() +
                            GetRightPartOfEquation(i) +
                            resultFormula.Replace('\'', '´'));
                        double actual = obj.Eval(
                            operators() +
                            left + "=" + expected + ";" +
                            GetRightPartOfEquation(i) +
                            expression.Replace('\'', '´'));
                        if (Math.Round(actual, 10, MidpointRounding.AwayFromZero) != Math.Round(expected, 10, MidpointRounding.AwayFromZero))
                            return false;
                    }
                    catch (System.Runtime.InteropServices.COMException)
                    {
                        throw;
                    }
                }
                return true;
            }
        }

        //для векторных формул
        private string GetRightPartOfEquation(int i, bool x)
        {
            StringBuilder builder = new StringBuilder();
            if (x)
            {
                for (int j = 0; j < vectorsCount; j++)
                {
                    builder.Append(right[j] + "=" + (vx0[j] + vxi[j] * i) + ";");
                }
            }
            else
            {
                for (int j = 0; j < vectorsCount; j++)
                {
                    builder.Append(right[j] + "=" + (vy0[j] + vyi[j] * i) + ";");
                }
            }
            return builder.ToString();
        }

        //для скалряных формул
        private string GetRightPartOfEquation(int i)
        {
            StringBuilder builder = new StringBuilder();
            for (int j = 0; j < right.Length; j++)
            {
                builder.Append(right[j] + "=" + (values[j, 0] + values[j, 2] * i) + ";");
            }
            return builder.ToString();
        }

        private string operators()
        {
            return "sqrt = Math.sqrt;" +
                "pow = Math.pow;" + 
                "cos = Math.cos;" +
                "sin = Math.sin;";
        }
    }
}
