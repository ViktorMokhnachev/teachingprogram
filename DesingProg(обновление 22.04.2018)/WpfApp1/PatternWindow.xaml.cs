﻿using System;
using System.Windows;

namespace Patulas
{
    /// <summary>
    /// Логика взаимодействия для PatternWindow.xaml
    /// </summary>
    public partial class PatternWindow : Window
    {
        public static int count = 0;

        public PatternWindow(string text)
        {
            InitializeComponent();
            AnswerPattern.Text = text;
            count++;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            count--;
        }
    }
}
