﻿using System.Windows;
using System.Windows.Controls;

namespace Patulas
{
    /// <summary>
    /// Логика взаимодействия для SlideNotice.xaml
    /// </summary>
    public partial class Slide1_2_1 : UserControl
    {
        private Slide1_2 previous;

        public Slide1_2_1(Slide1_2 slide)
        {
            InitializeComponent();
            previous = slide;
        }

        private void BtnTryAgain_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.get().OpenNewSlide(previous);
        }
    }
}
