﻿namespace Patulas
{
    /**
     * Класс, описывающий пользователя,
     * содержит его id в БД, логин, имя и другие личные сведения
     * */
    public class User
    {
        private int id;
        private string name;
        private string group;
        private string login;
        private int accessLevel;

        public User(int id, string login, string name, string group)
        {
            this.id = id;
            this.name = name;
            this.login = login;
            this.group = group;
            accessLevel = DatabaseAdapter.GetAccessLevel(id);
        }

        public string GetName()
        {
            return name;
        }

        public string GetLogin()
        {
            return login;
        }

        public string GetGroup()
        {
            return group;
        }

        public int GetID()
        {
            return id;
        }

        public int GetAccessLevel()
        {
            return accessLevel;
        }
    }
}
