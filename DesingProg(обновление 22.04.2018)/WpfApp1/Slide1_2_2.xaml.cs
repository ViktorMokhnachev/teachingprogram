﻿using System.Windows;
using System.Windows.Controls;

namespace Patulas
{
    /// <summary>
    /// Логика взаимодействия для SlideNotice2.xaml
    /// </summary>
    public partial class Slide1_2_2 : UserControl
    {
        private Slide1_2 previous;

        public Slide1_2_2(Slide1_2 slide, string text)
        {
            InitializeComponent();
            previous = slide;
            TextBlock.Text += text;
        }

        private void BtnTryAgain_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.get().OpenNewSlide(previous);
        }
    }
}
