﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Patulas
{
    /// <summary>
    /// Логика взаимодействия для Slide3в_3.xaml
    /// </summary>
    public partial class Slide3c_3 : UserControl, IActions
    {
        private ISituations slideElements;
        private List<Button> buttonsSelect = new List<Button>();
        private List<Button> buttonsParse = new List<Button>();
        private List<TextBlock> situations = new List<TextBlock>();
        private List<TextBox> answers = new List<TextBox>();
        private XmlDataAdapter taskAnswer;
        private bool isAllDone = false;
        private int currentSituation = 0;
        private string answerType;
        private int n;
        private int[] selectedSituations = { -1, -1 };
        private List<string> selectedSituationsText = new List<string>(2);
        private List<string> actions;
        private XmlDataAdapter actionsFile = new XmlDataAdapter(Constants.PathToActions);
        private int leftToParse;
        private List<Image> situationImages = new List<Image>();
        private List<Button> buttonsOpenImage = new List<Button>();
        private string taskType;
        private List<Button> buttonsSelectImage = new List<Button>();

        public Slide3c_3(int n, ISituations slideElements, string taskType, XmlDataAdapter taskAnswer, string answerType, bool answerIsCorrect)
        {
            InitializeComponent();
            this.n = n;
            this.slideElements = slideElements;
            this.taskAnswer = taskAnswer;
            this.answerType = answerType;
            this.taskType = taskType;
            if (taskType == Constants.Images)
            {
                buttonsOpenImage.Add(BtnOpenImage5);
                buttonsOpenImage.Add(BtnOpenImage6);
            }
            else if (taskType == Constants.Text_Images)
            {
                buttonsOpenImage.Add(BtnSituationImage5);
                buttonsOpenImage.Add(BtnSituationImage6);
            }
            BtnSituationImage5.Visibility = Visibility.Hidden;
            BtnSituationImage6.Visibility = Visibility.Hidden;
            BtnOpenImage5.Visibility = Visibility.Hidden;
            BtnOpenImage6.Visibility = Visibility.Hidden;
            foreach (UIElement control in GridMain.Children)
            {
                if (control is Button)
                {
                    if (control.Uid.Contains(Constants.Parse))
                        buttonsParse.Add((Button)control);
                    if (((Button)control).Name.Contains(Constants.BtnSelectSituation))
                        buttonsSelect.Add((Button)control);
                    if ((!control.Uid.Contains(Constants.Parse) && !control.Uid.Contains("5")) || control.Uid.Contains(Constants.Parse) || control.Uid.Contains(Constants.SelectImage))
                    {
                        control.Visibility = Visibility.Hidden;
                    }
                }
                if (control is TextBlock)
                {
                    situations.Add((TextBlock)control);
                    control.Visibility = Visibility.Hidden;
                }
                if (control is TextBox)
                {
                    answers.Add((TextBox)control);
                    control.Visibility = Visibility.Hidden;
                }
            }
            if (answerType == Constants.Images)
            {
                AnswerField.Visibility = Visibility.Hidden;
                YourAnswer.Visibility = Visibility.Hidden;
                buttonsSelectImage.Add(BtnSelectImage5);
                buttonsSelectImage.Add(BtnSelectImage6);
                BtnShowPattern.Visibility = Visibility.Hidden;
            }
            if (!answerIsCorrect)
                CorrectAnswerText.Text = "";
        }

        private void BtnSelect_Click(object sender, RoutedEventArgs e)
        {
            Button clicked = (Button)sender;
            slideElements.setSituationID(Convert.ToInt32(clicked.Uid));
            currentSituation = Convert.ToInt32(clicked.Uid);
            slideElements.setNextSlide(this);
            slideElements.setHeaderText(Constants.SelectSituation);
            slideElements.HideSelectFirstSituation();
            if (slideElements is Slide1_3)
                MainWindow.get().OpenNewSlide((Slide1_3)slideElements);
            else MainWindow.get().OpenNewSlide((Slide1_3_Images)slideElements);
        }

        private void BtnCheck_Click(object sender, RoutedEventArgs e)
        {
            if (isAllDone)
            {
                bool correct = true;
                //количество неверных
                int count = 0;
                //номер неверного
                int[] wrong = new int[2];
                AnswerChecker[] checker = new AnswerChecker[2];
                for (int i = 0; i < checker.Length; i++)
                {
                    checker[i] = new AnswerChecker(taskAnswer, Convert.ToInt32(answers[i].Uid));
                    try
                    {
                        if (!checker[i].CheckAnswer(answers[i].Text, answerType))
                        {
                            correct = false;
                            wrong[count] = i;
                            count++;
                        }
                    }
                    catch (System.Runtime.InteropServices.COMException)
                    {
                        return;
                    }
                }

                if (correct)
                {
                    //задание на оценку
                    for (int i = 0; i < 2; i++)
                    {
                        Globals.get().AddRight();
                    }
                    MainWindow.get().OpenNewSlide(new Slide4(n, slideElements, taskAnswer, answerType, true));
                }
                else
                {
                    if (count == 1)
                    {
                        Globals.get().AddRight();
                        Globals.get().AddWrong();
                        //показываем правильный ответ для неверной ситуации и идём дальше
                        switch (answerType)
                        {
                            case Constants.Text:
                                Slide3b_1_1 text = new Slide3b_1_1(wrong[0] + 5, selectedSituations[wrong[0]], situations[wrong[0]].Text, taskAnswer, this);
                                MainWindow.get().OpenNewSlide(text);
                                break;
                            case Constants.Text_Formula:
                                Slide3b_1_1 text_formula = new Slide3b_1_1(wrong[0] + 5, selectedSituations[wrong[0]], situations[wrong[0]].Text, taskAnswer, this);
                                MainWindow.get().OpenNewSlide(text_formula);
                                break;
                            case Constants.Formula:
                                Slide3b_1_1 formula = new Slide3b_1_1(wrong[0] + 5, selectedSituations[wrong[0]], situations[wrong[0]].Text,
                                    taskAnswer.GetFormulasCount(Convert.ToInt32(answers[wrong[0]].Uid)), taskAnswer, this);
                                MainWindow.get().OpenNewSlide(formula);
                                break;
                        }
                    }
                    else
                    {
                        //даём возможность разобрать неверные ситуации по алгоритму
                        for (int i = 0; i < count; i++)
                        {
                            answers[wrong[i]].Background = new SolidColorBrush(Color.FromArgb(200, 255, 0, 0));
                            buttonsParse[wrong[i]].Visibility = Visibility.Visible;
                            buttonsParse[wrong[i]].Click += ParseSituation;
                        }
                        BtnCheck.IsEnabled = false;
                        leftToParse = count;
                    }
                }
            }
            else
            {
                answers[currentSituation - 5].Text = AnswerField.Text;
                AnswerField.Text = "";
                AnswerField.IsEnabled = false;
                if (currentSituation != 6 && Globals.get().GetTotal() - 1 >= currentSituation)
                {
                    buttonsSelect[currentSituation - 4].Visibility = Visibility.Visible;
                }
                else
                {
                    isAllDone = true;
                    BtnCheck.IsEnabled = true;
                    BtnCheck.Content = Constants.Check;
                }
            }

        }

        private void AnswerField_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (AnswerField.Text == "")
                BtnCheck.IsEnabled = false;
            else BtnCheck.IsEnabled = true;
            AnswerField = Utils.CheckForGreekLetters(AnswerField);
        }

        private void ParseSituation(object sender, RoutedEventArgs e)
        {
            Button clicked = (Button)sender;
            int id = buttonsParse.IndexOf(clicked);
            currentSituation = selectedSituations[id];
            actions = actionsFile.GetActions(n);
            actions.Remove(actions.Last());
            Slide3a_1 next = new Slide3a_1(this, id, id + 5, false);
            leftToParse--;
            MainWindow.get().OpenNewSlide(next);
        }

        private void Continue(object sender, RoutedEventArgs e)
        {
            MainWindow.get().OpenNewSlide(new Slide4(n, slideElements, taskAnswer, answerType, false));
        }

        private void BtnSelectImage_Click(object sender, RoutedEventArgs e)
        {
            Button clicked = (Button)sender;
            int id = buttonsSelectImage.IndexOf(clicked);
            SlideTaskImages slideTaskImages = null;
            switch (taskType)
            {
                case Constants.Text:
                    slideTaskImages = new SlideTaskImages(this, n, id, Utils.GetAnswerImages(n, selectedSituations[id]), taskType, situations[id].Text);
                    break;
                case Constants.Images:
                    slideTaskImages = new SlideTaskImages(this, n, id, Utils.GetAnswerImages(n, selectedSituations[id]), taskType, situationImages[id]);
                    break;
                case Constants.Text_Images:
                    slideTaskImages = new SlideTaskImages(this, n, id, Utils.GetAnswerImages(n, selectedSituations[id]), taskType, situations[id].Text, situationImages[id]);
                    break;
            }
            MainWindow.get().OpenNewSlide(slideTaskImages);
        }

        private void BtnOpenImage_Click(object sender, RoutedEventArgs e)
        {
            Button clicked = (Button)sender;
            int id = buttonsOpenImage.IndexOf(clicked);
            MainWindow.OpenSituationImageWindow(situationImages[id]);
        }

        private void BtnShowTask_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.OpenTaskWindow(Globals.get().GetTaskText());
        }

        private void BtnShowPattern_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.OpenPatternWindow(taskAnswer.GetAnswerPattern());
        }

        /**
         * Ниже идёт реализация интерфейса IActions
         */
        public int GetNumber()
        {
            return n;
        }

        public List<string> GetActions()
        {
            return actions;
        }

        public string GetSituation(int id)
        {
            return selectedSituationsText[id];
        }

        public int GetCurrentSituation()
        {
            return currentSituation;
        }

        public void HideButtonParse(int id)
        {
            buttonsParse[id].Visibility = Visibility.Hidden;
        }

        public void HighlightCorrectAnswer(int id)
        {
            answers[id].Background = new SolidColorBrush(Color.FromArgb(200, 31, 126, 201));//FF1F7EC9
            leftToParse = 0;
        }

        public void HideButtonSelect(int id)
        {
            buttonsSelect[id].Visibility = Visibility.Hidden;
        }

        public void ShowAnswerField(int id)
        {
            answers[id].Visibility = Visibility.Visible;
            answers[id].Uid = id + "";
        }

        public void ShowSituationField(int id)
        {
            situations[id].Visibility = Visibility.Visible;
        }

        public void SetSituationFieldText(int id, string text)
        {
            situations[id].Text = text;
        }

        public void SetSelectedSituation(int id, int situationID)
        {
            selectedSituations[id] = situationID;
        }

        public int GetSelectedSituation(int id)
        {
            return selectedSituations[id];
        }

        public void SetAnswerFieldUid(int id, string uid)
        {
            answers[id].Uid = uid;
        }

        public void ShowCurrentAnswerField()
        {
            AnswerField.IsEnabled = true;
        }

        public void AddSituationText(string text)
        {
            selectedSituationsText.Add(text);
        }

        public void SendAnswer(int id, string text)
        {
            answers[id].Text = text;
        }

        public int GetLeftToParse()
        {
            return leftToParse;
        }

        public void AllowContinue()
        {
            BtnCheck.IsEnabled = true;
            BtnCheck.Content = Constants.Continue;
            BtnCheck.Click -= BtnCheck_Click;
            BtnCheck.Click += Continue;
        }

        public UserControl GetNextSlide()
        {
            return new Slide4(n, slideElements, taskAnswer, answerType, false);
        }

        public void AddSituationImage(Image image)
        {
            situationImages.Add(image);
        }

        public void ShowOpenImageButton(int id)
        {
            buttonsOpenImage[id].Visibility = Visibility.Visible;
        }

        public Image GetImage(int id)
        {
            return situationImages[id];
        }

        public string GetTaskType()
        {
            return taskType;
        }

        //подстветка неверного ответа (только задач с картинками)
        public void HighlightWrongAnswer(int id)
        {
            answers[id].Background = new SolidColorBrush(Color.FromArgb(200, 255, 0, 0));
            buttonsParse[id].Visibility = Visibility.Visible;
            leftToParse = 1;
            buttonsParse[id].Click += ParseSituation;
            Globals.get().IncreaseDifficulty();
        }

        public void ShowNextSelectButton(int id)
        {
            if (currentSituation != GetMaxSituationNumber() && Globals.get().GetTotal() - 1 >= currentSituation)
                buttonsSelect[id + 1].Visibility = Visibility.Visible;
        }

        public void ShowSelectImageButton(int id)
        {
            buttonsSelectImage[id].Visibility = Visibility.Visible;
        }

        public void HideSelectImageButton(int id)
        {
            buttonsSelectImage[id].Visibility = Visibility.Hidden;
        }

        public string GetAnswerType()
        {
            return answerType;
        }

        public int GetMaxSituationNumber()
        {
            return 6;
        }

        public int GetMinSituationNumber()
        {
            return 5;
        }

        public int GetSelectedSituationsCount()
        {
            int count = 0;
            for (int i = 0; i < selectedSituations.Length; i++)
            {
                if (selectedSituations[i] != -1)
                    count++;
            }
            return count;
        }

        public int GetMaxSituationsCount()
        {
            if (Globals.get().GetTotal() - GetMaxSituationNumber() > 0)
                return GetMaxSituationNumber() - 4;
            return Globals.get().GetTotal() - 5;
        }
    }
    
}
