﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace Patulas
{
    /// <summary>
    /// Логика взаимодействия для Slide3в_1.xaml
    /// </summary>
    public partial class Slide3c_1 : UserControl
    {
        public ISituations slideElements;
        private ListViewItem selected;
        private AnswerChecker checker;
        public XmlDataAdapter taskAnswer;
        private int Errors = 0;
        public int n;
        public List<string> actions;
        public string situation;
        public Image image;
        public string answerType;
        public int situationID;
        private string taskType;

        //конструктор для задач, где тип условия text или text_images
        public Slide3c_1(int n, int id, string text, string taskType, string answerType, ISituations slideElements, ListViewItem selected)
        {
            InitializeComponent();
            situation = text;
            this.slideElements = slideElements;
            this.selected = selected;
            this.n = n;
            this.answerType = answerType;
            this.taskType = taskType;
            if (answerType == Constants.Images)
            {
                YourAnswer.Visibility = Visibility.Hidden;
                AnswerField.Visibility = Visibility.Hidden;
                AnswerPattern.Visibility = Visibility.Hidden;
                AnswerPatternLabel.Visibility = Visibility.Hidden;
            }
            else
            {
                BtnSelectImage.Visibility = Visibility.Hidden;
            }
            situationID = id;
            Situation.Text = situation;
            if (!taskType.Contains(Constants.Images))
            {
                SituationImage.Visibility = Visibility.Hidden;
                BtnOpenImage.Visibility = Visibility.Hidden;
            }
            else
            {
                image = new Image();
                image.Source = Utils.GetTaskImage(n, id);
                if (image.Source == null)
                {
                    BtnOpenImage.Visibility = Visibility.Hidden;
                }
            }
            if (answerType != Constants.Images)
            {
                taskAnswer = new XmlDataAdapter(Constants.PathToAnswers + n + Constants.ExtensionXml);
                AnswerPattern.Text = taskAnswer.GetAnswerPattern();
                checker = new AnswerChecker(taskAnswer, id);
            }
        }

        //конструктор для задач, где тип условия images
        public Slide3c_1(int n, int id, Image image, string answerType, ISituations slideElements, ListViewItem selected)
        {
            InitializeComponent();
            Situation.Visibility = Visibility.Hidden;
            BtnOpenImage.Visibility = Visibility.Hidden;
            BtnSelectImage.Visibility = Visibility.Hidden;
            this.image = SituationImage;
            this.image.Source = image.Source;
            this.slideElements = slideElements;
            this.selected = selected;
            this.n = n;
            this.answerType = answerType;
            taskType = Constants.Images;
            situationID = id;
            Situation.Text = situation;
            if (answerType != Constants.Images)
            {
                taskAnswer = new XmlDataAdapter(Constants.PathToAnswers + n + Constants.ExtensionXml);
                AnswerPattern.Text = taskAnswer.GetAnswerPattern();
                checker = new AnswerChecker(taskAnswer, id);
            }
        }

        private void BtnCheck_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool correct = checker.CheckAnswer(AnswerField.Text, answerType);
                if (correct)
                {
                    Globals.get().AddRight();
                    selected.IsEnabled = false;
                    UserControl next;
                    if (Globals.get().GetTotal() == 2)
                        next = new Slide4(n, slideElements, taskAnswer, answerType, correct);
                    else next = new Slide3c_2(n, slideElements, taskType, taskAnswer, answerType);
                    MainWindow.get().OpenNewSlide(next);
                }
                else
                {
                    Errors++;
                    Globals.get().IncreaseDifficulty();
                    if (Errors == 1)
                    {
                        Slide2 next = new Slide2(n, this);
                        MainWindow.get().OpenNewSlide(next);
                    }
                    if (Errors == 2)
                    {
                        Slide3a_1 next = new Slide3a_1(this);
                        MainWindow.get().OpenNewSlide(next);
                    }
                }
            }
            catch (System.Runtime.InteropServices.COMException)
            {
                return;
            }
        }

        private void AnswerField_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (AnswerField.Text == "")
                BtnCheck.IsEnabled = false;
            else BtnCheck.IsEnabled = true;
            AnswerField = Utils.CheckForGreekLetters(AnswerField);
        }

        private void BtnOpenImage_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.OpenSituationImageWindow(image);
        }

        public string GetTaskType()
        {
            return taskType;
        }

        private void BtnSelectImage_Click(object sender, RoutedEventArgs e)
        {
            SlideTaskImages slideTaskImages = null;
            switch (taskType)
            {
                case Constants.Text:
                    slideTaskImages = new SlideTaskImages(this, n, -1, Utils.GetAnswerImages(n, situationID), taskType, situation);
                    break;
                case Constants.Images:
                    slideTaskImages = new SlideTaskImages(this, n, -1, Utils.GetAnswerImages(n, situationID), taskType, image);
                    break;
                case Constants.Text_Images:
                    slideTaskImages = new SlideTaskImages(this, n, -1, Utils.GetAnswerImages(n, situationID), taskType, situation, image);
                    break;
            }
            MainWindow.get().OpenNewSlide(slideTaskImages);
        }

        public int GetErrors()
        {
            return Errors;
        }

        public void IncreaseErrors()
        {
            if (Errors < 2)
            {
                Errors++;
                Globals.get().IncreaseDifficulty();
            }
        }

        private void BtnShowTask_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.OpenTaskWindow(Globals.get().GetTaskText());
        }

        private void BtnShowPattern_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.OpenPatternWindow(taskAnswer.GetAnswerPattern());
        }
    }
}
