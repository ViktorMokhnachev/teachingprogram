﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;

namespace Patulas
{
    public class AnswerChecker
    {
        //список обязательных ключевых слов
        //в каждом элементе списка хранится одно ключевое слово
        //ответ должен содержать все обязательные ключевые слова, иначе ответ будет считаться неверным
        private List<string> keywordsRequired;
        //массив списков необязательных ключевых слов
        //в каждом элементе массива хранится список ключевых слов с одинаковым id
        //хотя бы одно из этих слов должно содержаться в ответе, иначе ответ будет считаться неверным
        private List<string>[] keywordsOptional;
        //список слов, которых не должно быть в ответе
        //наличие хотя бы одного из этих слов в ответе означает, что ответ неверен
        private List<string> keywordsNotRequired;
        //общее число ключевых слов (включая списки optional, какждый из которых считается одним клюевым словом, но без not required)
        private double keywordsCount;
        //число ключевых слов, обнаруженных в ответе
        //если встречается слово из списка notrequired, это число уменьшается
        private double keywordsFound;
        //оценщики правильности формул - по одному на каждую в ответе
        private FormulaEvaluator[] evaluators;
        //количество формул в ответе
        private int formulasCount;
        //файл с ответами
        private XmlDataAdapter taskAnswer;
        //номер ситуации
        private int situationID;
        //номер действия
        private int actionID;
        //тип ответа на данном шаге при пошаговом разборе
        private string actionType;

        //конструктор для проверки простого текстового ответа
        public AnswerChecker(XmlDataAdapter taskAnswer, int situationId)
        {
            keywordsRequired = taskAnswer.GetKeywordsRequired(situationId);
            keywordsOptional = taskAnswer.GetKeywordsOptional(situationId);
            keywordsNotRequired = taskAnswer.GetKeywordsNotRequired(situationId);
            this.taskAnswer = taskAnswer;
            situationID = situationId;
        }

        //конструктор для проверки текстового ответа при разборе по действиям
        public AnswerChecker(XmlDataAdapter taskAnswer, int situationId, int actionID)
        {
            keywordsRequired = taskAnswer.GetActionKeywordsRequired(situationId, actionID);
            keywordsOptional = taskAnswer.GetActionKeywordsOptional(situationId, actionID);
            keywordsNotRequired = taskAnswer.GetActionKeywordsNotRequired(situationId, actionID);
            this.taskAnswer = taskAnswer;
            situationID = situationId;
            this.actionID = actionID;
            actionType = taskAnswer.GetActionType(actionID);
        }

        public bool CheckAnswer(string answer, string answerType)
        {
            switch (answerType)
            {
                case Constants.Text:
                    return CheckTextAnswer(answer);
                case Constants.Formula:
                    formulasCount = taskAnswer.GetFormulasCount(situationID);
                    evaluators = new FormulaEvaluator[formulasCount];
                    for (int i = 0; i < formulasCount; i++)
                    {
                        evaluators[i] = new FormulaEvaluator(situationID, i, taskAnswer);
                    }
                    try
                    {
                        return CheckFormulaAnswer(answer);
                    }
                    catch (System.Runtime.InteropServices.COMException)
                    {
                        throw;
                    }
                case Constants.Text_Formula:
                    List<string> formulas = findFormulas(answer);
                    formulasCount = taskAnswer.GetFormulasCount(situationID);
                    if (CheckTextAnswer(answer))
                    {
                        evaluators = new FormulaEvaluator[formulas.Count];
                        StringBuilder builder = new StringBuilder();
                        for (int i = 0; i < formulas.Count; i++)
                        {
                            evaluators[i] = new FormulaEvaluator(situationID, i, taskAnswer);
                        }
                        for (int i = 0; i < formulas.Count - 1; i++)
                        {
                            builder.Append(formulas[i]).Append("\n");
                        }
                        builder.Append(formulas.Last());
                        string answerFormulas = builder.ToString();
                        try
                        {
                            for (int i = 0; i < formulas.Count; i++)
                            {
                                if (!CheckFormulaAnswer(answerFormulas))
                                    return false;
                            }
                        }
                        catch (System.Runtime.InteropServices.COMException)
                        {
                            throw;
                        }
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                default:
                    return false;
            }
        }

        private List<string> findFormulas(string answer)
        {
            List<string> formulas = new List<string>();
            char[] chars = answer.ToCharArray();
            int indexStart = 0;
            int indexFinish = 0;
            for (int i = 0; i < chars.Length; i++)
            {
                if (chars[i] == '{')
                {
                    indexStart = i;
                }
                if (chars[i] == '}')
                {
                    indexFinish = i;
                    StringBuilder builder = new StringBuilder();
                    for (int j = indexStart + 1; j < indexFinish; j++)
                    {
                        builder.Append(chars[j]);
                    }
                    formulas.Add(builder.ToString());
                }
            }
            return formulas;
        }

        public static bool CheckThatFormulaIsCorrect(string answer, string answerType, XmlDataAdapter taskAnswer, int situationID)
        {
            int count = taskAnswer.GetFormulasCount(situationID);
            List<string> formulas = new List<string>();
            FormulaEvaluator[] evaluators;
            AnswerChecker checker = new AnswerChecker(taskAnswer, situationID);
            if (answerType == Constants.Text_Formula)
            {
                formulas = checker.findFormulas(answer);
                if (formulas.Count != count)
                {
                    MessageBox.Show(Constants.CheckFormulasCount, Constants.Error, MessageBoxButton.OK, MessageBoxImage.Error);
                    return false;
                }
            }
            evaluators = new FormulaEvaluator[count];
            for (int i = 0; i < count; i++)
            {
                evaluators[i] = new FormulaEvaluator(situationID, i, taskAnswer);
            }
            try
            {
                checker.CheckFormulaAnswer(formulas, evaluators, answerType);
            }
            catch (System.Runtime.InteropServices.COMException)
            {
                MessageBox.Show(Constants.CheckYourFormula, Constants.Error, MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            return true;
        }

        private void CheckFormulaAnswer(List<string> formulas, FormulaEvaluator[] evaluators, string answerType)
        {
            formulasCount = taskAnswer.GetFormulasCount(situationID);
            if (formulasCount > 1 && answerType == Constants.Formula)
            {
                //string[] formulas = answer.Replace("\r\n", "\n").Split('\n');
                if (formulas.Count != formulasCount)
                {
                    throw new System.Runtime.InteropServices.COMException();
                }
                for (int i = 0; i < formulasCount; i++)
                {
                    try
                    {
                        evaluators[i].evaluate(formulas[i]);
                    }
                    catch (System.Runtime.InteropServices.COMException)
                    {
                        throw;
                    }
                }
            }
            else
            {
                try
                {
                    evaluators[0].evaluate(formulas[0]);
                }
                catch (System.Runtime.InteropServices.COMException)
                {
                    throw;
                }
                catch (IndexOutOfRangeException)
                {
                    throw;
                }
            }
        }

        private bool CheckFormulaAnswer(string answer)
        {
            bool correct = false;
            if (formulasCount > 1)
            {
                string[] formulas = answer.Replace("\r\n", "\n").Split('\n');
                if (formulas.Length != formulasCount)
                {
                    MessageBox.Show(Constants.CheckFormulasCount, Constants.Error, MessageBoxButton.OK, MessageBoxImage.Error);
                    throw new System.Runtime.InteropServices.COMException();
                }
                for (int i = 0; i < formulasCount; i++)
                {
                    try
                    {
                        correct = evaluators[i].evaluate(formulas[i]);
                    }
                    catch (System.Runtime.InteropServices.COMException)
                    {
                        MessageBox.Show(Constants.CheckYourFormula, Constants.Error, MessageBoxButton.OK, MessageBoxImage.Error);
                        throw;
                    }
                    if (!correct) break;
                }
            }
            else
            {
                try
                {
                    correct = evaluators[0].evaluate(answer);
                }
                catch (System.Runtime.InteropServices.COMException)
                {
                    MessageBox.Show(Constants.CheckYourFormula, Constants.Error, MessageBoxButton.OK, MessageBoxImage.Error);
                    throw;
                }
                catch (IndexOutOfRangeException)
                {
                    MessageBox.Show(Constants.CheckYourFormula, Constants.Error, MessageBoxButton.OK, MessageBoxImage.Error);
                    throw;
                }
            }
            return correct;
        }

        private bool CheckTextAnswer(string answer)
        {
            answer = answer.ToLower().Replace('ё', 'е');
            answer = Regex.Replace(answer, "\\W", "");
            bool correct = true;
            foreach (string keyword in keywordsRequired)
            {
                if (!answer.Contains(keyword))
                {
                    correct = false;
                    break;
                }
            }
            if (!correct)
            {
                //ответ неверен, дальше не проверяем
                return false;
            }
            else
            {
                for (int i = 0; i < keywordsOptional.Length; i++)
                {
                    bool contains = false;
                    foreach (string keyword in keywordsOptional[i])
                    {
                        if (answer.Contains(keyword))
                        {
                            contains = true;
                            break;
                        }
                    }
                    correct = contains;
                    if (!correct)
                        break;
                }
            }
            if (!correct)
            {
                //ответ не верен
                return false;
            }
            else
            {
                foreach (string keyword in keywordsNotRequired)
                {
                    if (answer.Contains(keyword))
                    {
                        correct = false;
                        break;
                    }
                }
            }
            if (!correct)
            {
                //ответ не верен, потому что содержит стоп-слова
                return false;
            }
            else
            {
                //ответ верен
                return true;
            }
        }

        private void CheckTextAnswerForMark(string answer)
        {
            answer = answer.ToLower().Replace('ё', 'е');
            answer = Regex.Replace(answer, "\\W", "");
            keywordsCount = keywordsRequired.Count + keywordsOptional.Length;
            keywordsFound = keywordsCount;
            foreach (string keyword in keywordsRequired)
            {
                if (!answer.Contains(keyword))
                {
                    keywordsFound--;
                }
            }
            for (int i = 0; i < keywordsOptional.Length; i++)
            {
                bool contains = false;
                foreach (string keyword in keywordsOptional[i])
                {
                    if (answer.Contains(keyword))
                    {
                        contains = true;
                        break;
                    }
                }
                if (!contains)
                    keywordsFound--;
            }
            foreach (string keyword in keywordsNotRequired)
            {
                if (answer.Contains(keyword))
                {
                    if (keywordsFound > 0)
                        keywordsFound--;
                }
            }
        }

        public int GetMark(string answer, string answerType, double correctSituations)
        {
            switch (answerType)
            {
                case Constants.Text:
                    CheckTextAnswerForMark(answer);
                    double percent = keywordsFound / keywordsCount;
                    if (percent > 0.8)
                        return 5;
                    else
                    {
                        if (percent > 0.6 && percent <= 0.8)
                            return 4;
                        else
                        {
                            if (percent >= 0.5 && percent <= 0.6)
                            {
                                if (correctSituations > 0.6)
                                    return 4;
                                else return 3;
                            }

                            else
                            {
                                if (correctSituations > 0.6)
                                    return 3;
                                else return 2;
                            }
                        }
                    }
                case Constants.Formula:
                    formulasCount = taskAnswer.GetFormulasCount(situationID);
                    double step = 5.0 / formulasCount;
                    double score = 0;
                    evaluators = new FormulaEvaluator[formulasCount];
                    for (int i = 0; i < formulasCount; i++)
                    {
                        evaluators[i] = new FormulaEvaluator(situationID, i, taskAnswer);
                    }
                    try
                    {
                        for (int i = 0; i < formulasCount; i++)
                        {
                            if (CheckFormulaAnswer(answer))
                                score += step;
                        }
                    }
                    catch (System.Runtime.InteropServices.COMException)
                    {
                        throw;
                    }
                    if (score > 4)
                        return 5;
                    else
                    {
                        if (score > 3)
                            return 4;
                        else
                        {
                            if (score > 2)
                            {
                                if (correctSituations > 0.6)
                                    return 4;
                                else return 3;
                            }
                            else if (correctSituations > 0.6)
                                return 3;
                            else return 2;
                        }
                    }
                case Constants.Text_Formula:
                    List<string> formulas = findFormulas(answer);
                    formulasCount = taskAnswer.GetFormulasCount(situationID);
                    CheckTextAnswerForMark(answer);
                    step = 4.0 / formulasCount;
                    score = 0;
                    percent = keywordsFound / keywordsCount;
                    if (percent > 0.8)
                        score = 1;
                    evaluators = new FormulaEvaluator[formulasCount];
                    for (int i = 0; i < formulasCount; i++)
                    {
                        evaluators[i] = new FormulaEvaluator(situationID, i, taskAnswer);
                    }
                    StringBuilder builder = new StringBuilder();
                    for (int i = 0; i < formulas.Count - 1; i++)
                    {
                        builder.Append(formulas[i]).Append("\n");
                    }
                    builder.Append(formulas.Last());
                    string answerFormulas = builder.ToString();
                    try
                    {
                        for (int i = 0; i < formulas.Count; i++)
                        {
                            if (CheckFormulaAnswer(answerFormulas))
                                score += step;
                        }
                    }
                    catch (System.Runtime.InteropServices.COMException)
                    {
                        throw;
                    }
                    if (score > 4)
                        return 5;
                    else
                    {
                        if (score > 3)
                            return 4;
                        else
                        {
                            if (score > 2)
                            {
                                if (correctSituations > 0.6)
                                    return 4;
                                else return 3;
                            }
                            else if (correctSituations > 0.6)
                                return 3;
                            else return 2;
                        }
                    }
            }
            return 0;
        }
    }
}
