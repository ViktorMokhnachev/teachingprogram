﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Patulas
{
    /// <summary>
    /// Логика взаимодействия для Slide3б_1_1.xaml
    /// </summary>
    public partial class Slide3c_2 : UserControl, IActions
    {
        private ISituations slideElements;
        private List<Button> buttonsSelect = new List<Button>();
        private List<Button> buttonsParse = new List<Button>();
        private List<TextBlock> situations = new List<TextBlock>();
        private List<TextBox> answers = new List<TextBox>();
        private XmlDataAdapter taskAnswer;
        private bool isAllDone = false;
        private int currentSituation = 0;
        private string answerType;
        private int n;
        private int[] selectedSituations = { -1, -1, -1, -1};
        private List<string> selectedSituationsText = new List<string>(4);
        private List<string> actions;
        private XmlDataAdapter actionsFile = new XmlDataAdapter(Constants.PathToActions);
        private int leftToParse;
        //количество неверных
        private int countWrong = 0;
        //номер неверного
        private int[] wrong = new int[3];
        private bool firstTry = true;
        private List<Image> situationImages = new List<Image>();
        private List<Button> buttonsOpenImage = new List<Button>();
        private string taskType;
        private List<Button> buttonsSelectImage = new List<Button>();

        public Slide3c_2(int n, ISituations slideElements, string taskType, XmlDataAdapter taskAnswer, string answerType)
        {
            InitializeComponent();
            this.n = n;
            this.slideElements = slideElements;
            this.taskAnswer = taskAnswer;
            this.answerType = answerType;
            this.taskType = taskType;
            if (taskType == Constants.Images)
            {
                buttonsOpenImage.Add(BtnOpenImage2);
                buttonsOpenImage.Add(BtnOpenImage3);
                buttonsOpenImage.Add(BtnOpenImage4);
                buttonsOpenImage.Add(BtnOpenImage5);
            }
            else if (taskType == Constants.Text_Images)
            {
                buttonsOpenImage.Add(BtnSituationImage2);
                buttonsOpenImage.Add(BtnSituationImage3);
                buttonsOpenImage.Add(BtnSituationImage4);
                buttonsOpenImage.Add(BtnSituationImage5);
            }
            BtnSituationImage2.Visibility = Visibility.Hidden;
            BtnSituationImage3.Visibility = Visibility.Hidden;
            BtnSituationImage4.Visibility = Visibility.Hidden;
            BtnSituationImage5.Visibility = Visibility.Hidden;
            BtnOpenImage2.Visibility = Visibility.Hidden;
            BtnOpenImage3.Visibility = Visibility.Hidden;
            BtnOpenImage4.Visibility = Visibility.Hidden;
            BtnOpenImage5.Visibility = Visibility.Hidden;
            foreach (UIElement control in GridMain.Children)
            {
                if (control is Button)
                {
                    if (control.Uid.Contains(Constants.Parse))
                        buttonsParse.Add((Button)control);
                    if (((Button)control).Name.Contains(Constants.BtnSelectSituation))
                        buttonsSelect.Add((Button)control);
                    if ((!control.Uid.Contains(Constants.Parse) && !control.Uid.Contains("2")) || control.Uid.Contains(Constants.Parse) || control.Uid.Contains(Constants.SelectImage))
                    {
                        control.Visibility = Visibility.Hidden;
                    }
                }
                if (control is TextBlock)
                {
                    situations.Add((TextBlock)control);
                    control.Visibility = Visibility.Hidden;
                }
                if (control is TextBox)
                {
                    answers.Add((TextBox)control);
                    control.Visibility = Visibility.Hidden;
                }
            }
            Btn_Algorithm.Visibility = Visibility.Hidden;
            if (answerType == Constants.Images)
            {
                AnswerField.Visibility = Visibility.Hidden;
                YourAnswer.Visibility = Visibility.Hidden;
                buttonsSelectImage.Add(BtnSelectImage2);
                buttonsSelectImage.Add(BtnSelectImage3);
                buttonsSelectImage.Add(BtnSelectImage4);
                buttonsSelectImage.Add(BtnSelectImage5);
                BtnShowPattern.Visibility = Visibility.Hidden;
            }
        }

        private void BtnSelect_Click(object sender, RoutedEventArgs e)
        {
            Button clicked = (Button)sender;
            slideElements.setSituationID(Convert.ToInt32(clicked.Uid));
            currentSituation = Convert.ToInt32(clicked.Uid);
            slideElements.setNextSlide(this);
            slideElements.setHeaderText(Constants.SelectSituation);
            slideElements.HideSelectFirstSituation();
            if (slideElements is Slide1_3)
                MainWindow.get().OpenNewSlide((Slide1_3)slideElements);
            else MainWindow.get().OpenNewSlide((Slide1_3_Images)slideElements);
        }

        private void BtnCheck_Click(object sender, RoutedEventArgs e)
        {
            if (currentSituation == 2)
            {
                if (answerType.Contains(Constants.Formula))
                    if (!AnswerChecker.CheckThatFormulaIsCorrect(AnswerField.Text, answerType, taskAnswer, selectedSituations[currentSituation - 2]))
                        return;
                answers[0].Text = AnswerField.Text;
                AnswerField.Text = "";
                AnswerField.IsEnabled = false;
                AnswerChecker checker = new AnswerChecker(taskAnswer, Convert.ToInt32(answers[0].Uid));
                bool correct = checker.CheckAnswer(answers[0].Text, answerType);
                if (correct)
                {
                    Globals.get().AddRight();
                    buttonsSelect[1].Visibility = Visibility.Visible;
                    HighlightCorrectAnswer(0);
                    BtnCheck.Content = Constants.Confirm;
                }
                else
                {
                    answers[0].Background = new SolidColorBrush(Color.FromArgb(200, 255, 0, 0));
                    buttonsParse[0].Visibility = Visibility.Visible;
                    switch (Globals.get().GetDifficultyLevel())
                    {
                        case 0:
                            buttonsParse[0].Click += MakeAlgoritm;
                            break;
                        case 1:
                            buttonsParse[0].Click -= MakeAlgoritm;
                            buttonsParse[0].Click += ParseSituation;
                            break;
                        case 2:
                            Globals.get().AddWrong();
                            break;
                    }
                    Globals.get().IncreaseDifficulty();
                }
            }
            else if (isAllDone)
            {
                firstTry = false;
                bool correct = true;
                countWrong = 0;
                wrong = new int[3];
                AnswerChecker[] checker = new AnswerChecker[3];
                for (int i = 0; i < checker.Length; i++)
                {
                    checker[i] = new AnswerChecker(taskAnswer, Convert.ToInt32(answers[i + 1].Uid));
                    try
                    {
                        if (!checker[i].CheckAnswer(answers[i + 1].Text, answerType))
                        {
                            correct = false;
                            wrong[countWrong] = i + 1;
                            countWrong++;
                        }
                    }
                    catch (System.Runtime.InteropServices.COMException)
                    {
                        return;
                    }
                }

                if (correct)
                {
                    //задание на оценку
                    for (int i = 0; i < 3; i++)
                    {
                        Globals.get().AddRight();
                    }
                    MainWindow.get().OpenNewSlide(new Slide4(n, slideElements, taskAnswer, answerType, true));
                }
                else
                {
                    if (countWrong == 1)
                    {
                        //показываем правильный ответ для неверной ситуации и идём дальше
                        for (int i = 0; i < 2; i++)
                        {
                            Globals.get().AddRight();
                        }
                        Globals.get().AddWrong();
                        switch (answerType)
                        {
                            case Constants.Text:
                                Slide3b_1_1 text = new Slide3b_1_1(wrong[0] + 2, selectedSituations[wrong[0]], situations[wrong[0]].Text, taskAnswer, this);
                                MainWindow.get().OpenNewSlide(text);
                                break;
                            case Constants.Text_Formula:
                                Slide3b_1_1 text_formula = new Slide3b_1_1(wrong[0] + 2, selectedSituations[wrong[0]], situations[wrong[0]].Text, taskAnswer, this);
                                MainWindow.get().OpenNewSlide(text_formula);
                                break;
                            case Constants.Formula:
                                Slide3b_1_1 formula = new Slide3b_1_1(wrong[0] + 2, selectedSituations[wrong[0]], situations[wrong[0]].Text,
                                    taskAnswer.GetFormulasCount(Convert.ToInt32(answers[wrong[0]].Uid)), taskAnswer, this);
                                MainWindow.get().OpenNewSlide(formula);
                                break;
                        }
                    }
                    else
                    {
                        if (Globals.get().IsAlgorithmPassed())
                        {
                            for (int i = 0; i < 3 - countWrong; i++)
                            {
                                Globals.get().AddRight();
                            }
                            //даём возможность разобрать неверные ситуации по алгоритму
                            for (int i = 0; i < countWrong; i++)
                            {
                                HighlightWrongAnswer(wrong[i]);
                            }
                            BtnCheck.IsEnabled = false;
                            leftToParse = countWrong;
                        }
                        else
                        {
                            Globals.get().IncreaseDifficulty();
                            for (int i = 0; i < countWrong; i++)
                            {
                                answers[wrong[i]].Background = new SolidColorBrush(Color.FromArgb(200, 255, 0, 0));
                            }
                            Btn_Algorithm.Visibility = Visibility.Visible;
                            AnswerField.Visibility = Visibility.Hidden;
                            YourAnswer.Visibility = Visibility.Hidden;
                        }
                    }
                }
            }
            else
            {
                if (answerType.Contains(Constants.Formula))
                    if (!AnswerChecker.CheckThatFormulaIsCorrect(AnswerField.Text, answerType, taskAnswer, selectedSituations[currentSituation - 2]))
                        return;
                if (!Globals.get().IsAlgorithmPassed())
                {
                    answers[currentSituation - 2].Text = AnswerField.Text;
                    AnswerField.Text = "";
                    AnswerField.IsEnabled = false;
                    if (currentSituation != 5 && Globals.get().GetTotal() - 1 >= currentSituation)
                    {
                        buttonsSelect[currentSituation - 1].Visibility = Visibility.Visible;
                    }
                    else
                    {
                        isAllDone = true;
                        BtnCheck.IsEnabled = true;
                        BtnCheck.Content = Constants.Check;
                    }
                }
                else
                {
                    answers[currentSituation - 2].Text = AnswerField.Text;
                    AnswerField.Text = "";
                    AnswerField.IsEnabled = false;
                    if (leftToParse <= 0)
                    {
                        if (firstTry)
                        {
                            if (currentSituation != 5 && Globals.get().GetTotal() - 1 >= currentSituation)
                            {
                                buttonsSelect[currentSituation - 1].Visibility = Visibility.Visible;
                            }
                            else
                            {
                                isAllDone = true;
                                BtnCheck.IsEnabled = true;
                                BtnCheck.Content = Constants.Check;
                            }
                        }
                        else
                        {
                            isAllDone = true;
                            BtnCheck.IsEnabled = true;
                            BtnCheck.Content = Constants.Check;
                        }
                    }
                }
            }

        }

        private void AnswerField_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (AnswerField.Text == "")
                BtnCheck.IsEnabled = false;
            else BtnCheck.IsEnabled = true;
            AnswerField = Utils.CheckForGreekLetters(AnswerField);
        }

        private void ParseSituation(object sender, RoutedEventArgs e)
        {
            Button clicked = (Button)sender;
            int id = buttonsParse.IndexOf(clicked);
            currentSituation = id + 2;
            actions = actionsFile.GetActions(n);
            actions.Remove(actions.Last());
            Slide3a_1 next;
            if (id == 0)
                next = new Slide3a_1(this, id, id + 2, true);
            else next = new Slide3a_1(this, id, id + 2, false);
            leftToParse--;
            if (currentSituation != 2)
                BtnCheck.Content = Constants.Confirm;
            MainWindow.get().OpenNewSlide(next);
        }

        private void MakeAlgoritm(object sender, RoutedEventArgs e)
        {
            MainWindow.get().OpenNewSlide(new Slide2(n, this));
        }

        public void CorrectAnswer()
        {
            Btn_Algorithm.Visibility = Visibility.Hidden;
            if (answerType != Constants.Images)
            {
                AnswerField.Visibility = Visibility.Visible;
                YourAnswer.Visibility = Visibility.Visible;
            }
            if (currentSituation == 2)
            {
                if (answerType != Constants.Images)
                {
                    AnswerField.Text = answers[0].Text;
                    AnswerField.IsEnabled = true;
                }
                else
                {
                    buttonsSelectImage[0].Visibility = Visibility.Visible;
                }
                buttonsParse[0].Visibility = Visibility.Hidden;
            }
            else
            {
                if (answerType != Constants.Images)
                {
                    for (int i = 0; i < countWrong; i++)
                    {
                        buttonsParse[wrong[i]].Visibility = Visibility.Visible;
                        buttonsParse[wrong[i]].Click += CorrectAnswers;
                    }
                    BtnCheck.Content = Constants.Confirm;
                    isAllDone = false;
                    leftToParse = countWrong;
                }
                else
                {
                    buttonsParse[currentSituation - 2].Visibility = Visibility.Visible;
                    buttonsParse[currentSituation - 2].Click += CorrectAnswers;
                }
            }
        }

        private void CorrectAnswers(object sender, RoutedEventArgs e)
        {
            Button clicked = (Button)sender;
            currentSituation = buttonsParse.IndexOf(clicked) + 2;
            if (answerType != Constants.Images)
            {
                AnswerField.Text = answers[currentSituation - 2].Text;
                AnswerField.IsEnabled = true;
            }
            clicked.Visibility = Visibility.Hidden;
            leftToParse--;
            if (answerType == Constants.Images)
            {
                BtnSelectImage_Click(buttonsSelectImage[currentSituation - 2], null);
            }
        }

        private void Continue(object sender, RoutedEventArgs e)
        {
            MainWindow.get().OpenNewSlide(new Slide4(n, slideElements, taskAnswer, answerType, false));
        }


        private void Btn_Algorithm_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.get().OpenNewSlide(new Slide2(n, this));
        }

        public void ShowButtonSelectThirdSituation()
        {
            buttonsSelect[1].Visibility = Visibility.Visible;
        }

        private void BtnOpenImage_Click(object sender, RoutedEventArgs e)
        {
            Button clicked = (Button)sender;
            int id = buttonsOpenImage.IndexOf(clicked);
            MainWindow.OpenSituationImageWindow(situationImages[id]);
        }

        private void BtnSelectImage_Click(object sender, RoutedEventArgs e)
        {
            Button clicked = (Button)sender;
            int id = buttonsSelectImage.IndexOf(clicked);
            SlideTaskImages slideTaskImages = null;
            switch (taskType)
            {
                case Constants.Text:
                    slideTaskImages = new SlideTaskImages(this, n, id, Utils.GetAnswerImages(n, selectedSituations[id]), taskType, situations[id].Text);
                    break;
                case Constants.Images:
                    slideTaskImages = new SlideTaskImages(this, n, id, Utils.GetAnswerImages(n, selectedSituations[id]), taskType, situationImages[id]);
                    break;
                case Constants.Text_Images:
                    slideTaskImages = new SlideTaskImages(this, n, id, Utils.GetAnswerImages(n, selectedSituations[id]), taskType, situations[id].Text, situationImages[id]);
                    break;
            }
            MainWindow.get().OpenNewSlide(slideTaskImages);
        }

        private void BtnShowTask_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.OpenTaskWindow(Globals.get().GetTaskText());
        }

        private void BtnShowPattern_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.OpenPatternWindow(taskAnswer.GetAnswerPattern());
        }

        /**
         * Ниже идёт реализация интерфейса IActions
         */
        public int GetNumber()
        {
            return n;
        }

        public List<string> GetActions()
        {
            return actions;
        }

        public string GetSituation(int id)
        {
            return selectedSituationsText[id];
        }

        public Image GetImage(int id)
        {
            return situationImages[id];
        }

        public int GetCurrentSituation()
        {
            return currentSituation;
        }

        public void HideButtonParse(int id)
        {
            buttonsParse[id].Visibility = Visibility.Hidden;
        }

        //подстветка верного ответа (только задач с картинками)
        public void HighlightCorrectAnswer(int id)
        {
            answers[id].Background = new SolidColorBrush(Color.FromArgb(200, 31, 126, 201));//FF1F7EC9
            leftToParse = 0;
        }

        //подстветка неверного ответа (только задач с картинками)
        public void HighlightWrongAnswer(int id)
        {
            answers[id].Background = new SolidColorBrush(Color.FromArgb(200, 255, 0, 0));
            buttonsParse[id].Visibility = Visibility.Visible;
            leftToParse = 1;
            if (Globals.get().GetDifficultyLevel() == 0)
            {
                buttonsParse[id].Click -= CorrectAnswers;
                buttonsParse[id].Click += MakeAlgoritm;
            }
            else
            {
                buttonsParse[id].Click -= MakeAlgoritm;
                buttonsParse[id].Click += ParseSituation;
            }
            Globals.get().IncreaseDifficulty();
        }

        public void HideButtonSelect(int id)
        {
            buttonsSelect[id].Visibility = Visibility.Hidden;
        }

        public void ShowAnswerField(int id)
        {
            answers[id].Visibility = Visibility.Visible;
            answers[id].Uid = id + "";
        }

        public void ShowSituationField(int id)
        {
            situations[id].Visibility = Visibility.Visible;
        }

        public void SetSituationFieldText(int id, string text)
        {
            situations[id].Text = text;
        }

        public void SetSelectedSituation(int id, int situationID)
        {
            selectedSituations[id] = situationID;
        }

        public int GetSelectedSituation(int id)
        {
            return selectedSituations[id];
        }

        public void SetAnswerFieldUid(int id, string uid)
        {
            answers[id].Uid = uid;
        }

        public void ShowCurrentAnswerField()
        {
            AnswerField.IsEnabled = true;
        }

        public void AddSituationText(string text)
        {
            selectedSituationsText.Add(text);
        }

        public void AddSituationImage(Image image)
        {
            situationImages.Add(image);
        }

        public void SendAnswer(int id, string text)
        {
            answers[id].Text = text;
        }

        public int GetLeftToParse()
        {
            return leftToParse;
        }

        public void AllowContinue()
        {
            BtnCheck.IsEnabled = true;
            BtnCheck.Content = Constants.Continue;
            BtnCheck.Click -= BtnCheck_Click;
            BtnCheck.Click += Continue;
        }

        public UserControl GetNextSlide()
        {
            return new Slide4(n, slideElements, taskAnswer, answerType, false);
        }

        public void ShowOpenImageButton(int id)
        {
            buttonsOpenImage[id].Visibility = Visibility.Visible;
        }

        public string GetTaskType()
        {
            return taskType;
        }

        public void ShowNextSelectButton(int id)
        {
            if (currentSituation != GetMaxSituationNumber() && Globals.get().GetTotal() - 1 >= currentSituation)
                buttonsSelect[id + 1].Visibility = Visibility.Visible;
        }

        public void ShowSelectImageButton(int id)
        {
            buttonsSelectImage[id].Visibility = Visibility.Visible;
        }

        public void HideSelectImageButton(int id)
        {
            buttonsSelectImage[id].Visibility = Visibility.Hidden;
        }

        public string GetAnswerType()
        {
            return answerType;
        }

        public int GetMaxSituationNumber()
        {
            return 5;
        }

        public int GetMinSituationNumber()
        {
            return 2;
        }

        public int GetSelectedSituationsCount()
        {
            int count = 0;
            for (int i = 0; i < selectedSituations.Length; i++)
            {
                if (selectedSituations[i] != -1)
                    count++;
            }
            return count;
        }

        public int GetMaxSituationsCount()
        {
            if (Globals.get().GetTotal() - GetMaxSituationNumber() > 0)
                return GetMaxSituationNumber() - 1;
            return Globals.get().GetTotal() - 2;
        }
    }
}
