﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Patulas
{
    public static class Utils
    {
        public static Dictionary<int, string> letters = new Dictionary<int, string>
        {
            [0] = "А",
            [1] = "Б",
            [2] = "В",
            [3] = "Г",
            [4] = "Д",
            [5] = "Е",
            [6] = "Ё",
            [7] = "Ж",
            [8] = "З",
            [9] = "И",
            [10] = "Й",
            [11] = "К",
            [12] = "Л",
            [13] = "М",
            [14] = "Н",
            [15] = "О",
            [16] = "П",
            [17] = "Р",
            [18] = "С",
            [19] = "Т",
            [20] = "У",
            [21] = "Ф",
            [22] = "Х",
            [23] = "Ц",
            [24] = "Ч",
            [25] = "Ш",
            [26] = "Щ",
            [27] = "Ъ",
            [28] = "Ы",
            [29] = "Ь",
            [30] = "Э",
            [31] = "Ю",
            [32] = "Я"
        };

        public static List<string> shuffleList(List<string> list)
        {
            List<string> shuffled = new List<string>(list.Capacity);
            for (int i = 0; i < list.Count; i++)
            {
                shuffled.Add(list[i]);
            }
            Random rnd = new Random();
            for (int i = 0; i < shuffled.Count; i++)
            {
                string tmp = shuffled[i];
                shuffled.RemoveAt(i);
                shuffled.Insert(rnd.Next(shuffled.Count), tmp);
            }
            return shuffled;
        }

        public static ImageSource GetTaskImage(int n, int id)
        {
            string path = Constants.PathToTaskImages + n + "/" + id + Constants.ExtensionPng;
            Uri uri= new Uri(path, UriKind.Relative);
            if (CanLoadResource(uri))
                return new BitmapImage(uri);
            return null;
        }

        private static bool CanLoadResource(Uri uri)
        {
            try
            {
                Application.GetResourceStream(uri);
                return true;
            }
            catch (IOException)
            {
                return false;
            }
        }

        public static List<Image> GetAnswerImages(int n, int situationID)
        {
            List<Image> images = new List<Image>(4);
            for (int i = 0; i < 4; i++)
            {
                images.Add(new Image());
                images[i].Source = GetAnswerImage(n, situationID, i);
            }
            return images;
        }

        private static ImageSource GetAnswerImage(int n, int situationID, int id)
        {
            string path = Constants.PathToAnswerImages + n + "/" + situationID + "." + id + Constants.ExtensionPng;
            Uri uri = new Uri(path, UriKind.Relative);
            return new BitmapImage(uri);
        }

        public static void GenerateActionsList(List<string> actions, ListView ListActions)
        {
            for (int i = 0; i < actions.Count; i++)
            {
                Grid grid = new Grid();
                Label letter = new Label();
                letter.Content = letters[i] + ".";
                letter.FontSize = 18;
                letter.HorizontalAlignment = HorizontalAlignment.Left;
                letter.VerticalAlignment = VerticalAlignment.Top;
                letter.VerticalContentAlignment = VerticalAlignment.Center;
                letter.Margin = new Thickness(10, 0, 0, 0);
                letter.Width = 25;
                grid.Children.Add(letter);
                TextBlock actionText = new TextBlock();
                actionText.HorizontalAlignment = HorizontalAlignment.Left;
                actionText.TextWrapping = TextWrapping.Wrap;
                actionText.VerticalAlignment = VerticalAlignment.Center;
                actionText.FontSize = 18;
                actionText.Width = grid.Width - letter.Width - 10;
                actionText.Margin = new Thickness(35, 0, 0, 0);
                actionText.Text = actions[i];
                grid.Children.Add(actionText);
                ListViewItem item = new ListViewItem();
                item.Width = ListActions.Width - 10;
                item.Height = actionText.Height;
                item.Content = grid;
                ListActions.Items.Add(item);
            }
        }

        public static TextBox CheckForGreekLetters(TextBox AnswerField)
        {
            if (AnswerField.Text.Contains("alpha"))
            {
                AnswerField.Text = AnswerField.Text.Replace("alpha", "α");
            }
            if (AnswerField.Text.Contains("beta"))
            {
                AnswerField.Text = AnswerField.Text.Replace("beta", "β");
            }
            return AnswerField;
        }
    }
}
