﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace Patulas
{
    /// <summary>
    /// Логика взаимодействия для Slide2_2.xaml
    /// </summary>
    public partial class Slide2_2 : UserControl
    {
        private Slide3c_1 next;
        private IActions next2;

        public Slide2_2(Slide3c_1 slide, string genitive, List<string> actions, string actionElement)
        {
            InitializeComponent();
            ElementGenitive.Text += genitive + Constants.OrYourSequenceIsNotCorrect;
            ActionName.Text = Constants.ToDo + actionElement + Constants.YouNeed;
            Utils.GenerateActionsList(actions, ListActions);
            next = slide;
            next.actions = actions;
        }

        public Slide2_2(IActions slide, string genitive, List<string> actions, string actionElement)
        {
            InitializeComponent();
            ElementGenitive.Text += genitive + Constants.OrYourSequenceIsNotCorrect;
            ActionName.Text = Constants.ToDo + actionElement + Constants.YouNeed;
            Utils.GenerateActionsList(actions, ListActions);
            next2 = slide;
        }

        private void BtnCheck_Click(object sender, RoutedEventArgs e)
        {
            if (next != null)
                MainWindow.get().OpenNewSlide(next);
            else
            {
                if (next2 is Slide3c_2)
                    ((Slide3c_2)next2).CorrectAnswer();
                MainWindow.get().OpenNewSlide((UserControl)next2);
            }
        }
    }
}
