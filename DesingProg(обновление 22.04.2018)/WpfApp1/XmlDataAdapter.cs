﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Resources;
using System.Xml.Linq;

namespace Patulas
{
    /**
     * Класс взаимодействия программы с xml-файлами.
     * Для того, чтобы обратиться к некоторому файлу,
     * необходимо создать экземпляр этого класса и в него
     * передать относительный путь к нужному файлу.
     * Каждый метод этого класса последовательно проходит по дереву элементов
     * в поисках нужного тега и/или атрибута и возвращает найденное значение,
     * либо некоторое значение по умолчанию.
     * */
    public class XmlDataAdapter
    {
        //созданный экземпляр
        XDocument XmlFile;
        //перечень переменных для формул, используемых в файлах с ответами в нужном порядке
        private static string[] defaultVariables = { "x", "y", "z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "r", "s", "t", "u", "v", "w" };

        //конструктор класса
        public XmlDataAdapter(string path)
        {   //начинаем загрузку файла
            var xd = new XDocument();
            var uri = new Uri(path, UriKind.Relative);
            var rs = Application.GetResourceStream(uri);
            if (rs == null)
                throw new FileNotFoundException(Constants.ResourceNotFound + path);
            using (var stream = rs.Stream)
            using (var tr = new StreamReader(stream))
                //если файл не найден, выводится сообщение об ошибке
                try
                {
                    XmlFile = XDocument.Load(tr);
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message, Constants.Error, MessageBoxButton.OK, MessageBoxImage.Error);
                }
            //загрузка файла закончена
        }

        //получить список разделов (только для elements.xml)
        public List<string> getUnits()
        {
            List<string> units = new List<string>();
            foreach (XElement unit in XmlFile.Element(Constants.Units).Elements())
            {
                units.Add(unit.Attribute(Constants.Name).Value);
            }
            return units;
        }

        //получить список названий иконок MaterialDegign для разделов (только для elements.xml)
        public List<string> getIcons()
        {
            List<string> icons = new List<string>();
            foreach (XElement unit in XmlFile.Element(Constants.Units).Elements())
            {
                icons.Add(unit.Attribute(Constants.Icon).Value);
            }
            return icons;
        }

        //получить список тем (только для elements.xml)
        public List<string> getTopics(int UnitID)
        {
            List<string> topics = new List<string>();
            foreach (XElement unit in XmlFile.Element(Constants.Units).Elements())
            {
                if (Convert.ToInt32(unit.Attribute(Constants.Id).Value) == UnitID)
                {
                    foreach (XElement topic in unit.Elements())
                    {
                        topics.Add(topic.Attribute(Constants.Name).Value);
                    }
                }
            }
            return topics;
        }

        //получить список элементов знаний (только для elements.xml)
        public List<string> getElements(int UnitID, int TopicID)
        {
            List<string> elements = new List<string>();
            foreach (XElement unit in XmlFile.Element(Constants.Units).Elements())
            {
                if (Convert.ToInt32(unit.Attribute(Constants.Id).Value) == UnitID)
                {
                    foreach (XElement topic in unit.Elements())
                    {
                        if (Convert.ToInt32(topic.Attribute(Constants.Id).Value) == TopicID)
                        {
                            foreach (XElement element in topic.Elements())
                            {
                                string item = element.Attribute(Constants.Name).Value;
                                elements.Add(item);
                            }
                        }
                    }
                }
            }
            return elements;
        }

        //получить номер задания (только для elements.xml)
        public int getTaskNumber(int UnitID, int TopicID, int ElementID)
        {
            int n = -1;
            foreach (XElement unit in XmlFile.Element(Constants.Units).Elements())
            {
                if (Convert.ToInt32(unit.Attribute(Constants.Id).Value) == UnitID)
                {
                    foreach (XElement topic in unit.Elements())
                    {
                        if (Convert.ToInt32(topic.Attribute(Constants.Id).Value) == TopicID)
                        {
                            foreach (XElement element in topic.Elements())
                            {
                                if (Convert.ToInt32(element.Attribute(Constants.Id).Value) == ElementID)
                                {
                                    n = Convert.ToInt32(element.Attribute(Constants.Task).Value);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            return n;
        }

        //получить верное значение "для чего можно применить элемент знаний" (только для applying.xml)
        public List<string> GetApply(int n)
        {
            List<string> applyList = new List<string>(3);
            foreach (XElement element in XmlFile.Element(Constants.Elements).Elements())
            {
                if (Convert.ToInt32(element.Attribute(Constants.Id).Value) == n)
                {
                    foreach (XElement apply in element.Elements())
                    {
                        applyList.Add(apply.Attribute(Constants.Text).Value);
                    }
                    break;
                }
            }
            return applyList;
        }

        //получить основной текст задания (для файлов taskX.xml)
        public string GetTaskText()
        {
            return XmlFile.Element(Constants.Task).Value;
        }

        //получить перечень текстовых ситуаций в задании (для файлов taskX.xml)
        public List<string> GetTaskSituations()
        {
            List<string> situations = new List<string>();
            foreach (XElement element in XmlFile.Element(Constants.Task).Elements())
            {
                if (element.Name == Constants.Situations)
                {
                    foreach (XElement situation in element.Elements())
                    {
                        situations.Add(situation.Attribute(Constants.Text).Value);
                    }
                    break;
                }
            }
            return situations;
        }

        //получить текстовый шаблон ответа на задание (для файлов answerX.xml)
        public string GetAnswerPattern()
        {
            string pattern = "";
            foreach (XElement element in XmlFile.Element(Constants.Answers).Elements())
            {
                if (element.Name == Constants.Pattern)
                {
                    pattern = element.Attribute(Constants.Text).Value;
                    break;
                }
            }
            return pattern;
        }

        //получить полный верный ответ в указанной ситуации (для файлов answerX.xml)
        public string GetFullAnswer(int situationID)
        {
            foreach (XElement element in XmlFile.Element(Constants.Answers).Elements())
            {
                if (element.Name == Constants.General)
                {
                    foreach (XElement situation in element.Elements())
                    {
                        if (situation.Attribute(Constants.Id).Value == situationID.ToString())
                        {
                            foreach (XElement fullAnswer in situation.Elements())
                            {
                                if (fullAnswer.Name == Constants.FullAnswer)
                                {
                                    return fullAnswer.Attribute(Constants.Text).Value;
                                }
                            }
                        }
                    }
                }
            }
            return null;
        }

        //получить перечень обязательных ключевых слов полного ответа (для файлов answerX.xml)
        //при считывании из строки убираются лишние символы, в т.ч. пробелы, и строка переводится в нижний регистр
        public List<string> GetKeywordsRequired(int situationID)
        {
            List<string> keywordsRequired = new List<string>();
            foreach (XElement element in XmlFile.Element(Constants.Answers).Elements())
            {
                if (element.Name == Constants.General)
                {
                    foreach (XElement situation in element.Elements())
                    {
                        if (situation.Attribute(Constants.Id).Value == situationID.ToString())
                        {
                            foreach (XElement keyword in situation.Elements())
                            {
                                if (keyword.Name == Constants.Keyword)
                                {
                                    if (keyword.Attribute(Constants.Required).Value == Boolean.TrueString.ToLower())
                                    {
                                        keywordsRequired.Add(Regex.Replace(keyword.Attribute(Constants.Text).Value.ToLower(), "\\s", ""));
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (Convert.ToInt32(situation.Attribute(Constants.Id).Value) > situationID)
                            {
                                break;
                            }
                        }
                    }
                }
            }
            return keywordsRequired;
        }

        //получить перечень опциональных ключевых слов полного ответа (для файлов answerX.xml)
        //при считывании из строки убираются лишние символы, в т.ч. пробелы, и строка переводится в нижний регистр
        //выполняется в два прохода:
        //сперва выполняется подсчёт числа списков, затем выполняется их считывание и запись в массив
        public List<string>[] GetKeywordsOptional(int situationID)
        {
            int n = 0;
            int id = -1;
            foreach (XElement element in XmlFile.Element(Constants.Answers).Elements())
            {
                if (element.Name == Constants.General)
                {
                    foreach (XElement situation in element.Elements())
                    {
                        if (situation.Name == Constants.Situation)
                        {
                            if (situation.Attribute(Constants.Id).Value == situationID.ToString())
                            {
                                foreach (XElement keyword in situation.Elements())
                                {
                                    if (keyword.Name == Constants.Keyword)
                                    {
                                        if (keyword.Attribute(Constants.Required).Value == Constants.Optional)
                                        {
                                            int currentID = Convert.ToInt32(keyword.Attribute(Constants.Id).Value);
                                            if (currentID != id)
                                            {
                                                id = currentID;
                                                n++;
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (Convert.ToInt32(situation.Attribute(Constants.Id).Value) > situationID)
                                {
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            List<string>[] keywordsOptional = new List<string>[n];
            List<string> keywords = null;
            id = -1;
            n = -1;
            foreach (XElement element in XmlFile.Element(Constants.Answers).Elements())
            {
                if (element.Name == Constants.General)
                {
                    foreach (XElement situation in element.Elements())
                    {
                        if (situation.Name == Constants.Situation)
                        {
                            if (situation.Attribute(Constants.Id).Value == situationID.ToString())
                            {
                                foreach (XElement keyword in situation.Elements())
                                {
                                    if (keyword.Name == Constants.Keyword)
                                    {
                                        if (keyword.Attribute(Constants.Required).Value == Constants.Optional)
                                        {
                                            int currentID = Convert.ToInt32(keyword.Attribute(Constants.Id).Value);
                                            if (currentID != id)
                                            {
                                                if (n != -1)
                                                {
                                                    keywordsOptional[n] = keywords;
                                                }
                                                id = currentID;
                                                n++;
                                                keywords = new List<string>();
                                            }
                                            keywords.Add(Regex.Replace(keyword.Attribute(Constants.Text).Value.ToLower(), "\\s", ""));
                                        }

                                    }
                                }
                            }
                            else
                            {
                                if (Convert.ToInt32(situation.Attribute(Constants.Id).Value) > situationID)
                                {
                                    if (n >= 0)
                                    {
                                        keywordsOptional[n] = keywords;
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (element.Name == Constants.Actions)
                    {
                        if (n >= 0)
                        {
                            keywordsOptional[n] = keywords;
                        }
                        break;
                    }
                }
            }
            return keywordsOptional;
        }

        //получить перечень стоп-слов полного ответа (для файлов answerX.xml)
        //при считывании из строки убираются лишние символы, в т.ч. пробелы, и строка переводится в нижний регистр
        public List<string> GetKeywordsNotRequired(int situationID)
        {
            List<string> keywordsRequired = new List<string>();
            foreach (XElement element in XmlFile.Element(Constants.Answers).Elements())
            {
                if (element.Name == Constants.General)
                {
                    foreach (XElement situation in element.Elements())
                    {
                        if (situation.Attribute(Constants.Id).Value == situationID.ToString())
                        {
                            foreach (XElement keyword in situation.Elements())
                            {
                                if (keyword.Name == Constants.Keyword)
                                {
                                    if (keyword.Attribute(Constants.Required).Value == Boolean.FalseString.ToLower())
                                    {
                                        keywordsRequired.Add(Regex.Replace(keyword.Attribute(Constants.Text).Value.ToLower(), "\\s", ""));
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (Convert.ToInt32(situation.Attribute(Constants.Id).Value) > situationID)
                            {
                                break;
                            }
                        }
                    }
                }
            }
            return keywordsRequired;
        }

        //получить указнный элемент из списка действий для разбора задачи по алгоритму (для файла actions.xml)
        public string GetActionElement(int id)
        {
            string element = "";
            foreach (XElement xelement in XmlFile.Element(Constants.Actions).Elements())
            {
                if (xelement.Name == Constants.Element)
                {
                    if (xelement.Attribute(Constants.Id).Value == id.ToString())
                    {
                        element = xelement.Attribute(Constants.Text).Value;
                        break;
                    }
                }
            }
            return element;
        }

        //получить полный перечень действий алгоритма разбора указнного элемента знаний (файл actions.xml)
        public List<string> GetActions(int id)
        {
            List<string> actions = new List<string>();
            foreach (XElement xelement in XmlFile.Element(Constants.Actions).Elements())
            {
                if (xelement.Name == Constants.Element)
                {
                    if (xelement.Attribute(Constants.Id).Value == id.ToString())
                    {
                        foreach (XElement action in xelement.Elements())
                        {
                            actions.Add(action.Attribute(Constants.Text).Value);
                        }
                        break;
                    }
                }
            }
            return actions;
        }

        //получить форму родительного падежа для указанного элемента знаний (файл elements.xml)
        public string GetElementGenitiveByTaskID(int n)
        {
            foreach (XElement unit in XmlFile.Element(Constants.Units).Elements())
            {
                foreach (XElement topic in unit.Elements())
                {
                    foreach (XElement element in topic.Elements())
                    {
                        if (element.Attribute(Constants.Task).Value == n.ToString())
                        {
                            return element.Attribute(Constants.Genitive).Value;
                        }
                    }
                }
            }
            return "";
        }

        //получить тип ответа на указанном шаге разбора по алгоритму (файл answers.xml)
        //если значение не найдено, возвращается значение по умолчанию - text
        public string GetActionType(int id)
        {
            string actionType = "";
            foreach (XElement element in XmlFile.Element(Constants.Answers).Elements())
            {
                if (element.Name == Constants.Actions)
                {
                    foreach (XElement situation in element.Elements())
                    {
                        if (situation.Attribute(Constants.Id).Value == id.ToString())
                        {
                            foreach (XElement action in situation.Elements())
                            {
                                if (action.Attribute(Constants.Id).Value == id.ToString())
                                {
                                    if (action.Attribute(Constants.Type) != null)
                                        actionType = action.Attribute(Constants.Type).Value;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            if (actionType == "")
                return Constants.Text;
            return actionType;
        }

        //получить перечень обязательных ключевых слов ответа при пошаговом разборе для указанного шага (для файлов answerX.xml)
        //при считывании из строки убираются лишние символы, в т.ч. пробелы, и строка переводится в нижний регистр
        public List<string> GetActionKeywordsRequired(int situationID, int actionID)
        {
            List<string> keywordsRequired = new List<string>();
            foreach (XElement element in XmlFile.Element(Constants.Answers).Elements())
            {
                if (element.Name == Constants.Actions)
                {
                    foreach (XElement situation in element.Elements())
                    {
                        if (situation.Attribute(Constants.Id).Value == situationID.ToString())
                        {
                            foreach (XElement action in situation.Elements())
                            {
                                if (action.Attribute(Constants.Id).Value == actionID.ToString())
                                {
                                    foreach (XElement keyword in action.Elements())
                                    {
                                        if (keyword.Attribute(Constants.Required).Value == Boolean.TrueString.ToLower())
                                        {
                                            keywordsRequired.Add(Regex.Replace(keyword.Attribute(Constants.Text).Value.ToLower(), "\\s", ""));
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (Convert.ToInt32(situation.Attribute(Constants.Id).Value) > situationID)
                            {
                                break;
                            }
                        }
                    }
                }
            }
            return keywordsRequired;
        }

        //получить перечень опциональных ключевых слов ответа при пошаговом разборе для указанного шага (для файлов answerX.xml)
        //при считывании из строки убираются лишние символы, в т.ч. пробелы, и строка переводится в нижний регистр
        //выполняется в два прохода:
        //сперва выполняется подсчёт числа списков, затем выполняется их считывание и запись в массив
        public List<string>[] GetActionKeywordsOptional(int situationID, int actionID)
        {
            int n = 0;
            int id = -1;
            foreach (XElement element in XmlFile.Element(Constants.Answers).Elements())
            {
                if (element.Name == Constants.Actions)
                {
                    foreach (XElement situation in element.Elements())
                    {
                        if (situation.Attribute(Constants.Id).Value == situationID.ToString())
                        {
                            foreach (XElement action in situation.Elements())
                            {
                                if (action.Attribute(Constants.Id).Value == actionID.ToString())
                                {
                                    foreach (XElement keyword in action.Elements())
                                    {
                                        if (keyword.Attribute(Constants.Required).Value == Constants.Optional)
                                        {
                                            int currentID = Convert.ToInt32(keyword.Attribute(Constants.Id).Value);
                                            if (currentID != id)
                                            {
                                                id = currentID;
                                                n++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (Convert.ToInt32(situation.Attribute(Constants.Id).Value) > situationID)
                            {
                                break;
                            }
                        }
                    }
                }
            }
            List<string>[] keywordsOptional = new List<string>[n];
            List<string> keywords = null;
            id = -1;
            n = -1;
            foreach (XElement element in XmlFile.Element(Constants.Answers).Elements())
            {
                if (element.Name == Constants.Actions)
                {
                    foreach (XElement situation in element.Elements())
                    {
                        if (situation.Attribute(Constants.Id).Value == situationID.ToString())
                        {
                            foreach (XElement action in situation.Elements())
                            {
                                if (action.Attribute(Constants.Id).Value == actionID.ToString())
                                {
                                    foreach (XElement keyword in action.Elements())
                                    {
                                        if (keyword.Attribute(Constants.Required).Value == Constants.Optional)
                                        {
                                            int currentID = Convert.ToInt32(keyword.Attribute(Constants.Id).Value);
                                            if (currentID != id)
                                            {
                                                if (n != -1)
                                                {
                                                    keywordsOptional[n] = keywords;
                                                }
                                                id = currentID;
                                                n++;
                                                keywords = new List<string>();
                                            }
                                            keywords.Add(Regex.Replace(keyword.Attribute(Constants.Text).Value.ToLower(), "\\s", ""));
                                        }
                                        if (keywords != null && action.Elements().Last().Equals(keyword))
                                        {
                                            keywordsOptional[n] = keywords;
                                        }
                                    }
                                }
                                else
                                {
                                    if (Convert.ToInt32(action.Attribute(Constants.Id).Value) > actionID)
                                    {
                                        break;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (Convert.ToInt32(situation.Attribute(Constants.Id).Value) > situationID)
                            {
                                break;
                            }
                        }
                    }
                }
            }
            return keywordsOptional;
        }

        //получить перечень стоп-слов ответа при пошаговом разборе для указанного шага (для файлов answerX.xml)
        //при считывании из строки убираются лишние символы, в т.ч. пробелы, и строка переводится в нижний регистр
        public List<string> GetActionKeywordsNotRequired(int situationID, int actionID)
        {
            List<string> keywordsRequired = new List<string>();
            foreach (XElement element in XmlFile.Element(Constants.Answers).Elements())
            {
                if (element.Name == Constants.Actions)
                {
                    foreach (XElement situation in element.Elements())
                    {
                        if (situation.Attribute(Constants.Id).Value == situationID.ToString())
                        {
                            foreach (XElement action in situation.Elements())
                            {
                                if (action.Attribute(Constants.Id).Value == actionID.ToString())
                                {
                                    foreach (XElement keyword in action.Elements())
                                    {
                                        if (keyword.Attribute(Constants.Required).Value == Boolean.FalseString.ToLower())
                                        {
                                            keywordsRequired.Add(Regex.Replace(keyword.Attribute(Constants.Text).Value.ToLower(), "\\s", ""));
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (Convert.ToInt32(situation.Attribute(Constants.Id).Value) > situationID)
                            {
                                break;
                            }
                        }
                    }
                }
            }
            return keywordsRequired;
        }

        //получить тип задания (файл taskX.xml)
        public string GetTaskType()
        {
            return XmlFile.Element(Constants.Task).Attribute(Constants.Type).Value;
        }

        //получить тип конечного ответа (файл taskX.xml)
        public string GetAnswerType()
        {
            return XmlFile.Element(Constants.Task).Attribute(Constants.Answer).Value;
        }

        //получить номер задачи, из которой необходимо брать изображения для условия (файл taskX.xml)
        public string GetSourceImages()
        {
            return XmlFile.Element(Constants.Task).Attribute(Constants.SourceImages).Value;
        }

        //получить номер задачи, из которой необходимо брать перечень текстовых ситуаций (файл taskX.xml)
        public string GetSource()
        {
            try
            {
                return XmlFile.Element(Constants.Task).Attribute(Constants.Source).Value;
            }
            catch (NullReferenceException)
            {
                return null;
            }
        }

        //получить тип ответов при пошаговом разборе (файл taskX.xml)
        public string GetStepsType()
        {
            try
            {
                return XmlFile.Element(Constants.Task).Attribute(Constants.Steps).Value;
            }
            catch (NullReferenceException)
            {
                return Constants.Text;
            }
        }

        //проверить, является ли нужная формула векторной (файл answersX.xml)
        public bool isVector(int situationID, int formulaID)
        {
            foreach (XElement element in XmlFile.Element(Constants.Answers).Elements())
            {
                if (element.Name == Constants.General)
                {
                    foreach (XElement situation in element.Elements())
                    {
                        if (situation.Name == Constants.Situation)
                        {
                            if (Convert.ToInt32(situation.Attribute(Constants.Id).Value) == situationID)
                            {
                                foreach (XElement formula in situation.Elements())
                                {
                                    if (formula.Name == Constants.Formula)
                                    {
                                        if (Convert.ToInt32(formula.Attribute(Constants.Id).Value) == formulaID)
                                        {
                                            return (Convert.ToBoolean(formula.Attribute(Constants.Vector).Value));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }

        //получить количество формул, которые должны быть в ответе по указанной ситуации (файл answersX.xml)
        public int GetFormulasCount(int situationID)
        {
            foreach (XElement element in XmlFile.Element(Constants.Answers).Elements())
            {
                if (element.Name == Constants.General)
                {
                    foreach (XElement situation in element.Elements())
                    {
                        if (situation.Name == Constants.Situation)
                        {
                            if (Convert.ToInt32(situation.Attribute(Constants.Id).Value) == situationID)
                            {
                                return (Convert.ToInt32(situation.Attribute(Constants.Count).Value));
                            }
                        }
                    }
                }
            }
            return 0;
        }

        //получить количество векторов в векторной формуле (файл answersX.xml)
        public int GetVectorsCount(int situationID, int formulaID)
        {
            foreach (XElement element in XmlFile.Element(Constants.Answers).Elements())
            {
                if (element.Name == Constants.General)
                {
                    foreach (XElement situation in element.Elements())
                    {
                        if (situation.Name == Constants.Situation)
                        {
                            if (Convert.ToInt32(situation.Attribute(Constants.Id).Value) == situationID)
                            {
                                foreach (XElement formula in situation.Elements())
                                {
                                    if (formula.Name == Constants.Formula)
                                    {
                                        if (Convert.ToInt32(formula.Attribute(Constants.Id).Value) == situationID)
                                        {
                                            return Convert.ToInt32(formula.Attribute(Constants.Count).Value);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return 0;
        }

        //получить значения векторов в формуле для проверки формульного ответа (файл answersX.xml)
        public int[] GetVectorsValues(int situationID, int formulaID, int vectorsCount, string variable)
        {
            int[] values = new int[vectorsCount];
            foreach (XElement element in XmlFile.Element(Constants.Answers).Elements())
            {
                if (element.Name == Constants.General)
                {
                    foreach (XElement situation in element.Elements())
                    {
                        if (situation.Name == Constants.Situation)
                        {
                            if (Convert.ToInt32(situation.Attribute(Constants.Id).Value) == situationID)
                            {
                                foreach (XElement formula in situation.Elements())
                                {
                                    if (formula.Name == Constants.Formula)
                                    {
                                        if (Convert.ToInt32(formula.Attribute(Constants.Id).Value) == formulaID)
                                        {
                                            for (int i = 0; i < vectorsCount; i++)
                                            {
                                                values[i] = Convert.ToInt32(formula.Elements().ElementAt(i).Attribute(variable).Value);
                                            }
                                            break;
                                        }
                                    }

                                }
                                break;
                            }
                        }

                    }
                    break;
                }
            }
            return values;
        }

        //получить результирующий вектор (файл answersX.xml)
        public int[,] GetResultVector(int situationID, int formulaID)
        {
            int[,] values = null;
            foreach (XElement element in XmlFile.Element(Constants.Answers).Elements())
            {
                if (element.Name == Constants.General)
                {
                    foreach (XElement situation in element.Elements())
                    {
                        if (situation.Name == Constants.Situation)
                        {
                            if (Convert.ToInt32(situation.Attribute(Constants.Id).Value) == situationID)
                            {
                                foreach (XElement formula in situation.Elements())
                                {
                                    if (formula.Name == Constants.Formula) {
                                        if (Convert.ToInt32(formula.Attribute(Constants.Id).Value) == formulaID)
                                        {
                                            foreach (XElement result in formula.Elements())
                                            {
                                                if (result.Name == Constants.Result)
                                                {
                                                    int steps = GetStepsCount(situationID, formulaID, true);
                                                    values = new int[steps + 1, 2];
                                                    for (int i = 0; i < steps + 1; i++)
                                                    {
                                                        values[i, 0] = Convert.ToInt32(result.Attribute(Constants.x0).Value)
                                                            + Convert.ToInt32(result.Attribute(Constants.xi).Value) * i;
                                                        values[i, 1] = Convert.ToInt32(result.Attribute(Constants.y0).Value)
                                                            + Convert.ToInt32(result.Attribute(Constants.yi).Value) * i;
                                                    }
                                                    break;
                                                }
                                            }
                                            break;
                                        }
                                    }
                                }
                                break;
                            }
                        }
                    }
                    break;
                }
            }
            return values;
        }

        //получить указанную часть (левую или правую) уравнения (файл answersX.xml) 
        public string GetEquationPart(int situationID, int formulaID, string part)
        {
            foreach (XElement element in XmlFile.Element(Constants.Answers).Elements())
            {
                if (element.Name == Constants.General)
                {
                    foreach (XElement situation in element.Elements())
                    {
                        if (situation.Name == Constants.Situation)
                        {
                            if (Convert.ToInt32(situation.Attribute(Constants.Id).Value) == situationID)
                            {
                                foreach (XElement formula in situation.Elements())
                                {
                                    if (formula.Name == Constants.Formula)
                                    {
                                        if (Convert.ToInt32(formula.Attribute(Constants.Id).Value) == formulaID)
                                        {
                                            foreach (XElement variables in formula.Elements())
                                            {
                                                if (variables.Name == Constants.Variables)
                                                {
                                                    return variables.Attribute(part).Value;
                                                }
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            }
            return null;
        }

        //получить значения переменных в скалярной формуле для проверки ответа (файл answersX.xml) 
        public double[,] GetFormulaValues(int situationID, int formulaID)
        {
            double[,] values = null;
            foreach (XElement element in XmlFile.Element(Constants.Answers).Elements())
            {
                if (element.Name == Constants.General)
                {
                    foreach (XElement situation in element.Elements())
                    {
                        if (situation.Name == Constants.Situation)
                        {
                            if (Convert.ToInt32(situation.Attribute(Constants.Id).Value) == situationID)
                            {
                                foreach (XElement formula in situation.Elements())
                                {
                                    if (formula.Name == Constants.Formula)
                                    {
                                        if (Convert.ToInt32(formula.Attribute(Constants.Id).Value) == formulaID)
                                        {
                                            XElement valuesElement = formula.Elements().ElementAt(0);
                                            int count = valuesElement.Attributes().Count();
                                            values = new double[count / 3, 3];
                                            for (int i = 0; i < count / 3; i++)
                                            {
                                                for (int j = 0; j < 3; j++)
                                                {
                                                    values[i, j] = Convert.ToDouble(valuesElement.Attributes().ElementAt(i * 3 + j).Value);
                                                }
                                            }
                                            break;
                                        }
                                    }

                                }
                                break;
                            }
                        }
                    }
                    break;
                }
            }
            return values;
        }

        //получить правильынй результат скалярной формулы (файл answersX.xml) 
        public string GetFormulaResult(int situationID, int formulaID)
        {
            string result = null;
            foreach (XElement element in XmlFile.Element(Constants.Answers).Elements())
            {
                if (element.Name == Constants.General)
                {
                    foreach (XElement situation in element.Elements())
                    {
                        if (situation.Name == Constants.Situation)
                        {
                            if (Convert.ToInt32(situation.Attribute(Constants.Id).Value) == situationID)
                            {
                                foreach (XElement formula in situation.Elements())
                                {
                                    if (formula.Name == Constants.Formula)
                                    {
                                        if (Convert.ToInt32(formula.Attribute(Constants.Id).Value) == formulaID)
                                        {
                                            result = formula.Elements().ElementAt(1).Value;
                                            break;
                                        }
                                    }
                                }
                                break;
                            }
                        }
                    }
                    break;
                }
            }
            return result;
        }

        //получить количество шагов для проверки формулы (файл answersX.xml) 
        public int GetStepsCount(int situationID, int formulaID, bool isVector)
        {
            foreach (XElement element in XmlFile.Element(Constants.Answers).Elements())
            {
                if (element.Name == Constants.General)
                {
                    foreach (XElement situation in element.Elements())
                    {
                        if (situation.Name == Constants.Situation)
                        {
                            if (Convert.ToInt32(situation.Attribute(Constants.Id).Value) == situationID)
                            {
                                foreach (XElement formula in situation.Elements())
                                {
                                    if (formula.Name == Constants.Formula)
                                    {
                                        if (Convert.ToInt32(formula.Attribute(Constants.Id).Value) == formulaID)
                                        {
                                            foreach (XElement result in formula.Elements())
                                            {
                                                if (result.Name == Constants.Result)
                                                {
                                                    //вычисляем количество шагов для проверки формулы
                                                    if (isVector)
                                                    {
                                                        double stepsX = 0, stepsY = 0;
                                                        if (Convert.ToInt32(result.Attribute(Constants.xi).Value) != 0)
                                                            stepsX = (Convert.ToInt32(result.Attribute(Constants.x1).Value) - Convert.ToInt32(result.Attribute(Constants.x0).Value)) /
                                                                Convert.ToInt32(result.Attribute(Constants.xi).Value);
                                                        if (Convert.ToInt32(result.Attribute(Constants.yi).Value) != 0)
                                                            stepsY = (Convert.ToInt32(result.Attribute(Constants.y1).Value) - Convert.ToInt32(result.Attribute(Constants.y0).Value)) /
                                                                Convert.ToInt32(result.Attribute(Constants.yi).Value);
                                                        if ((int)stepsX != 0 && (int)stepsY != 0)
                                                        {
                                                            if ((int)stepsX != (int)stepsY)
                                                                throw new FormatException(Constants.ErrorWrongFormulaData);
                                                        }
                                                        double stepsXRes = stepsX;
                                                        double stepsYRes = stepsY;
                                                        foreach (XElement vector in formula.Elements())
                                                        {
                                                            if (vector.Name == Constants.Vector)
                                                            {
                                                                if (Convert.ToInt32(vector.Attribute(Constants.xi).Value) != 0)
                                                                    stepsX = (Convert.ToInt32(vector.Attribute(Constants.x1).Value) - Convert.ToInt32(vector.Attribute(Constants.x0).Value)) /
                                                                        Convert.ToInt32(vector.Attribute(Constants.xi).Value);
                                                                if (Convert.ToInt32(vector.Attribute(Constants.yi).Value) != 0)
                                                                    stepsY = (Convert.ToInt32(vector.Attribute(Constants.y1).Value) - Convert.ToInt32(vector.Attribute(Constants.y0).Value)) /
                                                                            Convert.ToInt32(vector.Attribute(Constants.yi).Value);
                                                                if ((int)stepsX != 0 && (int)stepsY != 0)
                                                                {
                                                                    if ((int)stepsX != (int)stepsY)
                                                                        throw new FormatException(Constants.ErrorWrongFormulaData);
                                                                }
                                                                if ((int)stepsX != 0 && (int)stepsXRes != 0)
                                                                {
                                                                    if ((int)stepsX != (int)stepsXRes)
                                                                        throw new FormatException(Constants.ErrorWrongFormulaData);
                                                                }
                                                                if ((int)stepsY != 0 && (int)stepsYRes != 0)
                                                                {
                                                                    if ((int)stepsY != (int)stepsYRes)
                                                                        throw new FormatException(Constants.ErrorWrongFormulaData);
                                                                }
                                                            }
                                                        }
                                                        return (int)stepsXRes;
                                                    }
                                                    else
                                                    {
                                                        double[] steps = new double[GetFormulaValues(situationID, formulaID).GetLength(0)];
                                                        foreach (XElement values in formula.Elements())
                                                        {
                                                            if (values.Name == Constants.Values)
                                                            {
                                                                for (int i = 0; i < steps.Length; i++)
                                                                {
                                                                    if (Convert.ToDouble(values.Attribute(defaultVariables[i] + "i").Value) != 0)
                                                                        steps[i] = (Convert.ToDouble(values.Attribute(defaultVariables[i] + "1").Value) -
                                                                            Convert.ToDouble(values.Attribute(defaultVariables[i] + "0").Value)) /
                                                                            Convert.ToDouble(values.Attribute(defaultVariables[i] + "i").Value);
                                                                }
                                                                for (int i = 0; i < steps.Length - 1; i++)
                                                                {
                                                                    if (steps[i] - (int)steps[i] > 0)
                                                                        steps[i] = (int)steps[i] + 1;
                                                                    if (steps[i + 1] - (int)steps[i + 1] > 0)
                                                                        steps[i + 1] = (int)steps[i + 1] + 1;
                                                                    if ((int)steps[i] != 0 && (int)steps[i + 1] != 0)
                                                                    {
                                                                        if ((int)steps[i] != (int)steps[i + 1])
                                                                            throw new FormatException(Constants.ErrorWrongFormulaData);
                                                                    }
                                                                }

                                                            }
                                                        }
                                                        return (int)steps[0];
                                                    }
                                                }
                                            }
                                            break;
                                        }
                                    }
                                }
                                break;
                            }
                        }
                    }
                    break;
                }
            }
            return 0;
        }

        //получить текст помощи (файл help/*имя слайда*/text.xml)
        public string GetHelp()
        {
            return XmlFile.Root.Value;
        }

        //получить общее число изображений для всех ситуаций в задаче (файл answersX.xml)
        public int GetCountImages()
        {
            return Convert.ToInt32(XmlFile.Element(Constants.Task).Attribute(Constants.CountImages).Value);
        }
    }
}
