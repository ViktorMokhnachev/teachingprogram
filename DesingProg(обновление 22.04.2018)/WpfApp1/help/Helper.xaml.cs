﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace Patulas.help
{
    /// <summary>
    /// Логика взаимодействия для hellper.xaml
    /// </summary>
    public partial class Helper : UserControl
    {
        private UserControl slide;
        private string slideName;

        public Helper(UserControl slide)
        {
            InitializeComponent();
            try
            {
                this.slide = slide;
                slideName = slide != null ? slide.GetType().Name : Constants.MainWindow;
                string path = slideName + Constants.PathToHelpImage;
                Image.Source = new BitmapImage(new Uri(path, UriKind.Relative));
                path = Constants.PathToHelp + slideName + Constants.PathToHelpText;
                XmlDataAdapter helper = new XmlDataAdapter(path);
                Helptext.Text = helper.GetHelp();
            }
            catch (Exception)
            {
                MessageBox.Show(Constants.ErrorNoDataForThisSlide, Constants.Error, MessageBoxButton.OK, MessageBoxImage.Error);
                Image.Source = null;
                Helptext.Text = "";
            }
        }

        private void Close_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (slide != null)
            {
                MainWindow.get().OpenNewSlide(slide);
            }
            else
            {
                MainWindow.get().TaskView.Children.Clear();
            }
        }
    }
}
