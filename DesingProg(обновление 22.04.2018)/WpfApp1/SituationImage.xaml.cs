﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Patulas
{
    /// <summary>
    /// Логика взаимодействия для SituationImage.xaml
    /// </summary>
    public partial class SituationImageWindow : Window
    {
        public static int count = 0;

        public SituationImageWindow(Image image)
        {
            InitializeComponent();
            SituationImage.Source = image.Source;
            count++;
        }

        public void SetNewImage(Image image)
        {
            if (count > 0 && image != null)
            {
                SituationImage.Source = image.Source;
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            count--;
        }
    }
}
