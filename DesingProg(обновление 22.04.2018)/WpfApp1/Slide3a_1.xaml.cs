﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Patulas
{
    /// <summary>
    /// Логика взаимодействия для Slide3a_1.xaml
    /// </summary>
    public partial class Slide3a_1 : UserControl
    {
        private List<TextBox> answerFields = new List<TextBox>();
        private List<string> answers;
        private int situationID;
        private int n;
        private int id;
        private Slide3c_1 source1;
        private IActions source2;
        private int actionsCount;
        private bool isSecondSituation;
        private Image situationImage = new Image();
        private Button[] buttonsSelectImage;
        private List<string> actions = new List<string>();
        public bool[] correctAnswers;
        private string answerType;

        public Slide3a_1(IActions source, int id, int situationID, bool isSecondSituation)
        {
            InitializeComponent();
            actionsCount = source.GetActions().Count;
            string taskType = source.GetTaskType();
            answerType = source.GetAnswerType();
            if (answerType == Constants.Images)
            {
                buttonsSelectImage = new Button[actionsCount];
                BtnShowPattern.Visibility = Visibility.Hidden;
            }
            if (taskType.Contains(Constants.Text))
                SituationText.Text = source.GetSituation(id);
            if (taskType.Contains(Constants.Images))
                situationImage.Source = source.GetImage(id).Source;
            this.situationID = source.GetSelectedSituation(id);
            SituationNumber.Content += situationID.ToString();
            n = source.GetNumber();
            source2 = source;
            this.id = id;
            this.isSecondSituation = isSecondSituation;
            actions = source.GetActions();
            GenerateGrid();
            correctAnswers = new bool[answerFields.Count];
        }

        public Slide3a_1(Slide3c_1 source)
        {
            InitializeComponent();
            actionsCount = source.actions.Count;
            source1 = source;
            situationID = source.situationID;
            string taskType = source.GetTaskType();
            answerType = source.answerType;
            if (answerType == Constants.Images)
                buttonsSelectImage = new Button[actionsCount];
            if (taskType.Contains(Constants.Text))
                SituationText.Text = source.situation;
            if (taskType.Contains(Constants.Images))
                situationImage.Source = source.image.Source;
            SituationNumber.Content += (id + 1).ToString();
            n = source.n;
            actions = source.actions;
            GenerateGrid();
            correctAnswers = new bool[answerFields.Count];
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            bool allAnswerFieldsAreFilled = true;
            foreach (TextBox answer in answerFields)
            {
                if (answer.IsEnabled)
                {
                    if (answer.Text == "")
                    {
                        allAnswerFieldsAreFilled = false;
                        break;
                    }
                }
                //answer = Utils.CheckForGreekLetters(answer);
            }
            if (answerType == Constants.Images)
            {
                if (allAnswerFieldsAreFilled)
                    buttonsSelectImage.Last().Visibility = Visibility.Visible;
                else buttonsSelectImage.Last().Visibility = Visibility.Hidden;
            }
            else
            {
                BtnCheck.IsEnabled = allAnswerFieldsAreFilled;
            }
        }

        private void BtnCheck_Click(object sender, RoutedEventArgs e)
        {
            foreach (TextBox answer in answerFields)
            {
                answer.IsReadOnly = true;
            }
            //количество неверных
            int count = 0;
            //номер неверного
            int[] wrong = new int[answerFields.Count];
            answers = new List<string>();
            foreach (TextBox textbox in answerFields)
            {
                answers.Add(textbox.Text);
            }
            XmlDataAdapter actionAnswers = null;
            actionAnswers = new XmlDataAdapter("task/answers/answers" + n + ".xml");
            List<AnswerChecker> checkers = new List<AnswerChecker>(answerFields.Count);
            for (int i = 0; i < checkers.Capacity - 1; i++)
            {
                checkers.Add(new AnswerChecker(actionAnswers, situationID, i));
            }
            checkers.Add(new AnswerChecker(actionAnswers, situationID));
            bool allCorrect = true;
            try
            {
                for (int i = 0; i < checkers.Count; i++)
                {
                    if (answerFields[i].IsEnabled)
                        correctAnswers[i] = checkers[i].CheckAnswer(answers[i], actionAnswers.GetActionType(i));
                }
            }
            catch (System.Runtime.InteropServices.COMException)
            {
                return;
            }
            for (int i = 0; i < correctAnswers.Length; i++)
            {
                if (!correctAnswers[i])
                {
                    allCorrect = false;
                    wrong[count] = i;
                    count++;
                }
            }
            if (isSecondSituation)
            {
                ((Slide3c_2)source2).ShowButtonSelectThirdSituation();
            }
            if (allCorrect || (correctAnswers.Last() && count <= 1))
            {
                //Globals.get().AddRight(); возможно из-за этого неправильно считает число верных
                if (source1 != null)
                {
                    if (Globals.get().GetTotal() == 2)
                        MainWindow.get().OpenNewSlide(new Slide4(n, source1.slideElements, source1.taskAnswer, source1.answerType, allCorrect));
                    else
                    {
                        MainWindow.get().OpenNewSlide(new Slide3b_1(n, source1.slideElements, source1.GetTaskType(), source1.taskAnswer, source1.answerType, allCorrect));
                    }
                }
                else
                {
                    source2.HideButtonParse(id);
                    source2.HighlightCorrectAnswer(id);
                    source2.SendAnswer(id, answers.Last());
                    if (source2.GetSelectedSituationsCount() == source2.GetMaxSituationsCount() &&  source2.GetLeftToParse() <= 0 && !isSecondSituation)
                    {
                        source2.AllowContinue();
                    }
                    else
                    {
                        if (answerType == Constants.Images)
                            source2.ShowNextSelectButton(id);
                    }
                    MainWindow.get().OpenNewSlide((UserControl)source2);
                }
            }
            else
            {
                //если не все шаги были написаны верно и/или в результате был дан неверный ответ,
                //подсвечиваем красным соответствующие поля и выводим правильные ответы
                Globals.get().AddWrong();
                for (int i = 0; i < count; i++)
                {
                    answerFields[wrong[i]].Background = new SolidColorBrush(Color.FromArgb(200, 255, 0, 0));
                }
                BtnCheck.Click -= BtnCheck_Click;
                BtnCheck.Click += Continue;
                BtnCheck.Content = Constants.Continue;
                if (source1 == null)
                {
                    if (source2.GetSelectedSituationsCount() == source2.GetMaxSituationsCount() && source2.GetLeftToParse() <= 0 && !isSecondSituation)
                    {
                        source2.AllowContinue();
                    }
                    else
                    {
                        if (answerType == Constants.Images)
                            source2.ShowNextSelectButton(id);
                    }
                    source2.HideButtonParse(id);
                    source2.SendAnswer(id, answers.Last());
                }
            }
        }

        private void Continue(object sender, RoutedEventArgs e)
        {
            if (source1 != null)
            {
                UserControl next;
                if (Globals.get().GetTotal() == 2)
                    next = new Slide4(n, source1.slideElements, source1.taskAnswer, source1.answerType, false);
                else next = new Slide3b_1(n, source1.slideElements, source1.GetTaskType(), source1.taskAnswer, source1.answerType, false);
                MainWindow.get().OpenNewSlide(next);
            }
            else
            {
                MainWindow.get().OpenNewSlide((UserControl)source2);
            }
        }

        private void GenerateGrid()
        {
            for (int i = 0; i < actionsCount; i++)
            {
                RowDefinition rowDefinition = new RowDefinition();
                rowDefinition.Height = new GridLength(60);
                GridMain.RowDefinitions.Add(rowDefinition);
                
                Label label = new Label();
                label.Content = (i + 1) + ".";
                label.HorizontalAlignment = HorizontalAlignment.Center;
                label.VerticalAlignment = VerticalAlignment.Center;
                label.Height = 60;
                label.Width = 33;
                label.FontSize = 24;
                label.Margin = new Thickness(5, 5, 5, 10);
                label.HorizontalContentAlignment = HorizontalAlignment.Center;
                label.VerticalContentAlignment = VerticalAlignment.Center;
                GridMain.Children.Add(label);
                Grid.SetRow(label, i);
                
                TextBlock textBlock = new TextBlock();
                textBlock.HorizontalAlignment = HorizontalAlignment.Left;
                textBlock.TextWrapping = TextWrapping.Wrap;
                textBlock.VerticalAlignment = VerticalAlignment.Top;
                textBlock.Height = 60;
                textBlock.Width = 341;
                textBlock.Margin = new Thickness(5, 5, 0, 0);
                textBlock.Padding = new Thickness(5, 5, 0, 0);
                textBlock.Background = new SolidColorBrush(Color.FromArgb(178, 176, 196, 222));//#B2B0C4DE
                GridMain.Children.Add(textBlock);
                textBlock.Text = actions[i];
                Grid.SetColumn(textBlock, 1);
                Grid.SetRow(textBlock, i);
                TextBox textBox = new TextBox();
                textBox.HorizontalAlignment = HorizontalAlignment.Left;
                textBox.Height = 60;
                textBox.TextWrapping = TextWrapping.Wrap;
                textBox.VerticalAlignment = VerticalAlignment.Top;
                textBox.Width = 341;
                MaterialDesignThemes.Wpf.HintAssist.SetHint(textBox, Constants.EnterAnswerHint);
                textBox.BorderBrush = null;
                textBox.Background = new SolidColorBrush(Color.FromArgb(178, 176, 196, 222));//#B2B0C4DE
                textBox.CaretBrush = new SolidColorBrush(Color.FromArgb(255, 28, 75, 112));//#FF1C4B70
                textBox.SelectionBrush = new SolidColorBrush(Color.FromArgb(255, 31, 126, 201));//#FF1F7EC
                textBox.Margin = new Thickness(5, 5, 0, 0);
                textBox.TextChanged += TextBox_TextChanged;
                answerFields.Add(textBox);
                GridMain.Children.Add(textBox);
                Grid.SetColumn(textBox, 2);
                Grid.SetRow(textBox, i);
                if (i == actionsCount - 1 && answerType == Constants.Images)
                {
                    textBox.IsEnabled = false;
                    MaterialDesignThemes.Wpf.HintAssist.SetHint(textBox, null);
                    Button button = new Button();
                    button.Content = Constants.SelectCorrectImage;
                    button.HorizontalAlignment = HorizontalAlignment.Center;
                    button.VerticalAlignment = VerticalAlignment.Center;
                    button.Click += BtnSelectImage_Click;
                    Grid.SetColumn(button, 2);
                    Grid.SetRow(button, i);
                    buttonsSelectImage[i] = button;
                    button.Visibility = Visibility.Hidden;
                    GridMain.Children.Add(button);
                }
            }
        }

        private void BtnSelectImage_Click(object sender, RoutedEventArgs e)
        {
            Button clicked = (Button)sender;
            int id = Array.IndexOf(buttonsSelectImage, clicked);
            int situationID = this.situationID;
            /*if (source2 != null)
                situationID--;*/
            MainWindow.get().OpenNewSlide(new SlideTaskImages(this, n, id, Utils.GetAnswerImages(n, situationID), Constants.Text, actions[id]));
        }

        public void HighlightWrongAction(int actionID)
        {
            answerFields[actionID].Background = new SolidColorBrush(Color.FromArgb(200, 255, 0, 0));
            buttonsSelectImage[actionID].Visibility = Visibility.Hidden;
            BtnCheck.IsEnabled = true;
            foreach (TextBox answer in answerFields)
            {
                answer.IsReadOnly = true;
            }
        }

        public void HighlightCorrectAction(int actionID)
        {
            answerFields[actionID].Background = new SolidColorBrush(Color.FromArgb(200, 31, 126, 201));//FF1F7EC9
            buttonsSelectImage[actionID].Visibility = Visibility.Hidden;
            BtnCheck.IsEnabled = true;
            foreach (TextBox answer in answerFields)
            {
                answer.IsReadOnly = true;
            }
        }

        private void BtnShowTask_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.OpenTaskWindow(Globals.get().GetTaskText());
        }

        private void BtnShowPattern_Click(object sender, RoutedEventArgs e)
        {
            XmlDataAdapter taskAnswer = new XmlDataAdapter("task/answers/answers" + n + ".xml");
            MainWindow.OpenPatternWindow(taskAnswer.GetAnswerPattern());
        }
    }
}
