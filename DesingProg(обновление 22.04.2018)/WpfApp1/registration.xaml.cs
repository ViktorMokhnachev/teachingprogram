﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Patulas
{
    /// <summary>
    /// Логика взаимодействия для registration.xaml
    /// </summary>
    public partial class Registration : Window
    {
        private int MaxUserNameLength;
        private int MaxRealNameLength;
        private int MaxGroupLength;
        private int MaxPasswordLength = 20;

        public Registration()
        {
            try
            {
                InitializeComponent();
                InputLanguageManager.SetInputLanguage(this, System.Globalization.CultureInfo.CreateSpecificCulture("en-US"));
                MaxUserNameLength = DatabaseAdapter.GetMaxLength(Constants.TableUsers, Constants.ColumnLogin);
                MaxRealNameLength = DatabaseAdapter.GetMaxLength(Constants.TableUsers, Constants.ColumnName);
                MaxGroupLength = DatabaseAdapter.GetMaxLength(Constants.TableUsers, Constants.ColumnGroup);
            }
            catch (Npgsql.NpgsqlException)
            {
                MessageBox.Show(Constants.SocketExceptionMessage, Constants.Error, MessageBoxButton.OK, MessageBoxImage.Error);
                Close();
            }
        }

        private void Close_MouseUp(object sender, MouseButtonEventArgs e)
        {
            Close();
        }

        private void Window_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            Key key = e.Key;
            if (key == Key.Escape)
            {
                Close();
            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string login = Login.Text.ToLower();
            if (login == "")
            {
                MessageBox.Show(Constants.ErrorEnterUserName, Constants.Error, MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (login.Length > MaxUserNameLength)
            {
                MessageBox.Show(Constants.ErrorLoginIsTooLong, Constants.Error, MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (RealName.Text == "")
            {
                MessageBox.Show(Constants.ErrorEnterRealName, Constants.Error, MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (RealName.Text.Length > MaxRealNameLength)
            {
                MessageBox.Show(Constants.ErrorRealNameIsTooLong + MaxRealNameLength + Constants.ErrorIsTooLong,
                    Constants.Error, MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (Group.Text == "")
            {
                MessageBox.Show(Constants.ErrorEnterGroup, Constants.Error, MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (Group.Text.Length > MaxGroupLength)
            {
                MessageBox.Show(Constants.ErrorGroupIsTooLong + MaxGroupLength + Constants.ErrorIsTooLong,
                    Constants.Error, MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (Password.Password == "")
            {
                MessageBox.Show(Constants.ErrorEnterPassword, Constants.Error, MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (Password.Password.Length > MaxPasswordLength)
            {
                MessageBox.Show(Constants.ErrorPasswordIsTooLong, Constants.Error, MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            if (Password.Password != PasswordRepeat.Password)
            {
                MessageBox.Show(Constants.ErrorPasswordsDoNotMatch, Constants.Error, MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            else
            {
                try
                {
                    User user = DatabaseAdapter.AddNewUser(login, Password.Password, RealName.Text, Group.Text);
                    if (user != null)
                    {
                        MainWindow.get().Register.Content = Constants.ExitAccount;
                        MainWindow.get().SetExitAccountHandler();
                        MainWindow.get().Login.Content = Constants.Journal;
                        MainWindow.get().SetJournalHandler();
                        MainWindow.get().SetUser(user);
                        MainWindow.get().UnlockButtons();
                        MainWindow.get().UserName.Content = Constants.Welcome + user.GetName();
                        MainWindow.get().InfoRectangle.Visibility = Visibility.Hidden;
                        MainWindow.get().InfoText.Visibility = Visibility.Hidden;
                    }
                    Close();
                }
                catch (ArgumentException)
                {
                    MessageBox.Show(Constants.ErrorUserExists, Constants.Error, MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Login.Focus();
        }
       
    }
}
