﻿using System.Collections.Generic;
using System.Windows.Controls;

namespace Patulas
{
    /**
     * Интерфейс, который должны реализовывать слайды,
     * на которых требуется дать ответ сразу на несколько ситуаций.
     * Данный интерфейс необходим для упрощения кода взаимодействия
     * между выше указанными слайдами со слайдами пошагового разбора
     * и выбора элемента знаний
     * */
    public interface IActions
    {
        int GetNumber();
        List<string> GetActions();
        string GetSituation(int id);
        Image GetImage(int id);
        int GetCurrentSituation();
        void HideButtonSelect(int id);
        void HideButtonParse(int id);
        void HighlightCorrectAnswer(int id);
        void HighlightWrongAnswer(int id);
        void ShowAnswerField(int id);
        void SetAnswerFieldUid(int id, string uid);
        void ShowSituationField(int id);
        void SetSituationFieldText(int id, string text);
        void SetSelectedSituation(int id, int situationID);
        int GetSelectedSituation(int id);
        void ShowCurrentAnswerField();
        void AddSituationText(string text);
        void AddSituationImage(Image image);
        void SendAnswer(int id, string text);
        int GetLeftToParse();
        void AllowContinue();
        UserControl GetNextSlide();
        void ShowOpenImageButton(int id);
        string GetTaskType();
        string GetAnswerType();
        void ShowNextSelectButton(int id);
        void ShowSelectImageButton(int id);
        void HideSelectImageButton(int id);
        int GetMaxSituationNumber();
        int GetMinSituationNumber();
        int GetSelectedSituationsCount();
        int GetMaxSituationsCount();
    }
}
