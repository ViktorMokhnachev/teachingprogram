﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace Patulas
{
    /// <summary>
    /// Логика взаимодействия для Slide4.xaml
    /// </summary>
    public partial class Slide4 : UserControl
    {
        private int n;
        private ISituations slideElements;
        private string answerType;
        private string taskType;
        private AnswerChecker checker;
        private Image image;
        private int situationID;
        private string situation;
        private XmlDataAdapter taskAnswer;
        //количество попыток (только для задач с выбором картинки)
        private int countTry = 0;
        //отношение числа правильно решённых ситуаций к общему числу решённых
        private double correctPercent;
        private List<ListViewItem> situations = null;

        public Slide4(int n, ISituations slideElements, XmlDataAdapter taskAnswer, string answerType, bool isCorrect)
        {
            InitializeComponent();
            this.n = n;
            this.slideElements = slideElements;
            this.answerType = answerType;
            if (!isCorrect)
                CorrectAnswerText.Text = "";
            situationID = GetRandomAvailableSituation();
            taskType = slideElements.GetTaskType();
            this.taskAnswer = taskAnswer;
            correctPercent = Globals.get().GetCountRight() / Globals.get().GetSelected();
            if (answerType != Constants.Images)
            {
                checker = new AnswerChecker(taskAnswer, situationID);
                BtnSelectImage.Visibility = Visibility.Hidden;
            }
            else
            {
                AnswerField.Visibility = Visibility.Hidden;
                YourAnswer.Visibility = Visibility.Hidden;
                BtnShowPattern.Visibility = Visibility.Hidden;
            }
            //
            if (!taskType.Contains(Constants.Images))
            {
                SituationImage.Visibility = Visibility.Hidden;
                BtnOpenImage.Visibility = Visibility.Hidden;
            }
            else
            {
                image = new Image();
                image.Source = Utils.GetTaskImage(n, situationID);
                if (!taskType.Contains(Constants.Text))
                {
                    SituationImage.Source = image.Source;
                    TextBlockTask.Visibility = Visibility.Hidden;
                    BtnOpenImage.Visibility = Visibility.Hidden;
                }
                else
                {
                    situation = ((Slide1_3)slideElements).GetSituation(situationID);
                    TextBlockTask.Text = situation;
                    if (image.Source == null)
                    {
                        BtnOpenImage.Visibility = Visibility.Hidden;
                    }
                }
            }
            if (taskType.Contains(Constants.Text))
            {
                situation = ((Slide1_3)slideElements).GetSituation(situationID);
                TextBlockTask.Text = situation;
            }
            else
            {
                SituationImage.Source = ((Slide1_3_Images)slideElements).GetSituation(situationID).Source;
            }
        }

        private int GetRandomAvailableSituation()
        {
            if (situations == null)
                situations = slideElements.getSituationsList();
            int count = situations.Count;
            int capacity = slideElements.GetAllSituaitonsCount();
            List<string> uids = new List<string>(count);
            for (int i = 0; i < count; i++)
            {
                uids.Add(situations[i].Uid);
            }
            Random random = new Random();
            int n = random.Next(capacity);
            int id = uids.IndexOf(n.ToString());
            while (!uids.Contains(n.ToString()))
            {
                n = random.Next(capacity);
                id = uids.IndexOf(n.ToString());
            }
            situations.Remove(situations[id]);
            return n;
        }

        private void BtnCheck_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int mark = 0;
                mark = checker.GetMark(AnswerField.Text, answerType, correctPercent);
                MainWindow.get().OpenNewSlide(new Slide4_1(mark));
            }
            catch (System.Runtime.InteropServices.COMException)
            {
                return;
            }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (AnswerField.Text == "")
                BtnCheck.IsEnabled = false;
            else BtnCheck.IsEnabled = true;
            AnswerField = Utils.CheckForGreekLetters(AnswerField);
        }

        private void BtnOpenImage_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.OpenSituationImageWindow(image);
        }

        private void BtnSelectImage_Click(object sender, RoutedEventArgs e)
        {
            SlideTaskImages slideTaskImages = null;
            switch (taskType)
            {
                case Constants.Text:
                    slideTaskImages = new SlideTaskImages(this, n, -1, Utils.GetAnswerImages(n, situationID), taskType, situation);
                    break;
                case Constants.Images:
                    slideTaskImages = new SlideTaskImages(this, n, -1, Utils.GetAnswerImages(n, situationID), taskType, image);
                    break;
                case Constants.Text_Images:
                    slideTaskImages = new SlideTaskImages(this, n, -1, Utils.GetAnswerImages(n, situationID), taskType, situation, image);
                    break;
            }
            MainWindow.get().OpenNewSlide(slideTaskImages);
        }

        private void BtnShowTask_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.OpenTaskWindow(Globals.get().GetTaskText());
        }

        private void BtnShowPattern_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.OpenPatternWindow(taskAnswer.GetAnswerPattern());
        }

        public void IncreaseCountTry()
        {
            if (countTry < 2)
                countTry++;
        }

        public bool IsAnotherTryPossible()
        {
            return Globals.get().GetTotal() - Globals.get().GetSelected() - (countTry - 1) - 1 > 0;
        }

        //дать ещё одну попытку решить задачу на оценку (только для задач с выбором картинок)
        internal void GiveAnotherTry()
        {
            situationID = GetRandomAvailableSituation();
            if (taskType.Contains(Constants.Images))
                image.Source = Utils.GetTaskImage(n, situationID);
            if (taskType == Constants.Images)
                SituationImage.Source = image.Source;
            if (taskType.Contains(Constants.Text))
            {
                situation = ((Slide1_3)slideElements).GetSituation(situationID);
                TextBlockTask.Text = situation;
            }
        }

        public int CalculateMark(bool correct)
        {
            int mark = 0;
            if (correct)
            {
                if (countTry == 1)
                {
                    if (correctPercent > 0.6)
                        mark = 5;
                    else mark = 4;
                }
                else
                {
                    if (correctPercent > 0.6)
                        mark = 4;
                    else mark = 3;
                }
            }
            else
            {
                if (correctPercent > 0.8)
                    mark = 4;
                else if (correctPercent > 0.6)
                    mark = 3;
                else mark = 2;
            }
            return mark;
        }
    }
}
