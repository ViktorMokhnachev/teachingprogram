﻿using System.Collections.Generic;
using System.Windows.Controls;

namespace Patulas
{
    public interface ISituations
    {
        void setSituationID(int situationID);
        void setNextSlide(UserControl slide);
        void setHeaderText(string text);
        void HideSelectFirstSituation();
        int GetAllSituaitonsCount();
        List<ListViewItem> getSituationsList();
        string GetTaskType();
    }
}
