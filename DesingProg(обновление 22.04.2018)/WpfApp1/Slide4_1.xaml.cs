﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Patulas
{
    /// <summary>
    /// Логика взаимодействия для Slide4_1.xaml
    /// </summary>
    public partial class Slide4_1 : UserControl
    {
        public Slide4_1(int mark)
        {
            InitializeComponent();
            Mark.Text = mark.ToString();
            if (MainWindow.get().GetCurrentUser() != null)
            {
                int userID = MainWindow.get().GetCurrentUserID();
                string unit = Globals.get().GetUnit();
                string topic = Globals.get().GetTopic();
                if (topic == null)
                    topic = "";
                string element = Globals.get().GetElement();
                string date = DateTime.Now.ToString();
                Globals globals = Globals.get();
                DatabaseAdapter.AddNewResult(userID, unit, topic, element,
                    globals.GetTotal(), globals.GetSelected(), globals.GetCountRight(),
                    globals.GetCountWrong(), globals.GetDifficultyLevel(), mark, date);
            }
        }

        private void BtnExit_Click(object sender, RoutedEventArgs e)
        {
            MainWindow.get().TaskView.Children.Clear();
            MainWindow.get().UnlockButtons();
        }
    }
}
